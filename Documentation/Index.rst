﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)

============================
EXT: MOOX News & Blog System
============================

:Author:
      Alf Drollinger

:Created:
      2014-08-30 23::0:0:

:Changed by:
      Alf Drollinger

:Changed:
      2014-09-01 23::3:8:

:Classification:
      moox_news

:Description:
      MOOX News & Blog is a fork of Ext:news from Georg Ringer (thanks a lot for this great work!). We forked, because we wanted Bootstrap-optimized Output (yes, you need moox_core to run moox_news) and we improved the backend. Visit http://www.moox.org for more information, free download and full documentation.

:Keywords:
      moox, news, blog, rss, social media, responsive, mobile

:Author:
      DCN GmbH

:Email:
      moox@dcn.de

:Language:
      en


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   ExtMooxNewsBlogSystem/Index

