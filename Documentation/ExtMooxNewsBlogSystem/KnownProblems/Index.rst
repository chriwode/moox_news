﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Known problems
--------------

MOOX should only be installed on new TYPO3-plattforms as it is
incompatible to following extensions:

- all templating Extensions like Templavoila etc.

- css\_styled\_content (will be automatically deinstalled by MOOX Setup)

- all older pibase-Extensions

- most Extensions that generate own FE-Output

Especially moox\_news is also incompatible to

- News by Georg Ringer (Ext:news), because this is a fork

- News (tt\_news)

Please refer to the MOOX Github Repository and check the issues for
more information:

`https://github.com/dcngmbh/moox\_core/issues
<https://github.com/dcngmbh/moox_core/issues>`_ (there is no issue
tracking for news by now)


