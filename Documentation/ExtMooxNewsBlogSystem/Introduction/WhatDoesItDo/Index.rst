﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


What does it do?
^^^^^^^^^^^^^^^^

MOOX News & Blog is a fork of Ext:news from Georg Ringer (thanks a lot
for this great work!). We forked, because we wanted Bootstrap-
optimized Output (consider to install moox\_core to run moox\_news) and we
improved the backend.

Visit `http://www.moox.org/docs <http://www.moox.org>`_ for more information.
