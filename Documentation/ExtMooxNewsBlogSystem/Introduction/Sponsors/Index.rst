﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Sponsors
^^^^^^^^

This Extension is developed by DCN GmbH ( `http://www.dcn.de
<http://www.dcn.de/>`_ ) and sponsored by Chemieverbände Baden-
Württemberg ( `http://www.chemie.com <http://www.chemie.com/>`_ )

