
.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Screenshots
^^^^^^^^^^^

Frontend Output of MOOX Bootstrap Responsive with additional Sequence
Slider showing the responsive views of the same website.

.. image:: ../../../Images/manual_html_3bdc4ebb.jpg

The frontend-output of moox\_news showing a responsive list-view with
integrated News-Teaser and the improved backend module.
