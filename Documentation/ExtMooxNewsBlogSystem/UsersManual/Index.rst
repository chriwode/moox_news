﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Users manual
------------

As moox\_news is a fork of Ext:news and installation and configuration
of moox\_news is currently not documented, please refer to the
documentation of Ext:news.

Alternatively you may order a ready2run plattform on
`http://www.moox.org/services/hosting
<http://www.moox.org/services/hosting>`_


