<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

/**
 * Add extra fields to the pages record
 */
$newPagesColumns = array(
	'news_types' => array(
		'exclude' => 1,
		// show field only for new news type
		'displayCond' => 'FIELD:doktype:=:254',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll . 'pages.news_types',
		'config' => array(
			'type' => 'select',			
			'size' => 6,			
			'maxitems' => 999,
			'minitems' => 0,
			'allowNonIdValues' => 1,			
			'itemsProcFunc' => 'Tx_MooxNews_Hooks_ItemsProcFunc->user_types',
			'default' => ''
		)
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $newPagesColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', 'news_types', '', 'after:hidden');