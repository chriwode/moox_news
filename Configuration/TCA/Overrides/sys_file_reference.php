<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

/**
 * Add extra field showinpreview and some special news controls to sys_file_reference record
 */
$newSysFileReferenceColumns = array(
	'showinpreview' => array(
		'exclude' => 1,
		'label' => $ll . 'tx_mooxnews_domain_model_media.showinpreview',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'import_id' => array(
		'label' => $ll . 'tx_mooxnews_domain_model_news.import_id',
		'config' => array(
			'type' => 'passthrough'
		)
	),
	'import_source' => array(
		'label' => $ll . 'tx_mooxnews_domain_model_news.import_source',
		'config' => array(
			'type' => 'passthrough'
		)
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $newSysFileReferenceColumns);

// add special news palette
$GLOBALS['TCA']['sys_file_reference']['palettes']['newsPalette'] = array(
	'showitem' => 'showinpreview',
	'canNotCollapse' => TRUE
);
