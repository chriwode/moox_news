MOOX News & Blog System
=========

### TYPO3-Extension moox_news

This Ext. is fork of [moox_news](https://github.com/typo3-moox/moox_news).

MOOX News & Blog is a fork of Ext:news from Georg Ringer (thanks a lot for this great work!). We forked, because we wanted Bootstrap-optimized Output (yes, you need moox_core to run moox_news) and we improved the backend.

#### More Links:

Visit http://www.moox.org for more information, free download and full documentation.

Please report all issues here: https://github.com/dcngmbh/moox_core/issues

Repository: http://typo3.org/extensions/repository/view/moox_core

Watch Video: https://www.youtube.com/watch?v=3dL-VImR2x8

Sourceforge: https://sourceforge.net/projects/moox-typo3-bootstrap/

TYPO3 Forge: https://forge.typo3.org/projects/extension-moox_core/


sponsored by http://www.chemie.com
