<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/* ===========================================================================
 	Register BE-Module for Administration
=========================================================================== */

if (TYPO3_MODE === 'BE') {

    /***************
     * Register Main Module
     */
	$mainModuleName = "moox";
	if (!isset($TBE_MODULES[$mainModuleName])) {
        $temp_TBE_MODULES = array();
        foreach ($TBE_MODULES as $key => $val) {
            if ($key == 'web') {
                $temp_TBE_MODULES[$key] = $val;
                $temp_TBE_MODULES[$mainModuleName] = '';
            } else {
                $temp_TBE_MODULES[$key] = $val;
            }
        }
        $TBE_MODULES = $temp_TBE_MODULES;
		if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_core')) {
			$mainModuleKey 		= "DCNGmbH.moox_core";
			$mainModuleIcon 	= 'EXT:moox_core/ext_icon32.png';
			$mainModuleLabels 	= 'LLL:EXT:moox_core/Resources/Private/Language/MainModule.xlf';			
		} else {
			$mainModuleKey 		= 'DCNGmbH.'.$_EXTKEY;
			$mainModuleIcon 	= 'EXT:'.$_EXTKEY.'/Resources/Public/Moox/MainModuleExtIcon.png';
			$mainModuleLabels 	= 'LLL:EXT:'.$_EXTKEY.'/Resources/Public/Moox/MainModule.xlf';
		}
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			$mainModuleKey,
			$mainModuleName,
			'',
			'',
			array(),
			array(
				'access' => 'user,group',
				'icon'   => $mainModuleIcon,
				'labels' => $mainModuleLabels,
			)
		);
    }    
}

/* ===========================================================================
 	Register BE-Module for Administration
=========================================================================== */

$boot = function($packageKey) {
	// The following calls are targeted for BE but might be needed in FE editing

	// CSH - context sensitive help
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tx_mooxnews_domain_model_news', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_news.xlf');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tx_mooxnews_domain_model_media', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_media.xlf');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tx_mooxnews_domain_model_file', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_file.xlf');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tx_mooxnews_domain_model_link', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_link.xlf');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tx_mooxnews_domain_model_tag', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_tag.xlf');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tx_mooxnews_domain_model_target', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_target.xlf');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
			'tt_content.pi_flexform.news_pi1.list', 'EXT:' . $packageKey . '/Resources/Private/Language/locallang_csh_flexforms.xlf');

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_news');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_media');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_file');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_link');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_tag');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_target');

	// Extension manager configuration
	$configuration = Tx_MooxNews_Utility_EmConfiguration::getSettings();

	// include pageTS
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:moox_news/pageTSconfig.txt">');
		
	if (TYPO3_MODE == 'BE') {
		$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($packageKey);
		$pluginSignature = strtolower($extensionName) . '_pi1';

		/***************
		 * Wizard pi1
		 */
		$GLOBALS['TBE_MODULES_EXT']['xMOD_db_new_content_el']['addElClasses'][$pluginSignature . '_wizicon'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($packageKey) . 'Resources/Private/Php/class.' . $packageKey . '_wizicon.php';		

		/***************
		 * Show news table in page module
		 */
		if ($configuration->getPageModuleFieldsNews()) {
			$addTableItems = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', $configuration->getPageModuleFieldsNews(), TRUE);
			foreach ($addTableItems as $item) {
				$split = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('=', $item, TRUE);
				if (count($split) == 2) {
					$fTitle = $split[0];
					$fList = $split[1];
				} else {
					$fTitle = '';
					$fList = $split[0];
				}
				$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_mooxnews_domain_model_news'][] = array(
					'MENU' => $fTitle,
					'fList' => $fList,
					'icon' => TRUE,
				);
			}
		}

		if ($configuration->getPageModuleFieldsCategory()) {
			$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['sys_category'][0] = array(
				'fList' => htmlspecialchars($configuration->getPageModuleFieldsCategory()),
				'icon' => TRUE
			);
		}

		// Extend user settings
		$GLOBALS['TYPO3_USER_SETTINGS']['columns']['newsoverlay'] = array(
			'label'			=> 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:usersettings.overlay',
			'type'			=> 'select',
			'itemsProcFunc'	=> 'Tx_MooxNews_Hooks_ItemsProcFunc->user_categoryOverlay',
		);
		$GLOBALS['TYPO3_USER_SETTINGS']['showitem'] .= ',
			--div--;LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:pi1_title,newsoverlay';

		// Add tables to livesearch (e.g. "#news:fo" or "#newscat:fo")
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['livesearch']['news'] = 'tx_mooxnews_domain_model_news';
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['livesearch']['newstag'] = 'tx_mooxnews_domain_model_tag';


		/* ===========================================================================
			Register BE-Modules
		=========================================================================== */
		/*
		if ($configuration->getShowImporter()) {
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
				$packageKey,
				'moox',
				'tx_mooxnews_m1',
				'',
				array(
					'Import' => 'index, runJob, jobInfo',
				),
				array(
					'access' => 'user,group',
					'icon'   => 'EXT:' . $packageKey . '/Resources/Public/Icons/import_module.gif',
					'labels' => 'LLL:EXT:' . $packageKey . '/Resources/Private/Language/locallang_mod.xlf',
				)
			);

			if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('t3blog')) {
				Tx_MooxNews_Utility_ImportJob::register(
					'Tx_MooxNews_Jobs_T3BlogNewsImportJob',
					'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:t3blog_importer_title',
					'');
				Tx_MooxNews_Utility_ImportJob::register(
					'Tx_MooxNews_Jobs_T3BlogCategoryImportJob',
					'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:t3blogcategory_importer_title',
					'');
			}
		}
		*/

		/* ===========================================================================
			Register BE-Module for Administration
		=========================================================================== */
		if ($configuration->getShowAdministrationModule()) {
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
				$packageKey,
				'moox',
				'tx_mooxnews_m2',
				'',
				array(
					'Administration' => 'index,newNews,deleteNews,cloneNews,deleteTranslation,toggleNewsState,toggleTranslationState,newCategory,newTag,newTarget,newsPidListing,moveToFolder,multiple',
				),
				array(
					'access' => 'user,group',
					'icon'   => 'EXT:' . $packageKey . '/ext_icon.gif',
					'labels' => 'LLL:EXT:' . $packageKey . '/Resources/Private/Language/locallang_modadministration.xlf',
				)
			);
		}

		/* ===========================================================================
			Ajax call to save tags
		=========================================================================== */
		$GLOBALS['TYPO3_CONF_VARS']['BE']['AJAX']['News::createTag'] = 'typo3conf/ext/moox_news/Classes/Hooks/SuggestReceiverCall.php:Tx_MooxNews_Hooks_SuggestReceiverCall->createTag';
		$GLOBALS['TYPO3_CONF_VARS']['BE']['AJAX']['News::createTarget'] = 'typo3conf/ext/moox_news/Classes/Hooks/SuggestReceiverCall.php:Tx_MooxNews_Hooks_SuggestReceiverCall->createTarget';
	}
	
	\FluidTYPO3\Flux\Core::registerProviderExtensionKey('DCNGmbH.MooxNews', 'Page');

	/* ===========================================================================
		Default configuration
	=========================================================================== */
	$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByCategory'] = 'uid,title,tstamp,sorting';
	$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByNews'] = 'tstamp,datetime,crdate,title' . ($configuration->getManualSorting() ? ',sorting' : '');
	$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByTag'] = 'tstamp,crdate,title';
	$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['list'] = $configuration->getRemoveListActionFromFlexforms();	
};

$boot($_EXTKEY);
unset($boot);

/***************
* Icon in page tree
*/
$TCA['pages']['columns']['module']['config']['items'][] = array('MOOX-News', 'mxnews', 'EXT:moox_news/ext_icon.gif');
\TYPO3\CMS\Backend\Sprite\SpriteManager::addTcaTypeIcon('pages', 'contains-mxnews', '../typo3conf/ext/moox_news/ext_icon.gif');

/***************
 * ###DCN### Added custom stylesheet to modify tca form elements (Dominic)
 */
//$TBE_STYLES['styleSheetFile_post'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Css/backend_overwrite.css';


?>