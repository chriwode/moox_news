<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "moox_news".
 *
 * Auto generated 22-10-2015 09:23
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'MOOX News',
    'description' => 'Adaptation of extension "news" (thanks to Georg Ringer) for MOOX. Optimized Backend Module, Responsive Output with MOOX Templates and Social Media Integration.',
    'category' => 'fe',
    'author' => 'Georg Ringer, Dominic Martin & the MOOX Team',
    'author_email' => 'moox@dcn.de',
    'author_company' => 'DCN GmbH',
    'state' => 'beta',
    'uploadfolder' => true,
    'createDirs' => false,
    'clearCacheOnLoad' => 1,
    'version' => '0.9.13',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.3-6.2.99',
            'vhs' => '2.4.0-2.4.99'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    )
);
