TYPO3.jQuery = jQuery.noConflict();

TYPO3.jQuery(document).ready(function() {	
	
	TYPO3.jQuery("#selected-folder").click(function(){
		if ( TYPO3.jQuery("#folder-list").is(":visible") ) {
			TYPO3.jQuery( "#folder-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );			
			TYPO3.jQuery( "#folder-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			TYPO3.jQuery( "#folder-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#folder-expander" ).addClass( "t3-icon-move-up" );
		}
		TYPO3.jQuery("#category-tree").hide("fast");
		TYPO3.jQuery("#folder-list").toggle("fast");
		
	});
	
	TYPO3.jQuery("#folder-list-close").click(function(){
		TYPO3.jQuery("#folder-list").hide("fast");
	});
	
	TYPO3.jQuery("#selected-category").click(function(){
		if ( TYPO3.jQuery("#category-tree").is(":visible") ) {
			TYPO3.jQuery( "#category-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );			
			TYPO3.jQuery( "#category-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			TYPO3.jQuery( "#category-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#category-expander" ).addClass( "t3-icon-move-up" );
		}
		TYPO3.jQuery("#folder-list").hide("fast");
		TYPO3.jQuery("#category-tree").toggle("fast");		
	});
	
	TYPO3.jQuery("#category-tree-close").click(function(){
		TYPO3.jQuery("#category-tree").hide("fast");
	});
	
	TYPO3.jQuery("#filter-query").on("click", function () {
	   TYPO3.jQuery(this).select();
	});
	
	TYPO3.jQuery("#filter-top-news-restriction").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-year").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-type").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-sys-language-uid").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-mailer-frequency").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-sorting-field").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-sorting-direction").on("change", function () {
		TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#news-new-top-selection-extender").on("mouseenter", function () {
		TYPO3.jQuery("#news-new-selection-top").show();
	});
	
	TYPO3.jQuery("#news-new-table-selection-extender").on("mouseenter", function () {
		TYPO3.jQuery("#news-new-selection-table").show();
	});
	
	TYPO3.jQuery("#news-new-selection-top").on("mouseleave", function () {
		TYPO3.jQuery("#news-new-selection-top").hide();
	});
	
	TYPO3.jQuery("#news-new-selection-table").on("mouseleave", function () {
		TYPO3.jQuery("#news-new-selection-table").hide();
	});
	
	TYPO3.jQuery(".news-move-expander").on("mouseenter", function () {
		id = TYPO3.jQuery(this).attr("id");
		id = id.replace("news-move-expander-","");
		showMoveSelection(id)
	});
	
	TYPO3.jQuery(".news-move-selection").on("mouseleave", function () {
		TYPO3.jQuery(".news-move-selection").hide("fast");
	});
	
	TYPO3.jQuery(".mooxnews-checkbox").on("change", function () {
		if(TYPO3.jQuery(this).prop("checked")){
			TYPO3.jQuery(this).parent().parent().addClass("tx-moox-news-admin-table-row-selected");
		} else {
			TYPO3.jQuery(this).parent().parent().removeClass("tx-moox-news-admin-table-row-selected");
		}
		checkedCnt 	= TYPO3.jQuery(".mooxnews-checkbox:checked").length;
		boxCnt 		= TYPO3.jQuery(".mooxnews-checkbox").length;
		if(checkedCnt==boxCnt){
			TYPO3.jQuery(".mooxnews-check-all").prop("checked", true);
		} else {
			TYPO3.jQuery(".mooxnews-check-all").prop("checked", false);
		}		
		toggleSelectionMenu();
	});
	
	TYPO3.jQuery(".mooxnews-check-all").on("change", function () {
		if(TYPO3.jQuery(this).prop("checked")){
			TYPO3.jQuery(".mooxnews-check-all").prop("checked", true);
			TYPO3.jQuery(".mooxnews-checkbox").each(function(){
				TYPO3.jQuery(this).prop("checked", true)
				TYPO3.jQuery(this).parent().parent().addClass("tx-moox-news-admin-table-row-selected");
			});
		} else {
			TYPO3.jQuery(".mooxnews-check-all").prop("checked", false);
			TYPO3.jQuery(".mooxnews-checkbox").each(function(){
				TYPO3.jQuery(this).prop("checked", false)
				TYPO3.jQuery(this).parent().parent().removeClass("tx-moox-news-admin-table-row-selected");
			});			
		}
		toggleSelectionMenu();
	});
});

function toggleSelectionMenu(){
	showMenu = TYPO3.jQuery(".mooxnews-checkbox:checked").length;
	if(showMenu){
		TYPO3.jQuery("#tx-moox-news-admin-multiple-selector-txt").html("<strong>" + showMenu + "</strong> Einträge gewählt");
		TYPO3.jQuery("#tx-moox-news-admin-multiple-selector").show("fast");
	} else {
		TYPO3.jQuery("#tx-moox-news-admin-multiple-selector").hide("fast");
		TYPO3.jQuery("#tx-moox-news-admin-multiple-selector-txt").html("Keine Einträge gewählt");
	}
}

function toggleInfoField(id){	
	if ( TYPO3.jQuery(id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}	
	TYPO3.jQuery(".news-info-field").hide("fast");
	TYPO3.jQuery(".news-info-translation-field").hide("fast");
	TYPO3.jQuery(".news-item-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );			
	TYPO3.jQuery(".news-item-expander-reset").addClass( "t3-icon-move-down" );
	TYPO3.jQuery(".news-item-translation-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );			
	TYPO3.jQuery(".news-item-translation-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		TYPO3.jQuery(id).hide("fast");		
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );			
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		TYPO3.jQuery(id).show("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );			
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-up" );
	}	
}

function toggleTranslations(uid){	
	TYPO3.jQuery(".news-info-field").hide("fast");
	TYPO3.jQuery(".news-info-translation-field").hide("fast");
	TYPO3.jQuery(".news-item-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );			
	TYPO3.jQuery(".news-item-expander-reset").addClass( "t3-icon-move-down" );
	TYPO3.jQuery(".news-item-translation-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );			
	TYPO3.jQuery(".news-item-translation-expander-reset").addClass( "t3-icon-move-down" );
	TYPO3.jQuery(".news-translation").hide("fast");
	if (!TYPO3.jQuery(".news-translation-" + uid).is(":visible")){
		TYPO3.jQuery(".news-translation-" + uid).show("fast");
	}
}

function showMoveSelection(uid){	
	TYPO3.jQuery(".news-move-selection").hide("fast");
	TYPO3.jQuery("#news-move-selection-" + uid).show("fast");	
}