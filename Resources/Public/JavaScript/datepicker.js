$(document).ready(function() {
	$('.ajaxnews-filterset .input-group.date').datepicker({
		format: 'dd.mm.yyyy',
		todayBtn: true,
		clearBtn: true,
		language: 'de',
		calendarWeeks: true,
		autoclose: true,
		todayHighlight: true
	})
});