TYPO3.jQuery(document).ready(function() {	
	TYPO3.jQuery( "form[name='editform']" ).submit(function( event ) {
		if(TYPO3.jQuery("#moox-news-tca-category-min")){
			var countOptions = TYPO3.jQuery('select.moox-news-tca-category-display option').size();
			var minCount = TYPO3.jQuery("#moox-news-tca-category-min").val();
			if(countOptions<minCount){
				alert("Sie müssem mindestens " + minCount + " Kategorie(n) auswählen.");
				event.preventDefault();
			}
		}
	});	
	//TYPO3.jQuery(".moox-news-tca-category-wrapper.is-parent").bind("contextmenu",function(e){
	TYPO3.jQuery(".moox-news-tca-category-wrapper").bind("contextmenu",function(e){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-news-tca-category-wrapper-/g,'');
		TYPO3.jQuery(".moox-news-tca-category-context").hide();
		TYPO3.jQuery("#moox-news-tca-category-context-" + value).show();		
	   return false;
	});
	TYPO3.jQuery(".moox-news-tca-category-context").mouseleave(function() {
		TYPO3.jQuery(this).hide();
	}); 	
});	

function mooxNewsUpdateCategoriesField(id){
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	var cnt = 1;
	TYPO3.jQuery(".moox-news-tca-category-wrapper .t3-icon-toolbar-menu-shortcut.moox-news-tca-category-icon").addClass( "t3-icon-mimetypes t3-icon-mimetypes-x t3-icon-x-sys_category moox-news-tca-category-icon" );
	TYPO3.jQuery(".moox-news-tca-category-wrapper .t3-icon-toolbar-menu-shortcut.moox-news-tca-category-icon").removeClass( "t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-news-tca-category-icon" );	
	if(countOptions>0){
		TYPO3.jQuery(id + '_list option').each( function() {
			TYPO3.jQuery(this).text(TYPO3.jQuery(this).text().replace(/ \(Hauptkategorie\)/g,''));			
			if(cnt==1){
				TYPO3.jQuery("#moox-news-tca-category-hidden").val(TYPO3.jQuery(this).attr('value'));
				TYPO3.jQuery(this).text(TYPO3.jQuery(this).text() + " (Hauptkategorie)");											
				TYPO3.jQuery("#moox-news-tca-category-icon-" + TYPO3.jQuery(this).attr('value')).removeClass( "t3-icon-mimetypes t3-icon-mimetypes-x t3-icon-x-sys_category moox-news-tca-category-icon" );
				TYPO3.jQuery("#moox-news-tca-category-icon-" + TYPO3.jQuery(this).attr('value')).addClass( "t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-news-tca-category-icon" );				
			} else {
				TYPO3.jQuery("#moox-news-tca-category-hidden").val(TYPO3.jQuery("#moox-news-tca-category-hidden").val() + "," + TYPO3.jQuery(this).attr('value'));
			}
			cnt = cnt + 1;
		});	
	} else {
		TYPO3.jQuery("#moox-news-tca-category-hidden").val("");
	}
	TYPO3.jQuery("#moox-news-tca-category-cnt").text(countOptions);
}

function mooxNewsAddOption(id,value,label){	
	var alreadyExisting = false;
	var maxCount = TYPO3.jQuery("#moox-news-tca-category-max").val();
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	if(countOptions<maxCount || maxCount==0){
		TYPO3.jQuery(id + '_list option').each(function(){
			if(TYPO3.jQuery(this).attr('value')==value){
				alreadyExisting = true;			
			}
		});
		TYPO3.jQuery(id + '_list option').removeAttr('selected');
		if(!alreadyExisting){
			TYPO3.jQuery(id + '_list').append( new Option(label,value,false,true) );
			TYPO3.jQuery("#moox-news-tca-category-wrapper-" + value).addClass( "moox-news-tca-category-active" );
		}
	}
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsToggleOption(id,value,label){	
	var alreadyExisting = false;
	var maxCount = TYPO3.jQuery("#moox-news-tca-category-max").val();
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	TYPO3.jQuery(id + '_list option').each(function(){
		if(TYPO3.jQuery(this).attr('value')==value){
			alreadyExisting = true;			
		}
    });
	TYPO3.jQuery(id + '_list option').removeAttr('selected');
	if(!alreadyExisting){
		if(countOptions<maxCount || maxCount==0){
			TYPO3.jQuery(id + '_list').append( new Option(label,value,false,true) );
			TYPO3.jQuery("#moox-news-tca-category-wrapper-" + value).addClass( "moox-news-tca-category-active" );
		}
	} else {
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').remove();
		TYPO3.jQuery("#moox-news-tca-category-wrapper-" + value).removeClass( "moox-news-tca-category-active" );
	}
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsAddAllBranchOptions(id,value){			
	TYPO3.jQuery(".moox-news-tca-category-wrapper-" + value).each(function(){
		branchValue = TYPO3.jQuery(this).attr('id').replace(/moox-news-tca-category-wrapper-/g,'');
		mooxNewsAddOption(id,branchValue,TYPO3.jQuery("#moox-news-tca-category-title-" + branchValue).attr('title'));		
    });
	TYPO3.jQuery(".moox-news-tca-category-wrapper-" + value).each(function(){
		branchValue = TYPO3.jQuery(this).attr('id').replace(/moox-news-tca-category-wrapper-/g,'');
		TYPO3.jQuery(id + '_list option[value="' + branchValue + '"]').attr('selected',true);		
    });
	mooxNewsUpdateCategoriesField(id);
	TYPO3.jQuery(".moox-news-tca-category-context").hide();
}
		
function mooxNewsAddAllOptions(id){	
		
	TYPO3.jQuery(".moox-news-tca-category-wrapper").each(function(){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-news-tca-category-wrapper-/g,'');
		mooxNewsAddOption(id,value,TYPO3.jQuery("#moox-news-tca-category-title-" + value).attr('title'));		
    });
	TYPO3.jQuery(".moox-news-tca-category-wrapper").each(function(){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-news-tca-category-wrapper-/g,'');
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').attr('selected',true);		
    });	
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsRemoveOptions(id){	
	TYPO3.jQuery(id + '_list option:selected').each(function(){
		TYPO3.jQuery("#moox-news-tca-category-wrapper-" + TYPO3.jQuery(this).attr('value')).removeClass( "moox-news-tca-category-active" );
		TYPO3.jQuery(this).remove();
    });
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsRemoveAllOptions(id){	
	TYPO3.jQuery(id + '_list option').each(function(){
		TYPO3.jQuery("#moox-news-tca-category-wrapper-" + TYPO3.jQuery(this).attr('value')).removeClass( "moox-news-tca-category-active" );
		TYPO3.jQuery(this).remove();
    });
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsRemoveAllBranchOptions(id,value){	
	TYPO3.jQuery(".moox-news-tca-category-wrapper-" + value).each(function(){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-news-tca-category-wrapper-/g,'');
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').each(function(){
			TYPO3.jQuery("#moox-news-tca-category-wrapper-" + TYPO3.jQuery(this).attr('value')).removeClass( "moox-news-tca-category-active" );
			TYPO3.jQuery(this).remove();
		});	
    });		
	mooxNewsUpdateCategoriesField(id);
	TYPO3.jQuery(".moox-news-tca-category-context").hide();
}

function mooxNewsSetMainCategory(id,value,label){	
	
	value = typeof value !== 'undefined' ? value : 0;
	label = typeof label !== 'undefined' ? label : '';
	
	var newPos = 0;
	var countOptions = TYPO3.jQuery(id + '_list option:selected').size();
	if(value>0){
		mooxNewsAddOption(id,value,label);
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').remove();
		TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+value+"' selected='selected'>"+ label +"</option>");		
		TYPO3.jQuery(".moox-news-tca-category-context").hide();
	} else {
		if(countOptions>1){
			alert("Bitte wählen Sie nur eine Kategorie als Hauptkategorie aus");
		} else {
			TYPO3.jQuery(id + '_list option:selected').each( function() {		
				value = TYPO3.jQuery(this).val();
				TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+value+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
				TYPO3.jQuery(this).remove();		
				newPos = newPos + 1;
			});					
		}
	}
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsMoveOptionsToTop(id){	
	var newPos = 0;
	TYPO3.jQuery(id + '_list option:selected').each( function() {		
		TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
		TYPO3.jQuery(this).remove();		
		newPos = newPos + 1;
    });
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsMoveOptionsOneUp(id){	
	TYPO3.jQuery(id + '_list option:selected').each( function() {
		var newPos = TYPO3.jQuery(id + '_list option').index(this) - 1;
		if (newPos > -1) {
			TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
			TYPO3.jQuery(this).remove();
		}	
    });
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsMoveOptionsOneDown(id){	
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	TYPO3.jQuery(TYPO3.jQuery(id + '_list option:selected').get().reverse()).each( function() {
		var newPos = TYPO3.jQuery(id + '_list option').index(this) + 1;
		if (newPos < countOptions) {
			TYPO3.jQuery(id + '_list option').eq(newPos).after("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
			TYPO3.jQuery(this).remove();
		}	
    });
	mooxNewsUpdateCategoriesField(id);
}

function mooxNewsMoveOptionsToBottom(id){	
	var newPos = TYPO3.jQuery(id + '_list option').size();
	newPos = newPos - 1;
	TYPO3.jQuery(TYPO3.jQuery(id + '_list option:selected').get().reverse()).each( function() {
		TYPO3.jQuery(id + '_list option').eq(newPos).after("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
		TYPO3.jQuery(this).remove();
		newPos = newPos - 1;	
    });
	mooxNewsUpdateCategoriesField(id);
}