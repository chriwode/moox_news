<?php
namespace DCNGmbH\MooxNews\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class GrabberService implements \TYPO3\CMS\Core\SingletonInterface {	
	
	// user upload folder
	public static $userUploadFolder			= "/user_upload";
	
	// news folder
	public static $newsFolder				= "/moox_news";
	
	// import folder
	public static $grabberFolder			= "/grabber";
	
	/**
	 * convert xml to array
	 * @param SimpleXMLElement $xml
	 * @return array $output
	 */
	public function xmlToArray(SimpleXMLElement $xml) {
		$json 	= json_encode($xml);
		$output	= json_decode($json,TRUE);
		return $output;
	}
	
	/**
	 * encode special chars
	 * @param string $input
	 * @return array $output
	 */
	public function encode ($input){	
		$charsArray 	= array('/ä/','/ö/','/ü/','/Ä/','/Ö/','/Ü/','/ß/','/&/');
		$replaceArray 	= array('#auml;','#ouml;','#uuml;','#Auml;','#Ouml;','#Uuml;','#szlig;','#amp;');
		$output			= preg_replace($charsArray , $replaceArray , $input);
		
		return $output;
	}
	
	/**
	 * get contents from url
	 * @param string $url
	 * @return array $header
	 */
	public function getContentsFromUrl($url){
		$options = array(
							CURLOPT_RETURNTRANSFER => true,     // return web page
							CURLOPT_HEADER         => false,    // don't return headers
							CURLOPT_FOLLOWLOCATION => true,     // follow redirects
							CURLOPT_ENCODING       => "",       // handle all encodings
							CURLOPT_USERAGENT      => "spider", // who am i
							CURLOPT_AUTOREFERER    => true,     // set referer on redirect
							CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
							CURLOPT_TIMEOUT        => 120,      // timeout on response
							CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
		);

		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	
	/**
	 * get news action
	 * @param string $grabberRemoteUid
	 * @param string $grabberRemoteHash
	 * @return string $action
	 */
	public function getAction($grabberRemoteUid,$grabberRemoteHash){	
				
		if(is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder)){
			if(!is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder)){				
				mkdir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder);				
			}
			if(is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder)){					
				if(!is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder)){
					mkdir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder);
				}
			}		 
		}
		
		$objectManager 	= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$newsRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		
		$news = $newsRepository->findOneByGrabberRemoteUid($grabberRemoteUid);
		
		if($news){
			if($news->getGrabberRemoteHash()!="" && $news->getGrabberRemoteHash()!=$grabberRemoteHash){
				$return['action'] 	= "update";
				$return['uid'] 		= $news->getUid();
			} elseif($news->getGrabberRemoteHash()!="" && $news->getGrabberRemoteHash()==$grabberRemoteHash){
				$return['action'] 	= "skip";
			} else {
				$return['action'] 	= "insert";
			}
		} else {
			$return['action'] 	= "insert";
		}		
		return $return;
	}
	
	/**
	 * copy image to server
	 * @param string $sourcefile
	 * @param string $title
	 * @param string $id
	 * @return void
	 */
	public function copyImage($sourcefile,$title,$id){
	
		$destinationfile = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder."/".$id."_".$title;
		
		if(!file_exists($destinationfile)){		
			copy($sourcefile,$destinationfile);
		}
	}
	
	/**
	 * get image title
	 * @param string $url
	 * @return string $filename
	 */
	public function getImageFileName($url){
		$urlParts = explode("/",$url);		
		$filename = $urlParts[count($urlParts)-1];
		return $filename;
	}
	
	/**
	 * get image extension
	 * @param string $filename
	 * @return string $extension
	 */
	public function getImageExtension($filename){
		$urlParts 	= explode(".",$filename);
		$extension = $urlParts[count($urlParts)-1];
		return strtolower($extension);
	}
	
	/**
	 * get image extension
	 * @param string $filename
	 * @return string $imagetitle
	 */
	public function getImageTitle($filename){
		$extension 	= $this->getImageExtension($filename);
		$imagetitle = str_replace(".".$extension,"",$filename);
		return $imagetitle;
	}
	
	/**
	 * make file name
	 * @param string $title
	 * @return string $fileName
	 */
	public function makeFileName($title){

		$string = htmlentities($string, ENT_QUOTES, 'UTF-8');
		$string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
		$string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
		$string = preg_replace(array('~[^0-9a-z]~i', '~[ -]+~'), ' ', $string);
		
		$fileName = trim($string, ' -');
		
		return $fileName;
	}
	
	/**
	 * write grabbed news to db
	 * @param array $news	
	 * @return string $action
	 */
	public function writeNews($news){
		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$newsRepository 	= $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		$categoryRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');
		
		$imageCount = 0;
		$images 	= array();
		
		if(count($news['images'])>0){			
			foreach($news['images'] AS $grabbedimage){
				$imageCount++;
				$image['sorting']	= $imageCount;
				$image['filename']	= $news['remoteid']."_".$this->getImageFileName($grabbedimage['src']);
				$image['extension']	= $this->getImageExtension($image['filename']);
				$image['title']		= $this->getImageTitle($image['filename']);
				$image['caption'] 	= $grabbedimage['desc'];
				$images[] = $image;
				$this->copyImage($grabbedimage['src'],$this->getImageFileName($grabbedimage['src']),$news['remoteuid']);
			}
		} 
		
		$timestamp = time();
		
		$insertNews = $objectManager->get('Tx_MooxNews_Domain_Model_News');		
		$insertNews->setPid($news['pid']);
		$insertNews->setTitle($news['title']);
		$insertNews->setTeaser($news['teaser']);
		$insertNews->setBodytext($news['bodytext']);
		$insertNews->setAuthor($news['author']);		
		$insertNews->setDatetime($timestamp);
		$insertNews->setGrabberRemoteUid($news['remoteid']);
		$insertNews->setGrabberRemoteHash($news['hash']);
		
		foreach($news['categories'] AS $category){
			$addCategory = $categoryRepository->findByUid($category);
			if($addCategory){
				$insertNews->addCategory($addCategory);
			}
		}
		
		$newsRepository->add($insertNews);
						
		$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();	

		$this->writeImagesFal($insertNews,$news,$images);				
	}
	
	/**
	 * update grabbed news in db
	 * @param array $news	
	 * @return string $action
	 */
	public function updateNews($news){
		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$newsRepository 	= $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		$categoryRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');		
		$updateNews			= $newsRepository->findByUid($news['uid']);
		
		if($updateNews){
		
			$imageCount = 0;
			$images 	= array();
			
			if(count($news['images'])>0){			
				foreach($news['images'] AS $grabbedimage){
					$imageCount++;
					$image['sorting']	= $imageCount;
					$image['filename']	= $news['remoteid']."_".$this->getImageFileName($grabbedimage['src']);
					$image['extension']	= $this->getImageExtension($image['filename']);
					$image['title']		= $this->getImageTitle($image['filename']);
					$image['caption'] 	= $grabbedimage['desc'];
					$images[] = $image;				
					$this->copyImage($grabbedimage['src'],$this->getImageFileName($grabbedimage['src']),$news['remoteid']);
				}
			} 
			
			$timestamp = time();
								
			$updateNews->setTitle($news['title']);
			$updateNews->setTeaser($news['teaser']);
			$updateNews->setBodytext($news['bodytext']);
			$updateNews->setGrabberRemoteHash($news['hash']);			
			
			$newsRepository->add($updateNews);
							
			$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();	

			$this->writeImagesFal($updateNews,$news,$images);		
		}
	}
	
	/**
	 * write images of grabbed news to db
	 * @param Tx_MooxNews_Domain_Repository_NewsRepository $newNews	
	 * @param array $news
	 * @param array $images
	 * @return void
	 */
	public function writeImages($newsObj,$news,$images){
				
		$mime_types['jpg'] 	= "image/jpeg";
		$mime_types['png'] 	= "image/png";
		$mime_types['gif'] 	= "image/gif";
		
		$types['jpg'] 		= 2;
		$types['png'] 		= 2;
		$types['gif'] 		= 2;		
		
		$res = $GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_mooxnews_domain_model_media', "parent = ".$newsObj->getUid());
		
		$sorting = 1;
		
		foreach($images AS $image){
		
			if(file_exists($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder."/".$image['filename'])){
			
				$image['cruser_id'] 		= $newsObj->getCruserId();						
				$image['import_id'] 		= $news['id'];
				$image['import_source'] 	= $news['storagepid'];			
				$image['pid'] 				= $news['pid'];
				$image['storage']			= $news['storagepid'];
				$image['type'] 				= $types[strtolower($image['extension'])];
				$image['identifier'] 		= self::$userUploadFolder.self::$newsFolder.self::$grabberFolder."/".$image['filename'];
				$image['identifier_hash'] 	= sha1($image['identifier']);
				$image['folder_hash'] 		= sha1(self::$userUploadFolder.self::$newsFolder.self::$grabberFolder);
				$image['extension'] 		= strtolower($image['extension']);
				$image['mime_type'] 		= $mime_types[$image['extension']];
				$image['name'] 				= ($image['title']!="")?$image['title']:$image['filename'];;
				$image['sha1'] 				= sha1_file($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin/'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder.'/'.$image['filename']);
				$image['size'] 				= filesize($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin/'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder.'/'.$image['filename']);
				
				$imageInfo 					= getimagesize($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin/'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder.'/'.$image['filename']);
				
				$image['width']				= $imageInfo[0];
				$image['height']			= $imageInfo[1];
				
				$time = time();
				
				$insertMedia 					= array();
				$insertMedia['uid'] 			= NULL;
				$insertMedia['pid'] 			= $image['pid'];
				$insertMedia['tstamp'] 			= $time;
				$insertMedia['crdate'] 			= $time;
				$insertMedia['cruser_id'] 		= $image['cruser_id'];
				$insertMedia['sorting'] 		= $sorting;
				$insertMedia['hidden'] 			= $image['hidden'];
				$insertMedia['parent'] 			= $newsObj->getUid();
				$insertMedia['caption'] 		= $image['caption'];
				$insertMedia['alt'] 			= $image['alt'];
				$insertMedia['title'] 			= $image['title'];
				$insertMedia['copyright'] 		= $image['copyright'];
				$insertMedia['image'] 			= $image['filename'];
				$insertMedia['type'] 			= 0;
				$insertMedia['description'] 	= $image['description'];
				$insertMedia['import_id'] 		= $image['import_id'];
				$insertMedia['import_source'] 	= $image['import_source'];
				$insertMedia['showinpreview'] 	= ($sorting>1)?0:1;
				
				$res = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
				  'tx_mooxnews_domain_model_media',
				  $insertMedia
				);
			
				$image['sorting'] 	= $sorting;
				$image['uid'] 		= $uid;
				
				$sorting++;
			}			
		}		
	}
	
	/**
	 * write images of grabbed news to db (FAL)
	 * @param Tx_MooxNews_Domain_Repository_NewsRepository $newNews	
	 * @param array $news
	 * @param array $images
	 * @return void
	 */
	public function writeImagesFal($newsObj,$news,$images){
		
		$mime_types['jpg'] 	= "image/jpeg";
		$mime_types['png'] 	= "image/png";
		$mime_types['gif'] 	= "image/gif";
		
		$types['jpg'] 		= 2;
		$types['png'] 		= 2;
		$types['gif'] 		= 2;		
		
		$query['fields'] 	= 'uid_local';
		$query['table'] 	= 'sys_file_reference';
		$query['where'] 	= 'fieldname="fal_media" AND tablenames="tx_mooxnews_domain_model_news" AND import_source="'.$news['sourcealias'].'" AND import_id='.$news['id'];
		
		$files = $GLOBALS['TYPO3_DB']->exec_SELECTquery($query['fields'],$query['table'],$query['where']);
		
		$res = $GLOBALS['TYPO3_DB']->exec_DELETEquery('sys_file_reference', 'fieldname="fal_media" AND tablenames="tx_mooxnews_domain_model_news" AND import_source="'.$news['sourcealias'].'" AND import_id='.$news['id']);
		
		while ($file = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($files)) {			
			$res = $GLOBALS['TYPO3_DB']->exec_DELETEquery('sys_file','uid='.$file['uid_local']);
			$res = $GLOBALS['TYPO3_DB']->exec_DELETEquery('sys_file_metadata','file='.$file['uid_local']);			
		}
		
		$sorting = 1;
		
		$imageCnt = 0;
		
		foreach($images AS $image){
			
			if(file_exists($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder."/".$image['filename'])){
			
				$image['cruser_id'] 		= $newsObj->getCruserId();						
				$image['import_id'] 		= $news['id'];
				$image['import_source'] 	= $news['storagepid'];			
				$image['pid'] 				= $news['pid'];
				$image['storage']			= $news['storagepid'];
				$image['type'] 				= $types[strtolower($image['extension'])];
				$image['identifier'] 		= self::$userUploadFolder.self::$newsFolder.self::$grabberFolder."/".$image['filename'];
				$image['identifier_hash'] 	= sha1($image['identifier']);
				$image['folder_hash'] 		= sha1(self::$userUploadFolder.self::$newsFolder.self::$grabberFolder);
				$image['extension'] 		= strtolower($image['extension']);
				$image['mime_type'] 		= $mime_types[$image['extension']];
				$image['name'] 				= ($image['title']!="")?$image['title']:$image['filename'];;
				$image['sha1'] 				= sha1_file($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin/'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder.'/'.$image['filename']);
				$image['size'] 				= filesize($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin/'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder.'/'.$image['filename']);
				$image['hidden']			= 0;
				
				$imageInfo 					= getimagesize($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/fileadmin/'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder.'/'.$image['filename']);
				
				$image['width']				= $imageInfo[0];
				$image['height']			= $imageInfo[1];
				
				$time = time();
				
				$insertFile 						= array();
				$insertFile['uid'] 					= NULL;
				$insertFile['pid'] 					= 0;
				$insertFile['tstamp'] 				= $time;
				$insertFile['storage'] 				= $image['storage'];
				$insertFile['type'] 				= $image['type'];
				$insertFile['identifier'] 			= $image['identifier'];
				$insertFile['identifier_hash'] 		= $image['identifier_hash'];
				$insertFile['folder_hash'] 			= $image['folder_hash'];
				$insertFile['extension'] 			= $image['extension'];
				$insertFile['mime_type'] 			= $image['mime_type'];
				$insertFile['name'] 				= $image['name'];
				$insertFile['sha1'] 				= $image['sha1'];
				$insertFile['size'] 				= $image['size'];
				$insertFile['creation_date'] 		= $time;
				$insertFile['modification_date'] 	= $time;
				$insertFile['metadata'] 			= 1;				
				
				$res = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
				  'sys_file',
				  $insertFile
				);								
											
				$lastInsertId = $GLOBALS['TYPO3_DB']->sql_insert_id();;									
				
				if($lastInsertId){
					
					$insertFileMetadataMetadata 		= array();
					$insertFileMetadata['uid'] 			= NULL;
					$insertFileMetadata['pid'] 			= 0;
					$insertFileMetadata['tstamp'] 		= $time;
					$insertFileMetadata['crdate'] 		= $time;
					$insertFileMetadata['cruser_id'] 	= $image['cruser_id'];
					$insertFileMetadata['file'] 		= $lastInsertId;
					$insertFileMetadata['title'] 		= $image['title'];
					$insertFileMetadata['width'] 		= $image['width'];
					$insertFileMetadata['height'] 		= $image['height'];
					$insertFileMetadata['description'] 	= $image['caption'];
					$insertFileMetadata['alternative'] 	= $image['alt'];
					
					$res = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					  'sys_file_metadata',
					  $insertFileMetadata
					);
					
					
					
					$insertFileReference 					= array();
					$insertFileReference['uid'] 			= NULL;
					$insertFileReference['pid'] 			= $image['pid'];
					$insertFileReference['tstamp'] 			= $time;					
					$insertFileReference['crdate'] 			= $time;
					$insertFileReference['cruser_id'] 		= $image['cruser_id'];
					$insertFileReference['hidden'] 			= $image['hidden'];
					$insertFileReference['uid_local'] 		= $lastInsertId;
					$insertFileReference['uid_foreign'] 	= $newsObj->getUid();
					$insertFileReference['tablenames'] 		= 'tx_mooxnews_domain_model_news';
					$insertFileReference['fieldname'] 		= 'fal_media';
					$insertFileReference['sorting_foreign'] = $sorting;
					$insertFileReference['table_local'] 	= 'sys_file';
					$insertFileReference['title'] 			= $image['title'];
					$insertFileReference['description'] 	= $image['caption'];
					$insertFileReference['alternative'] 	= $image['alt'];
					$insertFileReference['import_id'] 		= $image['import_id'];
					$insertFileReference['import_source'] 	= $image['import_source'];
					$insertFileReference['showinpreview'] 	= ($sorting>1)?0:1;
					
					$res = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					  'sys_file_reference',
					  $insertFileReference
					);	
					
				}
				
				$image['sorting'] 	= $sorting;
				$image['uid'] 		= $uid;
				
				$sorting++;
				
				$imageCnt++;
			}

			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'tx_mooxnews_domain_model_news',
				'uid='.$newsObj->getUid(),
				array("fal_media",$imageCnt)
			);
		}
	}
		
	/**
	 * decode special chars
	 * @param string $input
	 * @return string $output
	 */
	 public function decode($input){
		
		$output = $input;
		
		$replaceArray 	= array('ä','ö','ü','Ä','Ö','Ü','ß','&');
		$charsArray 	= array('/#auml;/','/#ouml;/','/#uuml;/','/#Auml;/','/#Ouml;/','/#Uuml;/','/#szlig;/','/#amp;/');
		$output = preg_replace($charsArray , $replaceArray , $output);
		$output = trim(str_replace("#strong#","<strong>",$output));
		$output = trim(str_replace("#/strong#","</strong>",$output));
		$output = trim(str_replace("#a1#","<a ",$output));
		$output = trim(str_replace("#a2#"," >",$output));
		$output = trim(str_replace("#/a#","</a>",$output));
		$output = trim(str_replace("#/strong#","</strong>",$output));
		$output = utf8_decode($output);
		
		return $output;
	}

}
?>