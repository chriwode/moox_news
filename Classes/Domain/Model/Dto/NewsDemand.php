<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News Demand object which holds all information to get the correct
 * news records.
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_Domain_Model_Dto_NewsDemand
	extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity implements Tx_MooxNews_Domain_Model_DemandInterface {

	/**
	 * @var array
	 */
	protected $categories;

	/**
	 * @var string
	 */
	protected $categoryConjunction;

	/**
	 * @var boolean
	 */
	protected $includeSubCategories = FALSE;

	/**
	 * @var string
	 */
	protected $author;	

	/**
	 * @var string
	 */
	protected $archiveRestriction;

	/**
	 * @var string
	 */
	protected $timeRestriction = NULL;

	/** @var string */
	protected $timeRestrictionHigh = NULL;

	/** @var integer */
	protected $topNewsRestriction;

	/** @var string */
	protected $dateField;

	/** @var integer */
	protected $month;

	/** @var integer */
	protected $year;

	/** @var integer */
	protected $day;
	
	/** @var string */
	protected $dateFrom;
	
	/** @var string */
	protected $dateTo;

	/** @var string */
	protected $searchFields;

	/** @var Tx_MooxNews_Domain_Model_Dto_Search */
	protected $search;

	/** @var string */
	protected $order;

	/** @var string */
	protected $orderByAllowed;

	/** @var boolean */
	protected $topNewsFirst;

	/** @var integer */
	protected $storagePage;

	/** @var integer */
	protected $limit;

	/** @var integer */
	protected $offset;
	
	/** @var string	 */
	protected $query;
	
	/** @var boolean */
	protected $includeOnlySelectedNewsInRss;
	
	/** @var boolean */
	protected $excludeAlreadyDisplayedNews;
	
	/** @var boolean */
	protected $excludeFromRss;
	
	/** \TYPO3\CMS\Extbase\Persistence\ObjectStorage */
	/**
	 * @var array
	 */
	protected $tags;
	
	/** @var string	 */
	protected $tag;
	
	/**
	 * @var array
	 */
	protected $targets;
	
	/** @var string	 */
	protected $target;
	
	/** @var integer */
	protected $sysLanguageUid;
	
	/** @var array	 */
	protected $types;
	
	/** @var string	 */
	protected $type;
	
	/** @var string	 */
	protected $char;
	
	/** @var integer */
	protected $privacy;
	
	/**
	 * @var array
	 */
	public $charList;

	/**
	 * Set archive settings
	 *
	 * @param string $archiveRestriction archive setting
	 * @return void
	 */
	public function setArchiveRestriction($archiveRestriction) {
		$this->archiveRestriction = $archiveRestriction;
	}

	/**
	 * Get archive setting
	 *
	 * @return string
	 */
	public function getArchiveRestriction() {
		return $this->archiveRestriction;
	}

	/**
	 * List of allowed categories
	 *
	 * @param array $categories categories
	 * @return void
	 */
	public function setCategories($categories) {
		$this->categories = $categories;
	}

	/**
	 * Get allowed categories
	 *
	 * @return array
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Set category mode
	 *
	 * @param string $categoryConjunction
	 * @return void
	 */
	public function setCategoryConjunction($categoryConjunction) {
		$this->categoryConjunction = $categoryConjunction;
	}

	/**
	 * Get category mode
	 *
	 * @return string
	 */
	public function getCategoryConjunction() {
		return $this->categoryConjunction;
	}

	/**
	 * Get include sub categories
	 * @return boolean
	 */
	public function getIncludeSubCategories() {
		return (boolean)$this->includeSubCategories;
	}

	/**
	 * @param boolean $includeSubCategories
	 * @return void
	 */
	public function setIncludeSubCategories($includeSubCategories) {
		$this->includeSubCategories = $includeSubCategories;
	}

	/**
	 * Set author
	 *
	 * @param string $author
	 * @return void
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * Get author
	 *
	 * @return string
	 */
	public function getAuthor() {
		return $this->author;
	}


	/**
	 * Set time limit low, either integer or string
	 *
	 * @param mixed $timeRestriction
	 * @return void
	 */
	public function setTimeRestriction($timeRestriction) {
		$this->timeRestriction = $timeRestriction;
	}

	/**
	 * Get time limit low
	 *
	 * @return mixed
	 */
	public function getTimeRestriction() {
		return $this->timeRestriction;
	}

	/**
	 * Get time limit high
	 *
	 * @return mixed
	 */
	public function getTimeRestrictionHigh() {
		return $this->timeRestrictionHigh;
	}

	/**
	 * Set time limit high
	 *
	 * @param mixed $timeRestrictionHigh
	 * @return void
	 */
	public function setTimeRestrictionHigh($timeRestrictionHigh) {
		$this->timeRestrictionHigh = $timeRestrictionHigh;
	}

	/**
	 * Set order
	 *
	 * @param string $order order
	 * @return void
	 */
	public function setOrder($order) {
		$this->order = $order;
	}

	/**
	 * Get order
	 *
	 * @return string
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * Set order allowed
	 *
	 * @param string $orderByAllowed allowed fields for ordering
	 * @return void
	 */
	public function setOrderByAllowed($orderByAllowed) {
		$this->orderByAllowed = $orderByAllowed;
	}

	/**
	 * Get allowed order fields
	 *
	 * @return string
	 */
	public function getOrderByAllowed() {
		return $this->orderByAllowed;
	}

	/**
	 * Set order respect top news flag
	 *
	 * @param boolean $topNewsFirst respect top news flag
	 * @return void
	 */
	public function setTopNewsFirst($topNewsFirst) {
		$this->topNewsFirst = $topNewsFirst;
	}

	/**
	 * Get order respect top news flag
	 *
	 * @return integer
	 */
	public function getTopNewsFirst() {
		return $this->topNewsFirst;
	}

	/**
	 * Set search fields
	 *
	 * @param string $searchFields search fields
	 * @return void
	 */
	public function setSearchFields($searchFields) {
		$this->searchFields = $searchFields;
	}

	/**
	 * Get search fields
	 *
	 * @return string
	 */
	public function getSearchFields() {
		return $this->searchFields;
	}

	/**
	 * Set top news setting
	 *
	 * @param integer $topNewsRestriction top news settings
	 * @return void
	 */
	public function setTopNewsRestriction($topNewsRestriction) {
		$this->topNewsRestriction = $topNewsRestriction;
	}

	/**
	 * Get top news setting
	 *
	 * @return integer
	 */
	public function getTopNewsRestriction() {
		return $this->topNewsRestriction;
	}

	/**
	 * Set list of storage pages
	 *
	 * @param string $storagePage storage page list
	 * @return void
	 */
	public function setStoragePage($storagePage) {
		$this->storagePage = $storagePage;
	}

	/**
	 * Get list of storage pages
	 *
	 * @return string
	 */
	public function getStoragePage() {
		return $this->storagePage;
	}
	
	/**
	 * Get date from
	 *
	 * @return integer
	 */
	public function getDateFrom() {
		return $this->dateFrom;
	}

	/**
	 * Set date from
	 *
	 * @param integer $dateFrom
	 * @return void
	 */
	public function setDateFrom($dateFrom) {
		$this->dateFrom = $dateFrom;
	}
	
	/**
	 * Get date to
	 *
	 * @return integer
	 */
	public function getDateTo() {
		return $this->dateTo;
	}

	/**
	 * Set date to
	 *
	 * @param integer $dateTo
	 * @return void
	 */
	public function setDateTo($dateTo) {
		$this->dateTo = $dateTo;
	}
	
	/**
	 * Get day restriction
	 *
	 * @return integer
	 */
	public function getDay() {
		return $this->day;
	}

	/**
	 * Set day restriction
	 *
	 * @param integer $day
	 * @return void
	 */
	public function setDay($day) {
		$this->day = $day;
	}

	/**
	 * Get month restriction
	 *
	 * @return integer
	 */
	public function getMonth() {
		return $this->month;
	}

	/**
	 * Set month restriction
	 *
	 * @param integer $month month
	 * @return void
	 */
	public function setMonth($month) {
		$this->month = $month;
	}

	/**
	 * Get year restriction
	 *
	 * @return integer
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 * Set year restriction
	 *
	 * @param integer $year year
	 * @return void
	 */
	public function setYear($year) {
		$this->year = $year;
	}

	/**
	 * Set limit
	 *
	 * @param integer $limit limit
	 * @return void
	 */
	public function setLimit($limit) {
		$this->limit = (int)$limit;
	}

	/**
	 * Get limit
	 *
	 * @return integer
	 */
	public function getLimit() {
		return $this->limit;
	}

	/**
	 * Set offset
	 *
	 * @param integer $offset offset
	 * @return void
	 */
	public function setOffset($offset) {
		$this->offset = (int)$offset;
	}

	/**
	 * Get offset
	 *
	 * @return integer
	 */
	public function getOffset() {
		return $this->offset;
	}
	
	/**
	 * Set per page
	 *
	 * @param integer $perPage per page
	 * @return void
	 */
	public function setPerPage($perPage) {
		$this->perPage = (int)$perPage;
	}

	/**
	 * Get per page
	 *
	 * @return integer
	 */
	public function getPerPage() {
		return $this->perPage;
	}

	/**
	 * Set date field which is used for datemenu
	 *
	 * @param string $dateField datefield
	 * @return void
	 */
	public function setDateField($dateField) {
		$this->dateField = $dateField;
	}

	/**
	 * Get datefield which is used for datemenu
	 *
	 * @return string
	 */
	public function getDateField() {
		return $this->dateField;
	}

	/**
	 * Get search object
	 *
	 * @return Tx_MooxNews_Domain_Model_Dto_Search
	 */
	public function getSearch() {
		return $this->search;
	}

	/**
	 * Set search object
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_Search $search search object
	 * @return void
	 */
	public function setSearch($search = NULL) {
		$this->search = $search;
	}
	
	/**
	 * @return string
	 */
	public function getQuery() {
		return $this->query;
	}

	/**
	 * @param string $query
	 * @return void
	 */
	public function setQuery($query) {
		$this->query = $query;
	}
	
	/**
	 * List of allowed tags
	 *
	 * @param array $tags tags
	 * @return void
	 */
	public function setTags($tags) {
		$this->tags = $tags;
	}

	/**
	 * Get allowed tags
	 *
	 * @return array
	 */
	public function getTags() {
		return $this->tags;
	}
	
	/**
	 * Set tag
	 *
	 * @param string $tag
	 * @return void
	 */
	public function setTag($tag) {
		$this->tag = $tag;
	}

	/**
	 * Get tag
	 *
	 * @return string
	 */
	public function getTag() {
		return $this->tag;
	}
	
	/**
	 * Get Tags
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	/*
	public function getTags() {
		return $this->tags;
	}
	*/

	/**
	 * Set Tags
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $tags tags
	 * @return void
	 */
	/*
	public function setTags($tags) {
		$this->tags = $tags;
	}
	*/
	
	/**
	 * List of allowed targets
	 *
	 * @param array $targets targets
	 * @return void
	 */
	public function setTargets($targets) {
		$this->targets = $targets;
	}

	/**
	 * Get allowed targets
	 *
	 * @return array
	 */
	public function getTargets() {
		return $this->targets;
	}
	
	/**
	 * Set target
	 *
	 * @param string $target
	 * @return void
	 */
	public function setTarget($target) {
		$this->target = $target;
	}

	/**
	 * Get target
	 *
	 * @return string
	 */
	public function getTarget() {
		return $this->target;
	}
	
	/**
	 * Current sys language uid
	 *
	 * @param integer $sysLanguageUid sys language uid
	 * @return void
	 */
	public function setSysLanguageUid($sysLanguageUid) {
		$this->sysLanguageUid = $sysLanguageUid;
	}

	/**
	 * Get allowed sys language uid
	 *
	 * @return integer
	 */
	public function getSysLanguageUid() {
		return $this->sysLanguageUid;
	}
	
	/**
	 * Set flag if only selected news should be shown in rss feeds
	 *
	 * @param boolean $includeOnlySelectedNewsInRss
	 * @return void
	 */
	public function setIncludeOnlySelectedNewsInRss($includeOnlySelectedNewsInRss) {
		$this->includeOnlySelectedNewsInRss = (bool)$includeOnlySelectedNewsInRss;
	}

	/**
	 * Get flag if only selected news should be shown in rss feeds
	 *
	 * @return boolean
	 */
	public function getIncludeOnlySelectedNewsInRss() {
		return $this->includeOnlySelectedNewsInRss;
	}
	
	/**
	 * Set flag if displayed news records should be excluded
	 *
	 * @param boolean $excludeAlreadyDisplayedNews
	 * @return void
	 */
	public function setExcludeAlreadyDisplayedNews($excludeAlreadyDisplayedNews) {
		$this->excludeAlreadyDisplayedNews = (bool)$excludeAlreadyDisplayedNews;
	}

	/**
	 * Get flag if displayed news records should be excluded
	 *
	 * @return boolean
	 */
	public function getExcludeAlreadyDisplayedNews() {
		return $this->excludeAlreadyDisplayedNews;
	}
	
	/**
	 * Set exclude from rss
	 *
	 * @param boolean $excludeFromRss
	 * @return void
	 */
	public function setExcludeFromRss($excludeFromRss) {
		$this->excludeFromRss = $excludeFromRss;
	}

	/**
	 * Get exclude from rss
	 *
	 * @return boolean
	 */
	public function getExcludeFromRss() {
		return $this->excludeFromRss;
	}
	
	/**
	 * Set types
	 *
	 * @param array $types
	 * @return void
	 */
	public function setTypes($types) {
		$this->types = $types;
	}

	/**
	 * Get types
	 *
	 * @return array
	 */
	public function getTypes() {
		return $this->types;
	}
	
	/**
	 * Set type
	 *
	 * @param string $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Set char
	 *
	 * @param string $char
	 * @return void
	 */
	public function setChar($char) {
		$this->char = $char;
	}

	/**
	 * Get char
	 *
	 * @return string
	 */
	public function getChar() {
		return $this->char;
	}
	
	/**
	 * Set char list
	 *
	 * @param array $charList
	 * @return void
	 */
	public function setCharList($charList) {
		$this->charList = $charList;
	}

	/**
	 * Get char list
	 *
	 * @return array
	 */
	public function getCharList() {
		return $this->charList;
	}
	
	/**
	 * Set privacy
	 *
	 * @param integer $privacy
	 * @return void
	 */
	public function setPrivacy($privacy) {
		$this->privacy = $privacy;
	}

	/**
	 * Get privacy
	 *
	 * @return integer
	 */
	public function getPrivacy() {
		return $this->privacy;
	}
}
