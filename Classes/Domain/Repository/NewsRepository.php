<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News repository with all the callable functionality
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_Domain_Repository_NewsRepository extends Tx_MooxNews_Domain_Repository_AbstractDemandedRepository {

	/**
	 * Returns a category constraint created by
	 * a given list of categories and a junction string
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param  array $categories
	 * @param  string $conjunction
	 * @param  boolean $includeSubCategories
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createCategoryConstraint(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query, $categories, $conjunction, $includeSubCategories = FALSE) {
		$constraint = NULL;
		$categoryConstraints = array();

		// If "ignore category selection" is used, nothing needs to be done
		if (empty($conjunction)) {
			return $constraint;
		}

		if (!is_array($categories)) {
			$categories = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $categories, TRUE);
		}
		foreach ($categories as $category) {
			if ($includeSubCategories) {
				$subCategories = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', Tx_MooxNews_Service_CategoryService::getChildrenCategories($category, 0, '', TRUE), TRUE);
				$subCategoryConstraint = array();
				$subCategoryConstraint[] = $query->contains('categories', $category);
				if (count($subCategories) > 0) {
					foreach ($subCategories as $subCategory) {
						$subCategoryConstraint[] = $query->contains('categories', $subCategory);
					}
				}
				if ($subCategoryConstraint) {
					$categoryConstraints[] = $query->logicalOr($subCategoryConstraint);
				}

			} else {
				$categoryConstraints[] = $query->contains('categories', $category);
			}
		}

		if ($categoryConstraints) {
			switch (strtolower($conjunction)) {
				case 'or':
					$constraint = $query->logicalOr($categoryConstraints);
					break;
				case 'notor':
					$constraint = $query->logicalNot($query->logicalOr($categoryConstraints));
					break;
				case 'notand':
					$constraint = $query->logicalNot($query->logicalAnd($categoryConstraints));
					break;
				case 'and':
				default:
					$constraint = $query->logicalAnd($categoryConstraints);
			}
		}
		
		return $constraint;
	}

	/**
	 * Returns an array of constraints created from a given demand object.
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param Tx_MooxNews_Domain_Model_DemandInterface $demand
	 * @throws UnexpectedValueException
	 * @throws InvalidArgumentException
	 * @throws Exception
	 * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
	 */
	protected function createConstraintsFromDemand(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query, Tx_MooxNews_Domain_Model_DemandInterface $demand) {		
		
		$constraints = array();
		
		if(is_array($demand->getTypes()) && count($demand->getTypes())){
			$constraints[] = $query->in('type', $demand->getTypes());		
		} elseif($demand->getType()!=""){
			$constraints[] = $query->equals('type', (string)$demand->getType());		
		}
		
		if($demand->getIncludeOnlySelectedNewsInRss()){
			$constraints[] = $query->equals('shareRss', 1);		
		}
			
		if ($demand->getCategories() && $demand->getCategories() !== '0') {			
			$constraints[] = $this->createCategoryConstraint(
				$query,
				$demand->getCategories(),
				$demand->getCategoryConjunction(),
				$demand->getIncludeSubCategories()
			);
		}

		if ($demand->getAuthor()) {
			$constraints[] = $query->equals('author', $demand->getAuthor());
		}
		
		$listViewSearchFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields']['moox_news']['default'];				
		
		if ($demand->getQuery()) {
			$listViewSearchFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields']['moox_news']['default'];
			if(is_array($demand->getTypes())){
				$type = reset($demand->getTypes());
			}
			if($type!="" && isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields'][$type]['default']) && $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields'][$type]['default']!=""){			
				$listViewSearchFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields'][$type]['default'];		
			} else {
				$listViewSearchFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields']['moox_news']['default'];
			}
			if($listViewSearchFields!=""){
				$listViewSearchFields = explode(",",$listViewSearchFields);
			} else {
				$listViewSearchFields = array("title");
			}
			$queryConstraints = array();
			foreach($listViewSearchFields AS $listViewSearchField){
				$queryConstraints[] = $query->like($listViewSearchField, "%".$demand->getQuery()."%");
			}
			$constraints[] = $query->logicalOr($queryConstraints);
		}
		
		if ($demand->getChar()) {
			if($demand->getChar()!="other"){				
				$char = substr($demand->getChar(),0,1);
				if(in_array($char,$demand->getCharList())){
					$constraints[] = $query->like("title", $char."%");
				}
			} else {
				$charConstraints = array();
				foreach($demand->getCharList() AS $char){
					if($char!="all" && $char!="other"){
						$charConstraints[] = $query->logicalNot($query->like("title", $char."%"));
					}
				}				
				if(count($charConstraints)){
					$constraints[] = $query->logicalAnd($charConstraints);
				}
			}
		}
		
		// archived
		if ($demand->getArchiveRestriction() == 'archived') {
			$constraints[] = $query->logicalAnd(
				$query->lessThan('archive', $GLOBALS['EXEC_TIME']),
				$query->greaterThan('archive', 0)
			);
		} elseif ($demand->getArchiveRestriction() == 'active') {
			$constraints[] = $query->logicalOr(
				$query->greaterThanOrEqual('archive', $GLOBALS['EXEC_TIME']),
				$query->equals('archive', 0)
			);
		}
		
		if($demand->getDateField()=="type" && is_array($demand->getTypes()) && count($demand->getTypes())==1){
		//if($demand->getDateField()=="type" && is_array($demand->getTypes())){
			$type = reset($demand->getTypes());
			if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['dateTimeDefaultField'][$type])&& $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['dateTimeDefaultField'][$type]!=""){			
				$dateTimeDefaultField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['dateTimeDefaultField'][$type];		
			} else {
				$dateTimeDefaultField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['dateTimeDefaultField']['moox_news'];
			}
			$demand->setDateField($dateTimeDefaultField);
		} elseif($demand->getDateField()=="type") {
			$dateTimeDefaultField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['dateTimeDefaultField']['moox_news'];			
			$demand->setDateField($dateTimeDefaultField);
		}

		// Time restriction greater than or equal
		$timeRestrictionField = $demand->getDateField();
		$timeRestrictionField = (empty($timeRestrictionField)) ? 'datetime' : $timeRestrictionField;
		
		if ($demand->getTimeRestriction()) {
			$timeLimit = 0;
			// integer = timestamp
			if (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($demand->getTimeRestriction())) {
				$timeLimit = $GLOBALS['EXEC_TIME'] - $demand->getTimeRestriction();
			} else {
				// try to check strtotime
				$timeFromString = strtotime($demand->getTimeRestriction());

				if ($timeFromString) {
					$timeLimit = $timeFromString;
				} else {
					throw new Exception('Time limit Low could not be resolved to an integer. Given was: ' . htmlspecialchars($timeLimit));
				}
			}

			$constraints[] = $query->greaterThanOrEqual(
				$timeRestrictionField,
				$timeLimit
			);
		}

		// Time restriction less than or equal
		if ($demand->getTimeRestrictionHigh()) {
			$timeLimit = 0;
			// integer = timestamp
			if (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($demand->getTimeRestrictionHigh())) {
				$timeLimit = $GLOBALS['EXEC_TIME'] + $demand->getTimeRestrictionHigh();
			} else {
				// try to check strtotime
				$timeFromString = strtotime($demand->getTimeRestrictionHigh());

				if ($timeFromString) {
					$timeLimit = $timeFromString;
				} else {
					throw new Exception('Time limit High could not be resolved to an integer. Given was: ' . htmlspecialchars($timeLimit));
				}
			}

			$constraints[] = $query->lessThanOrEqual(
				$timeRestrictionField,
				$timeLimit
			);
		}

		// top news
		if ($demand->getTopNewsRestriction() == 1) {
			$constraints[] = $query->equals('istopnews', 1);
		} elseif ($demand->getTopNewsRestriction() == 2) {
			$constraints[] = $query->equals('istopnews', 0);
		}
		
		// privacy
		if ($demand->getPrivacy() === "0") {
			$constraints[] = $query->equals('privacy', 0);
		} elseif ($demand->getPrivacy() === "1") {
			$constraints[] = $query->equals('privacy', 1);		
		}
		
		// exclude from rss
		if ($demand->getExcludeFromRss() == 1) {
			$constraints[] = $query->equals('excludeFromRss', 0);			
		} 

		// storage page
		if ($demand->getStoragePage() != 0) {
			$pidList = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $demand->getStoragePage(), TRUE);
			$constraints[] = $query->in('pid', $pidList);
		}
		
		// datepicker filter
		$datepickerConstraints = array();
		if ($demand->getDateFrom()!="") {
			$dateFrom = strtotime($demand->getDateFrom());
			if($dateFrom>0){
				$dateFrom = mktime(0,0,0,date("n",$dateFrom),date("j",$dateFrom),date("Y",$dateFrom));
				$datepickerConstraints[] = $query->greaterThanOrEqual($timeRestrictionField,$dateFrom);
			}
		}
		if ($demand->getDateTo()!="") {
			$dateTo = strtotime($demand->getDateTo());
			if($dateTo>0){
				$dateTo = mktime(23,59,59,date("n",$dateTo),date("j",$dateTo),date("Y",$dateTo));
				$datepickerConstraints[] = $query->lessThanOrEqual($timeRestrictionField,$dateTo);
			}
		}
		if(count($datepickerConstraints)>1){
			$constraints[] = $query->logicalAnd($datepickerConstraints);
		} elseif(count($datepickerConstraints)>0){
			$constraints[] = $datepickerConstraints[0];
		}
		
		// month & year OR year only
		if (is_numeric($demand->getYear())) {			
			if($demand->getYear() > 0) {
				if($demand->getMonth() > 0) {
					if ($demand->getDay() > 0) {
						$begin = mktime(0, 0, 0, $demand->getMonth(), $demand->getDay(), $demand->getYear());
						$end = mktime(23, 59, 59, $demand->getMonth(), $demand->getDay(), $demand->getYear());
					} else {
						$begin = mktime(0, 0, 0, $demand->getMonth(), 1, $demand->getYear());
						$end = mktime(23, 59, 59, ($demand->getMonth() + 1), 0, $demand->getYear());
					}
				} else {
					$begin = mktime(0, 0, 0, 1, 1, $demand->getYear());
					$end = mktime(23, 59, 59, 12, 31, $demand->getYear());
				}
				$constraints[] = $query->logicalAnd(
					$query->greaterThanOrEqual($timeRestrictionField, $begin),
					$query->lessThanOrEqual($timeRestrictionField, $end)
				);	
			} else {
				$constraints[] = $query->equals($timeRestrictionField, 0);
			}
		}
		
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields'])){
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields'] AS $extkey => $queryFields){
				if($extkey!="moox_news"){
					foreach($queryFields AS $fieldname => $queryField){
						if($fieldname!="default" && $queryField!=""){
							$fieldname = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
							$getCall = "get".ucfirst($fieldname);
							if(method_exists($demand, $getCall)){
								$fieldvalue = $demand->$getCall();
								if($fieldvalue!=""){
									$listViewSearchFields = explode(",",$queryField);									
									$queryConstraints = array();
									foreach($listViewSearchFields AS $listViewSearchField){
										$queryConstraints[] = $query->like($listViewSearchField, "%".$fieldvalue."%");
									}									
									$constraints[] = $query->logicalOr($queryConstraints);
								}
							}
						}
					}
				}
			}
		}		
		
		// set constraints based on extended filter fields from external extensions
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){			
			if($field['addToMooxNewsFrontendDemand']){
				$fieldname = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
				$getCall = "get".ucfirst($fieldname);				
				if(method_exists($demand, $getCall)){
					$fieldvalue = $demand->$getCall();
					if(trim($fieldvalue)!="" && trim($fieldvalue)!="all"){
						if(in_array($field['config']['type'],array("input","text")) && !in_array($field['addToMooxNewsFrontendFilter']['type'],array("select"))){
							if(is_array($field['addToMooxNewsFrontendDemand']) && in_array($field['addToMooxNewsFrontendDemand']['operator'],array("equals","like","startsWith","endsWith"))){
								if($field['addToMooxNewsFrontendDemand']['operator']=="equals"){
									$constraints[] = $query->equals($fieldname, $fieldvalue);
								} elseif($field['addToMooxNewsFrontendDemand']['operator']=="like"){
									$constraints[] = $query->like($fieldname, '%' . $fieldvalue . '%');
								} elseif($field['addToMooxNewsFrontendDemand']['operator']=="startsWith"){
									$constraints[] = $query->like($fieldname, $fieldvalue . '%');
								} elseif($field['addToMooxNewsFrontendDemand']['operator']=="endsWith"){
									$constraints[] = $query->like($fieldname, '%' . $fieldvalue);
								}
							} else { 
								$constraints[] = $query->like($fieldname, '%' . $fieldvalue . '%');
							}
						} else {
							if(in_array($field['addToMooxNewsFrontendFilter']['type'],array("select")) && $fieldvalue=="empty"){
								$constraints[] = $query->logicalOr(
									$query->equals($fieldname, ""),
									$query->equals($fieldname, NULL)
								);
							} else {
								$constraints[] = $query->equals($fieldname, $fieldvalue);
							}
						}
					}
				}							
			}
		}
		
		// sys language
		if (TYPO3_MODE == 'BE') {
			$query->getQuerySettings()->setRespectSysLanguage(false);
			$constraints[] = $query->equals('t3_origuid', 0);
			if (is_numeric($demand->getSysLanguageUid())) {
				$constraints[] = $query->equals('sys_language_uid', $demand->getSysLanguageUid());			
			}
		}
		
		// Tags
		if(is_array($demand->getTags()) && count($demand->getTags())){
			$tags = $demand->getTags();
			if(count($tags)>1){
				$tagConstraints = array();
				foreach($tags AS $tag){
					$tagConstraints[] = $query->contains('tags', $tag);
				}
				$constraints[] = $query->logicalOr($tagConstraints);	
			} else {
				$constraints[] = $query->contains('tags', $tags[0]);		
			}
		} elseif($demand->getTag()!=""){
			$constraints[] = $query->contains('tags', $demand->getTag());	
		}
		
		// Targets
		if(is_array($demand->getTargets()) && count($demand->getTargets())){
			$targets = $demand->getTargets();
			if(count($targets)>1){
				$targetConstraints = array();
				foreach($targets AS $target){
					$targetConstraints[] = $query->contains('targets', $target);
				}
				$constraints[] = $query->logicalOr($targetConstraints);	
			} else {
				$constraints[] = $query->contains('targets', $targets[0]);		
			}
		} elseif($demand->getTarget()!=""){
			$constraints[] = $query->contains('targets', $demand->getTarget());	
		}
		
		// mailer frequency
		if (TYPO3_MODE == 'BE') {
			if (is_numeric($demand->getMailerFrequency())) {
				$constraints[] = $query->equals('mailerFrequency', $demand->getMailerFrequency());			
			}
		}

		// Search
		$searchConstraints = $this->getSearchConstraints($query, $demand);
		if (!empty($searchConstraints)) {
			$constraints[] = $query->logicalAnd($searchConstraints);
		}

		// Exclude already displayed
		if ($demand->getExcludeAlreadyDisplayedNews() && isset($GLOBALS['EXT']['moox_news']['alreadyDisplayed']) && !empty($GLOBALS['EXT']['moox_news']['alreadyDisplayed'])) {
			$constraints[] = $query->logicalNot(
				$query->in(
					'uid',
					$GLOBALS['EXT']['moox_news']['alreadyDisplayed']
				)
			);
		}

		// Clean not used constraints
		foreach ($constraints as $key => $value) {
			if (is_null($value)) {
				unset($constraints[$key]);
			}
		}
		
		/*
		$parser = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Storage\\Typo3DbQueryParser');  
		$queryParts = $parser->parseQuery($query); 
		\TYPO3\CMS\Core\Utility\DebugUtility::debug($queryParts, 'Query');
		exit();
		*/
		
		return $constraints;
	}

	/**
	 * Returns an array of orderings created from a given demand object.
	 *
	 * @param Tx_MooxNews_Domain_Model_DemandInterface $demand
	 * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
	 */
	protected function createOrderingsFromDemand(Tx_MooxNews_Domain_Model_DemandInterface $demand) {
		$orderings = array();
		if ($demand->getTopNewsFirst()) {
			$orderings['istopnews'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
		}
		if (Tx_MooxNews_Utility_Validation::isValidOrdering($demand->getOrder(), $demand->getOrderByAllowed())) {
			$orderList = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $demand->getOrder(), TRUE);
						
			if (!empty($orderList)) {
				// go through every order statement
				foreach ($orderList as $orderItem) {										
					list($orderField, $ascDesc) = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(' ', $orderItem, TRUE);
					// count == 1 means that no direction is given
					if ($ascDesc) {
						$orderings[$orderField] = ((strtolower($ascDesc) == 'desc') ?
							\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING :
							\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);
					} else {
						$orderings[$orderField] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
					}
					$orderFieldUnderscored = \TYPO3\CMS\Core\Utility\GeneralUtility::camelCaseToLowerCaseUnderscored($orderField);
					if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$orderFieldUnderscored]['addToMooxNewsFrontendSorting']['additionalSorting'])){
						$additionalSorting = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$orderFieldUnderscored]['addToMooxNewsFrontendSorting']['additionalSorting'];
						if($additionalSorting!=''){
							list($orderField, $ascDesc) = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(' ', $additionalSorting, TRUE);
							// count == 1 means that no direction is given
							if(!isset($orderings[$orderField])){
								if ($ascDesc) {
									$orderings[$orderField] = ((strtolower($ascDesc) == 'desc') ?
										\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING :
										\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);
								} else {
									$orderings[$orderField] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
								}
							}
						}
					}
				}
				if(!isset($orderings['title'])){
					$orderings['title'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
				}
			}
		}
		
		return $orderings;
	}

	/**
	 * Find first news by import and source id
	 *
	 * @param string $importSource import source
	 * @param integer $importId import id
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findOneByImportSourceAndImportId($importSource, $importId) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('importSource', $importSource),
				$query->equals('importId', $importId)
			))->execute()->getFirst();
	}
	
	/**
	 * Find first news by grabber remote uid
	 *
	 * @param string $grabberRemoteUid grabber remote uid
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findOneByGrabberRemoteUid($grabberRemoteUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->equals('grabberRemoteUid', $grabberRemoteUid)
		)->execute()->getFirst();
	}
	
	/**
	 * Find news that are not indexed yet or index is not up-to-date 
	 *	
	 * @param array $pids pids
	 * @param integer $limit limit
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findNewsToIndex($pids = array(),$limit = 0) {
		
		if($pids){
			$pids = (is_array($pids))?$pids:array($pids);
			$pidStmt = "pid IN (".implode(",",$pids).") AND ";
		} 
		
		$preselect['fields'] 	= 'uid';
		$preselect['table']		= 'tx_mooxnews_domain_model_news';
		$preselect['where'] 	= $pidStmt.'deleted=0 AND indexed<tstamp';
	
		$newsUids = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(	$preselect['fields'], 
																$preselect['table'], 
																$preselect['where'],
																'',
																'',
																($limit>0)?$limit:''
															);
		
		$uids = array();
		foreach ($newsUids as $newsUid) {
			$uids[] = $newsUid['uid'];
		}
		
		$result = array();
		
		if(count($uids)>0){
			$query = $this->createQuery();		
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
			$query->getQuerySettings()->setRespectSysLanguage(FALSE);
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);		
			$query->matching(
				$query->in('uid', $uids)
			);				
			$result = $query->execute();
		}
		
		return $result;		
	}
	
	/**
	 * Find all news  
	 *	
	 * @param array $pids pids
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findAllNewsToIndex($pids = array()) {
		
		$query 		= $this->createQuery();
		
		if($pids){
			$pids = (is_array($pids))?$pids:array($pids);
			$query->getQuerySettings()->setRespectStoragePage(TRUE);
			$query->getQuerySettings()->setStoragePageIds($pids);
			$pidStmt = "pid IN (".implode(",",$pids).") AND ";
		} else {
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}
		
		$preselect['fields'] 	= 'uid';
		$preselect['table']		= 'tx_mooxnews_domain_model_news';
		$preselect['where'] 	= $pidStmt.'deleted=0 AND indexed<tstamp';
	
		$newsUids = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(	$preselect['fields'], 
																$preselect['table'], 
																$preselect['where']
															);
		
		$uids = array();
		foreach ($newsUids as $newsUid) {
			$uids[] = $newsUid['uid'];
		}
		
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		
		$query->matching($query->in('uid', $uids));
				
		$result = $query->execute();
		
		return $result;		
	}
	
	/**
	 * Find news by mailer frequency and pid(list)
	 *	
	 * @param array $pids pids
	 * @param integer $frequenzy mailer frequenzy
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findNewsByMailerFrequenzy($pids = array(),$frequency = 0) {
		
		if($frequency<1){
			$frequency = 99;
		}
		
		$pids = (is_array($pids))?$pids:array($pids);

		$query 	= $this->createQuery();	
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		
		$selectStmt = 'SELECT * FROM tx_mooxnews_domain_model_news WHERE pid IN ('.implode(",",$pids).') AND deleted=0 AND mailer_frequency='.$frequency;
		
		$query->statement($selectStmt);
		
		return $query->execute();		
	}
	
	/**
	 * Find news by mailer frequency and pid(list) and categories
	 *	
	 * @param array $pids pids
	 * @param array $categories categories
	 * @param integer $frequenzy mailer frequenzy
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findNewsByMailerFrequenzyAndCategories($pids = array(),$categories = array(),$frequency = 0) {
		
		if($frequency<1){
			$frequency = 99;
		}
		
		$pids 		= (is_array($pids))?$pids:array($pids);		
		$categories = (is_array($categories))?$categories:array($categories);
		
		$query = $this->createQuery();
		
		$constraints = array();
		foreach($categories AS $category){
			$constraints[] = $query->contains('categories', $category);
		}
		
		$query->getQuerySettings()->setStoragePageIds($pids);
		if(count($constraints)){
			$query->matching(
				$query->logicalAnd(
					$query->equals('mailer_frequency', $frequency),
					$query->logicalOr($constraints)
				)
			);
		} else {
			$query->matching(
				$query->equals('mailer_frequency', $frequency)
			);
		}
		return $query->execute();				
	}
	
	/**
	 * Find news by filter
	 *	
	 * @param array $filter filter	
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findNewsByFilter($filter = array()) {
		
		$constraints = array();
		
		if($filter['mailerFrequenzy']<1){
			$filter['mailerFrequenzy'] = 99;
		}
		
		$filter['newsPids'] 		= (is_array($filter['newsPids']))?$filter['newsPids']:array($filter['newsPids']);		
		$filter['categoriesUids'] 	= (is_array($filter['categoriesUids']))?$filter['categoriesUids']:array($filter['categoriesUids']);
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setStoragePageIds($filter['newsPids']);
		
		if($filter['mailer']){
			$constraints[] = $query->equals('mailer_frequency', $filter['mailerFrequenzy']);
		}
		
		if($filter['sendRestrictedNewsOnly']){
			$constraints[] = $query->logicalNot($query->equals('feGroup', ""));
		} 
		
		if($filter['rss']){
			$constraints[] = $query->logicalNot($query->equals('excludeFromRss', 1));			
		}
		
		if($filter['twitter']){
			$constraints[] = $query->equals('pushToTwitter', 1);
			$constraints[] = $query->logicalNot($query->equals('pushToTwitterText', ""));
		} 
		
		$categoryConstraints = array();
		foreach($filter['categoriesUids'] AS $category){
			$categoryConstraints[] = $query->contains('categories', $category);
		}
		if(count($categoryConstraints)){
			$constraints[] = $query->logicalOr($categoryConstraints);
		}
		
		return $query->matching(
					$query->logicalAnd($constraints)
				)->execute();				
	}		
	
	/**
	 * Find news by pid(list)
	 *	
	 * @param array $pids pids	
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findNewsByPidList($pids = array()) {
		
		$pids 		= (is_array($pids))?$pids:array($pids);		
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setStoragePageIds($pids);
		
		return $query->execute();
				
	}
	
	/**
	 * all years in frontend news dates
	 *	
	 * @param array $storagePage
	 * @return array
	 */
	public function findAllYears($storagePage = 0) {
		
		if(!is_array($storagePage)){
			$storagePage = array($storagePage);
		}
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);		
		$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePage).")";		
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$sysLanguageUid = $query->getQuerySettings()->getSysLanguageUid();
		if(is_numeric($sysLanguageUid)){
			$sysLanguageUidStmt = " AND sys_language_uid=".$sysLanguageUid;
		}		
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$query->statement('SELECT DISTINCT FROM_UNIXTIME(`datetime`, "%Y") AS "year" FROM `tx_mooxnews_domain_model_news` WHERE deleted=0'.$storagePageIdsStmt.$sysLanguageUidStmt.' AND hidden=0 ORDER BY `datetime` DESC');
		$result = $query->execute();
		return $result;		
	}
	
	/**
	 * all years in all news dates
	 *	
	 * @param boolean $respectEnableFields if set to false, hidden records are shown	
	 * @return array
	 */
	public function findAllBackendYears($respectEnableFields = TRUE) {
				
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
		$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$query->statement('SELECT DISTINCT FROM_UNIXTIME(`datetime`, "%Y") AS "year" FROM `tx_mooxnews_domain_model_news` WHERE deleted=0 ORDER BY `datetime` DESC');
		$result = $query->execute();
		return $result;
				
	}
	
	/**
	 * find extended filter items
	 *	
	 * @param array $field
	 * @param array $storagePage
	 * @param string $table
	 * @param string $where
	 * @param array $dependFields
	 * @return array
	 */
	public function findExtendedFilterItems($field,$storagePage,$table,$where = "",$dependFields = array()) {
		
		$result = array();
		
		if($table!=""){
			if(!is_array($storagePage)){
				$storagePage = array($storagePage);
			}
			
			$existingFilterValues = array();
			if(count($dependFields)){
				$existingFilterValues = $this->findExtendedFilterUniqueValues($field,$storagePage,$dependFields);
				
				if(count($existingFilterValues)){
					
					$existingFilterValuesStatic 	= array();
					$existingFilterValuesDb 		= array();
					
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$field];
					$items = $field['config']['items'];
					
					foreach($existingFilterValues AS $existingFilterValue){
						$inStatic = false;
						foreach($items AS $item){
							if($item[1]==""){
								$item[1] = "empty";
								if($field['addToMooxNewsFrontendFilter']['selectEmptyLabel']!==""){
									$item[0] = $field['addToMooxNewsFrontendFilter']['selectEmptyLabel'];
								}
							}
							if($existingFilterValue == $item[1]){
								$existingFilterValuesStatic[] = array("uid" => $item[1], "title" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($item[0], $field['extkey']));
								$inStatic = true;
							}							
						}
						if(!$inStatic && is_numeric($existingFilterValue)){
							$existingFilterValuesDb[] = $existingFilterValue;
						}
					}
					
					if(count($existingFilterValuesDb)){
						$dependStmt = " AND uid IN (".implode(",",$existingFilterValuesDb).")";
					}
				}
			}			
			
			$query = $this->createQuery();
			
			$query->getQuerySettings()->setRespectStoragePage(FALSE);		
			$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePage).")";
			$query->getQuerySettings()->setRespectSysLanguage(TRUE);
			$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
			$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
			$query->statement('SELECT uid,title FROM `'.$table.'` WHERE 1=1'.$storagePageIdsStmt.$dependStmt.' '.$where);
			$resultDb = $query->execute();
			
			if(is_array($existingFilterValuesStatic)){
				$result = $existingFilterValuesStatic;
			} 
			foreach($resultDb AS $item){
				$result[] = array("uid" => $item['uid'], "title" => $item['title']);
			}
		}
		
		return $result;			
	}
	
	/**
	 * find extended filter unique values
	 *	
	 * @param array $field
	 * @param array $storagePage
	 * @param array $dependFields
	 * @return array
	 */
	public function findExtendedFilterUniqueValues($field,$storagePage,$dependFields = array()) {
		
		$values = array();
		
		if($field!=""){
			if(!is_array($storagePage)){
				$storagePage = array($storagePage);
			}
			
			$where = "	AND starttime <= UNIX_TIMESTAMP() 
						AND (
							endtime >= UNIX_TIMESTAMP() 
							OR endtime=0
						) 
						AND deleted = 0 
						AND hidden = 0	";
			
			$depend = "";
			if(count($dependFields)){
				foreach($dependFields AS $dependField => $dependFieldValue){
					if($dependFieldValue!="all"){
						if($dependFieldValue=="empty"){
							$depend .= ' AND ('.$dependField.'="" OR '.$dependField.' IS NULL)';
						} else {
							$depend .= ' AND '.$dependField.'="'.$dependFieldValue.'"';
						}
					}
				}
			}
					
			$ordering = " ORDER BY ".$field." ASC";
			
			$query = $this->createQuery();
			
			$query->getQuerySettings()->setRespectStoragePage(FALSE);		
			$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePage).")";
			$query->getQuerySettings()->setRespectSysLanguage(TRUE);
			$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
			$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
			
			$stmt = 'SELECT DISTINCT '.$field.' FROM `tx_mooxnews_domain_model_news` WHERE 1=1'.$storagePageIdsStmt.' '.$where.$depend.$ordering;
			
			$query->statement($stmt);
			$result = $query->execute();
			
			foreach($result AS $value){
				if($value[$field]=="" || $value[$field]==NULL){
					if(!in_array("empty",$values)){
						$values[] = "empty";
					}
				} else {
					$values[] = $value[$field];
				}
			}			
		}
		
		return $values;			
	}
	
	/**
	 * all sys language uids
	 *	
	 * @param boolean $respectEnableFields if set to false, hidden records are shown	
	 * @return array
	 */
	public function findAllSysLanguageUids($respectEnableFields = TRUE) {
		
		$query = $this->createQuery();
		
		if(count($query->getQuerySettings()->getStoragePageIds())){
			$pidStmt = "WHERE pid IN (".implode(",",$query->getQuerySettings()->getStoragePageIds()).") ";
		}
		
		$query->getQuerySettings()->setRespectStoragePage(TRUE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
		$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$query->statement('SELECT DISTINCT `sys_language_uid` FROM `tx_mooxnews_domain_model_news` '.$pidStmt.'ORDER BY `sys_language_uid` ASC');
		$result = $query->execute();
		return $result;
				
	}
	
	/**
	 * get max sort
	 *	
	 * @param int $pid	
	 * @return array
	 */
	public function findMaxSortingByPid($pid) {
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(TRUE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$query->statement('SELECT sorting FROM tx_mooxnews_domain_model_news WHERE pid='.$pid.' AND deleted=0 ORDER BY sorting DESC LIMIT 1');
		$result = $query->execute();		
		return (int)$result[0]['sorting'];
				
	}

	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param integer $uid id of record
	 * @param boolean $respectEnableFields if set to false, hidden records are shown
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findByUid($uid, $respectEnableFields = TRUE) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}

	/**
	 * Get the count of news records by month/year and
	 * returns the result compiled as array
	 *
	 * @param Tx_MooxNews_Domain_Model_DemandInterface $demand
	 * @return array
	 */
	public function countByDate(Tx_MooxNews_Domain_Model_DemandInterface $demand) {
		$data = array();
		$sql = $this->findDemandedRaw($demand);

		// Get the month/year into the result
		$field = $demand->getDateField();
		$field = empty($field) ? 'datetime' : $field;

		$sql = 'SELECT FROM_UNIXTIME(' . $field . ', "%m") AS "_Month",' .
			' FROM_UNIXTIME(' . $field . ', "%Y") AS "_Year" ,' .
			' count(FROM_UNIXTIME(' . $field . ', "%m")) as count_month,' .
			' count(FROM_UNIXTIME(' . $field . ', "%y")) as count_year' .
			' FROM tx_mooxnews_domain_model_news ' . substr($sql, strpos($sql, 'WHERE '));

		if (TYPO3_MODE === 'FE') {
			$sql .= $GLOBALS['TSFE']->sys_page->enableFields('tx_mooxnews_domain_model_news');
		} else {
			$sql .= \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields('tx_mooxnews_domain_model_news') .
				\TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause('tx_mooxnews_domain_model_news');
		}

		// strip unwanted order by
		$sql = $GLOBALS['TYPO3_DB']->stripOrderBy($sql);

		// group by custom month/year fields
		$orderDirection = strtolower($demand->getOrder());
		if ($orderDirection !== 'desc' && $orderDirection != 'asc') {
			$orderDirection = 'asc';
		}
		$sql .= ' GROUP BY _Month, _Year ORDER BY _Year ' . $orderDirection . ', _Month ' . $orderDirection;

		$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$data['single'][$row['_Year']][$row['_Month']] = $row['count_month'];
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($res);

		// Add totals
		if (is_array($data['single'])) {
			foreach ($data['single'] as $year => $months) {
				$countOfYear = 0;
				foreach ($months as $month) {
					$countOfYear += $month;
				}
				$data['total'][$year] = $countOfYear;
			}
		}

		return $data;
	}
	
	/**
	* Debugs a SQL query from a QueryResult
	*
	* @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult
	* @param boolean $explainOutput
	* @return void
	*/
	public function debugQuery(\TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult, $explainOutput = FALSE){
	  $GLOBALS['TYPO3_DB']->debugOuput = 2;
	  if($explainOutput){
		$GLOBALS['TYPO3_DB']->explainOutput = true;
	  }
	  $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
	  $queryResult->toArray();
	  //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($GLOBALS['TYPO3_DB']->debug_lastBuiltQuery);
	  echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
	  
	 
	  $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = false;
	  $GLOBALS['TYPO3_DB']->explainOutput = false;
	  $GLOBALS['TYPO3_DB']->debugOuput = false;
	}
	
	/**
	 * Find news translations
	 *	
	 * @param int $l10nParent l10n parent
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function findTranslations($l10nParent) {
		
		$news = array();
		
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			"*",
			"tx_mooxnews_domain_model_news",
			'deleted=0 AND l10n_parent='.$l10nParent,
			'',
			'sys_language_uid ASC',
			''
		);
		while($newsItem = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
            $news[] = $newsItem;
        }
		
		return $news;
	}
	
	/**
	 * Get the search constraints
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param Tx_MooxNews_Domain_Model_DemandInterface $demand
	 * @return array
	 * @throws UnexpectedValueException
	 */
	protected function getSearchConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query, Tx_MooxNews_Domain_Model_DemandInterface $demand) {
		$constraints = array();
		if ($demand->getSearch() === NULL) {
			return $constraints;
		}

		/* @var $searchObject Tx_MooxNews_Domain_Model_Dto_Search */
		$searchObject = $demand->getSearch();

		$searchSubject = $searchObject->getSubject();
		if (!empty($searchSubject)) {
			$searchFields = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $searchObject->getFields(), TRUE);
			$searchConstraints = array();

			if (count($searchFields) === 0) {
				throw new UnexpectedValueException('No search fields defined', 1318497755);
			}


			foreach ($searchFields as $field) {
				if (!empty($searchSubject)) {
					$searchConstraints[] = $query->like($field, '%' . $searchSubject . '%');
				}
			}

			if (count($searchConstraints)) {
				$constraints[] = $query->logicalOr($searchConstraints);
			}
		}

		$minimumDate = strtotime($searchObject->getMinimumDate());
		if ($minimumDate) {
			$field = $searchObject->getDateField();
			if (empty($field)) {
				throw new UnexpectedValueException('No date field is defined', 1396348732);
			}
			$constraints[] = $query->greaterThanOrEqual($field, $minimumDate);
		}
		$maximumDate = strtotime($searchObject->getMaximumDate());
		if ($maximumDate) {
			$field = $searchObject->getDateField();
			if (empty($field)) {
				throw new UnexpectedValueException('No date field is defined', 1396348733);
			}
			$constraints[] = $query->lessThanOrEqual($field, $maximumDate);
		}
		
		return $constraints;
	}

}
