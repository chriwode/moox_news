<?php
namespace DCNGmbH\MooxNews\Tasks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Set news access from parent category
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SetNewsAccessTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {		
	
	/**
	 * PIDs der Seite/Ordner in dem die News gespeichert sind die versendet werden sollen
	 *
	 * @var string
	 */
	public $newsPids;
	
	/**
	 * Bestehende News Berechtigungen ignorieren
	 *
	 * @var boolean
	 */
	public $ignore;
	
	
	/**
	 * Prepares the mailer job.
	 *
	 * @return	boolean	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute() {									
		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		
		$newsRepository 	= $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		
		$pids = explode(",",$this->newsPids);
		
		$newsItems = $newsRepository->findNewsByPidList($pids);
		
		foreach($newsItems AS $news){
			$newsFirstCategory 	= $news->getCategories()->current();
			
			// WORKAROUD BY DOMINIC MARTIN: changed object variable to public for direct access, because access by "getter" fails.
			// $categoryFeGroups = $newsFirstCategory->getFeGroup();
			$categoryFeGroups = $newsFirstCategory->feGroup;
			
			if($categoryFeGroups){
				if($this->ignore || in_array($news->getFeGroup(),array(-1,-2))){
					$news->setFeGroup($categoryFeGroups);
				} else {
					$newsFeGroups 		= explode(",",$news->getFeGroup());
					$categoryFeGroups 	= explode(",",$categoryFeGroups);
					$newNewsFeGroups	= $categoryFeGroups;
					foreach($newsFeGroups AS $group){
						if(!in_array($group,$newNewsFeGroups)){
							$newNewsFeGroups[] = $group;
						}
					}
					$news->setFeGroup(implode(",",$newNewsFeGroups));
				}
				$newsRepository->update($news);				
			}						
		}
		
		$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
		
		$executionSucceeded = true;
		
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation() {
		
		$info = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_setnewsaccesstask.news_pids_label', 'moox_news' ).": ".$this->getNewsPids();
				
		return $info;
	}
	
	/**
	 * Returns the news pids
	 *
	 * @return string
	 */
	public function getNewsPids() {
		return $this->newsPids;
	}

	/**
	 * Set the news pids
	 *
	 * @param string $newsPids news pids
	 * @return void
	 */
	public function setNewsPids($newsPids) {
		$this->newsPids = $newsPids;
	}
	
	/**
	 * Returns ignore
	 *
	 * @return boolean
	 */
	public function getIgnore() {
		return $this->ignore;
	}

	/**
	 * Set ignore
	 *
	 * @param boolean $ignore ignore
	 * @return void
	 */
	public function setIgnore($ignore) {
		$this->ignore = $ignore;
	}
}
?>