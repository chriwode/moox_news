<?php
namespace DCNGmbH\MooxNews\Tasks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Set news access from parent category
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class NewsAutoSetTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {		
	
	/**
	 * PIDs der Seite/Ordner in dem die News gespeichert sind die versendet werden sollen
	 *
	 * @var string
	 */
	public $newsPids;
	
	/**
	 * Zugriffsrechte setzen
	 *
	 * @var boolean
	 */
	public $setAccess;
	
	/**
	 * Bestehende News Berechtigungen ignorieren
	 *
	 * @var boolean
	 */
	public $ignore;
	
	/**
	 * Single-PID setzen
	 *
	 * @var boolean
	 */
	public $setSinglePid;
	
	
	/**
	 * Prepares the mailer job.
	 *
	 * @return	boolean	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute() {									
		
		$executionSucceeded = true;
		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		
		$newsRepository 	= $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		
		$pids = explode(",",$this->newsPids);
		
		$newsItems = $newsRepository->findNewsByPidList($pids);
		
		$doPersistAll = false;
		
		$categories = array();
		
		// process query
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery("*","tx_mooxnews_domain_model_news","deleted=0 AND pid IN (".$this->newsPids.")","","","");
		
		while( $news = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$catRefRes 	= $GLOBALS['TYPO3_DB']->exec_SELECTquery("uid_local","sys_category_record_mm",'tablenames="tx_mooxnews_domain_model_news" AND uid_foreign='.$news['uid'],"","sorting_foreign ASC","1");
			$catRef 	= $GLOBALS['TYPO3_DB']->sql_fetch_assoc($catRefRes);
			$catUid 	= $catRef['uid_local'];
			if($catUid>0){
				if(!isset($categories[$catUid])){
					$catRes = $GLOBALS['TYPO3_DB']->exec_SELECTquery("*","sys_category",'uid='.$catUid,"","","1");
					$cat = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($catRes);
					$categories[$catUid] = $cat;
				} else {
					$cat = $categories[$catUid];
				}
					
				$doUpdateFeGroup 	= false;
				$doUpdateSinglePid 	= false;
				
				$categoryFeGroups 	= $cat['fe_group'];	
				$oldNewsGroups 		= $news['fe_group'];
					
				if($categoryFeGroups){
					if($this->setAccess){
						if($this->ignore || in_array($news['fe_group'],array(-1,-2))){
							if($oldNewsGroups!=$categoryFeGroups){
								$newNewsFeGroups = $categoryFeGroups;
								$doUpdateFeGroup = true;							
							}
						} else {
							$newsFeGroups = array();
							if($news['fe_group']!=""){
								$newsFeGroups = explode(",",$news['fe_group']);
							}								
							if($categoryFeGroups!=""){
								$categoryFeGroups 	= explode(",",$categoryFeGroups);
							} else {
								$categoryFeGroups 	= array();
							}
							$newNewsFeGroups	= $categoryFeGroups;
							foreach($newsFeGroups AS $group){
								if(!in_array($group,$newNewsFeGroups)){
									$newNewsFeGroups[] = $group;
								}
							}
								
							$newNewsFeGroups = implode(",",$newNewsFeGroups);
							if($oldNewsGroups!=$newNewsFeGroups){
								$doUpdateFeGroup = true;								
							}														
						}
					}
						
					if($this->setSinglePid){
						if($news['single_pid']!=$cat['single_pid']){
							$newNewsSinglePid = $cat['single_pid'];
							$doUpdateSinglePid = true;							
						}
					}
					if($doUpdateFeGroup || $doUpdateSinglePid){
						
						$updateFields = array();
						
						if($doUpdateFeGroup){
							$updateFields['fe_group'] = $newNewsFeGroups;
						}
						if($doUpdateSinglePid){
							$updateFields['single_pid'] = $newNewsSinglePid;
						}
						
						$result = 	$GLOBALS['TYPO3_DB']->exec_UPDATEquery(
																			'tx_mooxnews_domain_model_news',
																			'uid='.$news['uid'],
																			$updateFields
									);							
					}				
				}
			}			
		}
		
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation() {
		
		$info = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_setnewsaccesstask.news_pids_label', 'moox_news' ).": ".$this->getNewsPids();
				
		return $info;
	}
	
	/**
	 * Returns the news pids
	 *
	 * @return string
	 */
	public function getNewsPids() {
		return $this->newsPids;
	}

	/**
	 * Set the news pids
	 *
	 * @param string $newsPids news pids
	 * @return void
	 */
	public function setNewsPids($newsPids) {
		$this->newsPids = $newsPids;
	}
	
	/**
	 * Returns set access
	 *
	 * @return boolean
	 */
	public function getSetAccess() {
		return $this->setAccess;
	}

	/**
	 * Set set access
	 *
	 * @param boolean $setAccess set access
	 * @return void
	 */
	public function setSetAccess($setAccess) {
		$this->setAccess = $setAccess;
	}
	
	/**
	 * Returns ignore
	 *
	 * @return boolean
	 */
	public function getIgnore() {
		return $this->ignore;
	}

	/**
	 * Set ignore
	 *
	 * @param boolean $ignore ignore
	 * @return void
	 */
	public function setIgnore($ignore) {
		$this->ignore = $ignore;
	}
	
	/**
	 * Returns set single pid
	 *
	 * @return boolean
	 */
	public function getSetSinglePid() {
		return $this->setSinglePid;
	}

	/**
	 * Set set single pid
	 *
	 * @param boolean $setSinglePid set single pid
	 * @return void
	 */
	public function setSetSinglePid($setSinglePid) {
		$this->setSinglePid = $setSinglePid;
	}
}
?>