<?php
namespace DCNGmbH\MooxNews\Tasks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Additional field provider for the SetNewsAccessTask
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class NewsAutoSetTaskAdditionalFieldProvider implements \TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface {

	/**
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds an pid field
	 *
	 * @param array $taskInfo Reference to the array containing the info used in the add/edit form
	 * @param object $task When editing, reference to the current task object. Null when adding.
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return array	Array containing all the information pertaining to the additional fields
	 */
	public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject) {
		
		// Initialize extra field value
		if (empty($taskInfo['newsPids'])) {
			if ($parentObject->CMD == 'edit') {
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['newsPids'] = $task->newsPids;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['newsPids'] = '';
			}
		}
		
		
		if (empty($taskInfo['setAccess'])) {
			if ($parentObject->CMD == 'edit') {
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['setAccess'] = $task->setAccess;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['setAccess'] = 1;
			}
		}
		
		if (empty($taskInfo['ignore'])) {
			if ($parentObject->CMD == 'edit') {
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['ignore'] = $task->ignore;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['ignore'] = 0;
			}
		}
		
		if (empty($taskInfo['setSinglePid'])) {
			if ($parentObject->CMD == 'edit') {
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['setSinglePid'] = $task->setSinglePid;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['setSinglePid'] = 1;
			}
		}
		
		$additionalFields = array();
				
		// Write the code for the field
		$fieldID 	= 'task_newsPids';		
		$fieldCode 	= $this->getMooxNewsFoldersSelector('tx_scheduler[newsPids]',$taskInfo['newsPids']);		
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_newsautosettask.news_pids_label', 'moox_news' ),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		);	

		// Write the code for the field
		$fieldID 	= 'task_setAccess';		
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[setAccess]" id="' . $fieldID . '" value="1" '.($taskInfo['setAccess']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_newsautosettask.set_access_label', 'moox_news' ),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		);
		
		// Write the code for the field
		$fieldID 	= 'task_ignore';		
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[ignore]" id="' . $fieldID . '" value="1" '.($taskInfo['ignore']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_newsautosettask.ignore_label', 'moox_news' ),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		);
		
		// Write the code for the field
		$fieldID 	= 'task_setSinglePid';		
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[setSinglePid]" id="' . $fieldID . '" value="1" '.($taskInfo['setSinglePid']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_newsautosettask.set_single_pid_label', 'moox_news' ),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		);
		
		return $additionalFields;
	}

	/**
	 * This method checks any additional data that is relevant to the specific task
	 * If the task class is not relevant, the method is expected to return TRUE
	 *
	 * @param array $submittedData Reference to the array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return boolean TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject) {
				
		$submittedData['newsPids'] 	= (is_array($submittedData['newsPids']))?implode(",",$submittedData['newsPids']):$submittedData['newsPids'];
		
		if ($submittedData['newsPids']=="") {
			$parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_setnewsaccesstask.news_pids_error'), \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
			$result = FALSE;
		} else {
			$result = true;
		}
		
		$submittedData['ignore'] 		= isset($submittedData['ignore'])?1:0;
		$submittedData['setAccess'] 	= isset($submittedData['setAccess'])?1:0;
		$submittedData['setSinglePid'] 	= isset($submittedData['setSinglePid'])?1:0;
		
		return $result;
	}

	/**
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param array $submittedData Array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
	 * @return void
	 */
	public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task) {
				
		$task->newsPids 	= $submittedData['newsPids'];
		$task->setAccess 	= $submittedData['setAccess'];
		$task->ignore 		= $submittedData['ignore'];
		$task->setSinglePid = $submittedData['setSinglePid'];
	
	}
	
	/**
	 * Get select box of folders with news articles	
	 *
	 * @param string $selectorName selector name
	 * @param string $selected current storage pids	
	 * @return	string	Folder selector HTML code
	 */
	public function getMooxNewsFoldersSelector($selectorName,$selected) {				
		
		$selected = explode(",",$selected);
		
		$query = array(
			'SELECT' => '*',
			'FROM' => 'pages',
			'WHERE' => "deleted=0 AND doktype=254 AND module='mxnews'"
		);
		$folders = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);
		
		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" multiple="multiple" class="wide" size="5">';
		
		foreach ($folders as $folder) {			
			$selector .= '<option '.(in_array($folder['uid'],$selected)?'selected="selected" ':'').'value="'.$folder['uid'].'">'.$folder['title'].' ['.$folder['uid'].']</option>';
		}
								
		 $selector .= '</select>';
		
		return $selector;
	}
	
}

?>