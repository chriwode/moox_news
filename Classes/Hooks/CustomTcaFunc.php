<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MooxNews_Hooks_CustomTcaFunc {
		
	/**
	 * generate category selection form
	 *
	 * @param array $PA
	 * @param array $fobj
	 * @return string $tcaForm
	 */
	function generateTcaCategoriesField(&$PA, &$fobj)    {
	
		$options 	= array();
		$uids 		= array();		
		$values 	= explode(",",$PA['itemFormElValue']);
		
		foreach($values AS $value){
			$value 		= explode("|",$value);
			if($value[1]!=""){
				$options[] 	= array("value" => $value[0],"label" => $value[1]);
				$uids[] 	= $value[0]; 			
			}
		}
		
		$configuration 		= Tx_MooxNews_Utility_EmConfiguration::getSettings();		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$categoryRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');
		$backendSession 	= $objectManager->get('DCNGmbH\\MooxNews\\Service\\BackendSessionService');
		$pageUid 			= $backendSession->get("id");
		$demand 			= unserialize($backendSession->get("administrationDemand"));
		
		if(!is_object($demand)){
			$objectManager 	= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$demand 		= $objectManager->get('Tx_MooxNews_Domain_Model_Dto_AdministrationDemand');
			$demand->setType($PA['row']['type']);
		} elseif($demand->getType()!='' && $demand->getType()!=$PA['row']['type']){
			$demand->setType($PA['row']['type']);
			$backendSession->store("administrationDemand",serialize($demand));
		}
		
		$demand->setType($PA['row']['type']);
				
		if($configuration->getCategoryRestriction()=="current_pid"){
			if($demand->getType()!=""){
				$categories = $categoryRepository->findCategoriesByTypeAndPid($pageUid,array($demand->getType()),$demand->getSysLanguageUid());				
			} else {
				$categories = $categoryRepository->findParentCategoriesByPid($pageUid,$demand->getSysLanguageUid());				
			}
		} else {
			if($demand->getType()!=""){
				$categories = $categoryRepository->findAllCategoriesByType(array($demand->getType()),$demand->getSysLanguageUid());
			} else {
				$categories = $categoryRepository->findAllCategories($demand->getSysLanguageUid());
			}
		}
		
		$idList = array();
		foreach ($categories as $c) {
			$idList[] = $c->getUid();
		}
		
		$categories = (count($idList) || $configuration->getCategoryRestriction()!="current_pid")?$categoryRepository->findTree($idList):array();
		
		$fobj->additionalCode_pre[] = '
				<link rel="stylesheet" type="text/css" href="'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_news').'Resources/Public/Css/tca_category.css" />';
		$fobj->additionalCode_pre[] = '
				<script src="'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_news').'Resources/Public/JavaScript/tca_category.js" type="text/javascript"></script>';
		
		$code .= '<div class="moox-news-tca">';
		$code .= '<div class="moox-news-tca-category-display-wrapper">';
		$code .= '<div class="moox-news-tca-category-display-header">Kategorie-Sortierung</div>';
		$code .= '<select id="'.md5($PA['itemFormElName']).'_list" size="10" class="formField tceforms-multiselect moox-news-tca-category-display" multiple="multiple" name="' . $PA['itemFormElName'] . '_list"';      
        $code .= ' onchange="' . htmlspecialchars(implode('', $PA['fieldChangeFunc'])) . '"';
        $code .= $PA['onFocus'];
        $code .= '>';
		$optionCnt = 1;
		foreach($options AS $option){
			if($optionCnt==1){
				$code .= '<option value="'.$option['value'].'">'.self::getCategoryLabel($option['value']).' (Hauptkategorie)</option>';
			} else {
				$code .= '<option value="'.$option['value'].'">'.self::getCategoryLabel($option['value']).'</option>';
			}
			$optionCnt++;
		}
		$code .= '</select>';
		$code .= '<input type="hidden" id="moox-news-tca-category-hidden" name="' . $PA['itemFormElName'] . '" value="'.implode(",",$uids).'">';
		$code .= '<input type="hidden" id="moox-news-tca-category-max" name="moox-news-tca-category-max" value="'.$PA['fieldConf']['config']['maxitems'].'">';
		if($PA['fieldConf']['config']['minitems']>0){
			$code .= '<input type="hidden" id="moox-news-tca-category-min" name="moox-news-tca-category-min" value="'.$PA['fieldConf']['config']['minitems'].'">';
		}
		$code .= '<div class="moox-news-tca-category-display-footer">';
		if($PA['fieldConf']['config']['maxitems']>0){
			$code .= '(<span id="moox-news-tca-category-cnt">'.count($options).'</span> von '.(($PA['fieldConf']['config']['maxitems']!=9999)?$PA['fieldConf']['config']['maxitems']:"unbegrenzt").' möglichen Kategorien gewählt)';
		}
		$code .= '</div>';
		$code .= '</div>';
		$code .= '<div class="moox-news-tca-category-actions">';
		$code .= '<div class="moox-news-tca-category-actions-icon"><span onclick="mooxNewsMoveOptionsToTop(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-to-top t3-btn t3-btn-moveoption-top" title="Ausgewählte Objekte zum Anfang verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-news-tca-category-actions-icon"><span onclick="mooxNewsMoveOptionsOneUp(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-up t3-btn t3-btn-moveoption-up" title="Ausgewählte Objekte nach oben verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-news-tca-category-actions-icon"><span onclick="mooxNewsMoveOptionsOneDown(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-down t3-btn t3-btn-moveoption-down" title="Ausgewählte Objekte nach unten verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-news-tca-category-actions-icon"><span onclick="mooxNewsMoveOptionsToBottom(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-to-bottom t3-btn t3-btn-moveoption-bottom" title="Ausgewählte Objekte zum Ende verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-news-tca-category-actions-icon"><span onclick="mooxNewsRemoveOptions(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-selection t3-icon-selection-delete t3-btn t3-btn-removeoption" title="Ausgewähltes Objekt löschen">&nbsp;</span></div>';
		$code .= '<div class="moox-news-tca-category-actions-icon"><span onclick="mooxNewsSetMainCategory(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut" title="Ausgewähltes Objekt löschen">&nbsp;</span></div>';
		$code .= '</div>';
		$code .= '<div class="moox-news-tca-category-selector-wrapper">';
		$code .= '<div class="moox-news-tca-category-selector-header">Kategorie-Auswahl</div>';
		$code .= '<div class="moox-news-tca-category-selector">';		
		$code .= self::generateCategorySelectionTree(md5($PA['itemFormElName']),$demand,$categories,$uids);	
		$code .= '</div>';
		$code .= '<div class="moox-news-tca-category-selector-footer">';
		$code .= '<span onclick="mooxNewsAddAllOptions(\'#'.md5($PA['itemFormElName']).'\')" class="moox-news-icon icon-select-all">Alle auswählen</span>';
		$code .= '<span onclick="mooxNewsRemoveAllOptions(\'#'.md5($PA['itemFormElName']).'\')" class="moox-news-icon icon-unselect-all">Alle abwählen</span>';
		$code .= '</div>';
		$code .= '</div>';
		$code .= '</div>';		
        
		return $code;		
	}
	
	/**
	 * generate category selection tree
	 *
	 * @param string $id identifier of selection field
	 * @param Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $categories categories
	 * @param array $selected selected categories
	 * @param string $class class
	 * @param integer $depth depth
	 * @return string $tree
	 */
	function generateCategorySelectionTree($id,$demand,$categories,$selected = array(),$class="",$title="",$depth = 0){
		$tree  = "<ul>";
		foreach($categories AS $category){
			if($category['item']->getNewsTypes()=='' || $demand->getType()=='' || in_array($demand->getType(),explode(",",$category['item']->getNewsTypes()))){			
				if(in_array($category['item']->getUid(),$selected)){
					$activeClass = " moox-news-tca-category-active"; 
				} else {
					$activeClass = "";
				}
				if($class!=""){
					$elementClass = $class." moox-news-tca-category-".$category['item']->getUid();
				} else {
					$elementClass = " moox-news-tca-category moox-news-tca-category-".$category['item']->getUid();
				}
				if($title!=""){
					$wrapperTitle = $title." &rsaquo; ".$category['item']->getTitle();
				} else {
					$wrapperTitle = $category['item']->getTitle();
				}
				$wrapperClass = str_replace("-category","-category-wrapper",$elementClass);
				if(isset($category['children'])){
					$isParentClass = " is-parent";
				} else {
					$isParentClass = "";
				}
				$tree .= '<li id="moox-news-tca-category-'.$category['item']->getUid().'" class="'.$elementClass.'">';
				$tree .= '<div id="moox-news-tca-category-wrapper-'.$category['item']->getUid().'" title="'.$category['item']->getTitle().' [UID: '.$category['item']->getUid().']" onclick="mooxNewsToggleOption(\'#'.$id.'\','.$category['item']->getUid().',\''.$wrapperTitle.'\')" class="moox-news-tca-category-wrapper'.$wrapperClass.$isParentClass.$activeClass.'">';			
				if($selected[0]==$category['item']->getUid()){
					$tree .= '<span id="moox-news-tca-category-icon-'.$category['item']->getUid().'" class="t3-icon t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-news-tca-category-icon">&nbsp;</span>';
				} else {
					$tree .= '<span id="moox-news-tca-category-icon-'.$category['item']->getUid().'" class="t3-icon t3-icon-mimetypes t3-icon-mimetypes-x t3-icon-x-sys_category moox-news-tca-category-icon">&nbsp;</span>';
				}
				$tree .= '<span id="moox-news-tca-category-title-'.$category['item']->getUid().'" title="'.$wrapperTitle.'" class="moox-news-tca-category-title">'.$category['item']->getTitle().'</span>';
				$tree .= '</div>';			
				$tree .= '<div id="moox-news-tca-category-context-'.$category['item']->getUid().'" class="moox-news-tca-category-context">';
				$tree .= '<div onclick="mooxNewsSetMainCategory(\'#'.$id.'\','.$category['item']->getUid().',\''.$wrapperTitle.'\')" class="moox-news-context-element icon-select-maincategory"><span class="t3-icon t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-news-tca-category-icon">&nbsp;</span>"'.$category['item']->getTitle().'" als Hauptkategorie wählen</div>';
				if($isParentClass){
					$tree .= '<div onclick="mooxNewsAddAllBranchOptions(\'#'.$id.'\','.$category['item']->getUid().')" class="moox-news-context-element icon-select-branch">Kompletten Bereich "'.$category['item']->getTitle().'" auswählen</div>';
					$tree .= '<div onclick="mooxNewsRemoveAllBranchOptions(\'#'.$id.'\','.$category['item']->getUid().')" class="moox-news-context-element icon-unselect-branch">Kompletten Bereich "'.$category['item']->getTitle().'" abwählen</div>';
				}
				$tree .= '</div>';	
				
				if(isset($category['children'])){
					$tree .= self::generateCategorySelectionTree($id,$demand,$category['children'],$selected,$elementClass,$wrapperTitle,$depth+1)."</li>";				
				} else {
					$tree .= "</li>";
				}
			}
		}
		$tree .= "</ul>";
		return $tree;
	}
	
	/**
	 * get category label
	 *
	 * @param integer $uid category uid	 
	 * @return string $label
	 */
	function getCategoryLabel($uid = 0)    {
				
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$categoryRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');
		
		$uids = array($uid);
		
		while($uid>0){		
			$parent = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
				'parent',
				'sys_category',
				'uid='.$uid,
				'',
				'',
				1
			);
			if($parent[0]['parent']>0){
				$uids[] = $parent[0]['parent'];
			}
			$uid = $parent[0]['parent'];
		}
		
		$uids = array_reverse($uids);
		
		$category 	= $categoryRepository->findByUid($uids[0]);
		if(is_object($category)){
			$label 		= $category->getTitle();
		}
		unset($uids[0]);
		foreach($uids AS $uid){
			$category = $categoryRepository->findByUid($uid);
			if(is_object($category)){
				$label = $label." &rsaquo; ".$category->getTitle();
			}		
		}
		
		return $label;		
	}
		
	/**
	 * set tca category values
	 *
	 * @param array $config	 
	 * @return array $config
	 */
	/*
	function setTcaCategoriesField($config) {
		
		$configuration 		= Tx_MooxNews_Utility_EmConfiguration::getSettings();		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$categoryRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');
		$backendSession 	= $objectManager->get('DCNGmbH\\MooxNews\\Service\\BackendSessionService');
		$pageUid 			= $backendSession->get("id");
		
		if($configuration->getCategoryRestriction()=="current_pid"){
			$categories = $categoryRepository->findParentCategoriesByPid($pageUid);			
		} else {
			$categories = $categoryRepository->findAllCategories();			
		}
		
		$idList = array();
		foreach ($categories as $c) {
			$idList[] = $c->getUid();
		}
		
		$config['items'] = array();
		
		$categories = (count($idList) || $configuration->getCategoryRestriction()!="current_pid")?$categoryRepository->findTree($idList):array();
		
		$config['items'] = self::setTcaCategoriesFieldItemHelper($categories);
			
		return $config;
	}
	*/
	
	/**
	 * set tca category helper
	 *
	 * @param array $categories
	 * @param integer $depth depth
	 * @return array $config
	 */
	/*
	function setTcaCategoriesFieldItemHelper($categories,$depth = 0){
		$items 		= array();
		$subitems 	= array();
		foreach($categories AS $category){
			$title = "";
			for($i=1;$i<=($depth*5);$i++){
				$title .= "&nbsp;"; 
			}			
			$title .= $category['item']->getTitle();
			$items[] = array(0 => $title,1 => $category['item']->getUid(),2 => "");
			if(isset($category['children'])){
				$subitems = self::setTcaCategoriesFieldItemHelper($category['children'],$depth+1);
				if(count($subitems)){
					$items = array_merge($items,$subitems);
				}
			}			
		}
		return $items;
	}
	*/
	
	/**
	 * generate filter field form
	 *
	 * @param array $PA
	 * @param array $fobj
	 * @return string $tcaForm
	 */
	function generateTcaFilterField(&$PA, &$fobj)    {
		
		
		//$configuration 		= Tx_MooxNews_Utility_EmConfiguration::getSettings();		
		$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$newsRepository 	= $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		$backendSession 	= $objectManager->get('DCNGmbH\\MooxNews\\Service\\BackendSessionService');
		$pageUid 			= $backendSession->get("id");
		
		$fobj->additionalCode_pre[] = '
				<link rel="stylesheet" type="text/css" href="'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_news').'Resources/Public/Css/tca_filter.css" />';
		$fobj->additionalCode_pre[] = '
				<script src="'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_news').'Resources/Public/JavaScript/tca_filter.js" type="text/javascript"></script>';
		
		
		$valuesFromDb 	= $newsRepository->findExtendedFilterUniqueValues($PA['field'],$pageUid,array());		
		$field 			= $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$PA['field']];		
		$values = array();
		if(count($valuesFromDb)){			
			$valueCnt 	= 0;						
			foreach($valuesFromDb AS $value){
				if($value=="empty"){
					if($field['addToMooxNewsFrontendFilter']['selectEmptyLabel']!=""){
						$valueLabel = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectEmptyLabel'], $field['extkey']);
						$values[$value] = $valueLabel;
					} else {
						$valueLabel = "Ohne Wert";
					}
				} else {
					$values[$value] = $value;
					$valueCnt++;
				}				
			}			
		}
		
		if(count($values) && !(count($values)==1 && isset($values['empty']))){
			$selector = '<button class="filter-selector-select" data-display="tceforms-textfield-'.md5($PA['itemFormElName']).'">&laquo;&laquo;</button>';
			$selector .= '<select class="filter-selector" id="tceforms-textfield-'.md5($PA['itemFormElName']).'_select">';
			foreach($values AS $key => $value){				
				$selector .= '<option value="'.(($key!="empty")?$key:"").'" '.(($key==$PA['itemFormElValue'])?' selected="selected"':'').'>'.$value.'</option>';
			}
			$selector .= '</select>';
		}
		
		
		return '<div class="t3-form-field-item">
					<div class="t3-form-field-disable"></div>
					<span class="t3-tceforms-input-wrapper" onmouseover="if (document.getElementById(\'tceforms-textfield-'.md5($PA['itemFormElName']).'\').value) {this.className=\'t3-tceforms-input-wrapper-hover\';} else {this.className=\'t3-tceforms-input-wrapper\';};" onmouseout="this.className=\'t3-tceforms-input-wrapper\';">
					<span tag="a" class="t3-icon t3-icon-actions t3-icon-actions-input t3-icon-input-clear t3-tceforms-input-clearer" onclick="document.getElementById(\'tceforms-textfield-'.md5($PA['itemFormElName']).'\').value=\'\';document.getElementById(\'tceforms-textfield-'.md5($PA['itemFormElName']).'\').focus();typo3form.fieldGet(\''.$PA['itemFormElName'].'\',\'\',\'\',0,\'\');'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'">&nbsp;</span>
						<input type="text" id="tceforms-textfield-'.md5($PA['itemFormElName']).'" class="formField tceforms-textfield hasDefaultValue" name="'.$PA['itemFormElName'].'_hr" value="'.$PA['itemFormElValue'].'" style="width: 384px" maxlength="256" onchange="typo3form.fieldGet(\''.$PA['itemFormElName'].'\',\'\',\'\',0,\'\');'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'">						
						<input type="hidden" id="'.$PA['itemFormElName'].'" name="'.$PA['itemFormElName'].'" value="'.$PA['itemFormElValue'].'">
					</span>
					'.$selector.'
					<div style="clear:both;"></div>
				</div>';	
	}

}