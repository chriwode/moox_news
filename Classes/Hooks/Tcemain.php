<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Hook into tcemain which is used to show preview of news item
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_Hooks_Tcemain {

	/**
	 * Flushes the cache if a news record was edited.
	 *
	 * @param array $params
	 * @return void
	 */
	public function clearCachePostProc(array $params) {
		if (isset($params['table']) && $params['table'] === 'tx_mooxnews_domain_model_news' && isset($params['uid'])) {
			$cacheTag = $params['table'] . '_' . $params['uid'];

			/** @var $cacheManager \TYPO3\CMS\Core\Cache\CacheManager */
			$cacheManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager');
			$cacheManager->getCache('cache_pages')->flushByTag($cacheTag);
			$cacheManager->getCache('cache_pagesection')->flushByTag($cacheTag);
			$cacheManager->getCache('cache_pages')->flushByTag('tx_mooxnews');
			$cacheManager->getCache('cache_pagesection')->flushByTag('tx_mooxnews');
		}
	}

	/**
	 * Generate a different preview link     *
	 * @param string $status status
	 * @param string $table table name
	 * @param integer $recordUid id of the record
	 * @param array $fields fieldArray
	 * @param NULL|\TYPO3\CMS\Core\DataHandling\DataHandler $parentObject parent Object
	 * @return void
	 */
	public function processDatamap_afterDatabaseOperations($status, $table, $recordUid, array $fields, \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject = NULL) {
		// Clear category cache
		if ($table === 'sys_category') {
			/** @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface $cache */
			$cache = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('cache_news_category');
			$cache->flush();
		}

		// Preview link
		if ($table === 'tx_mooxnews_domain_model_news') {
			
			$oldRecordUid = $recordUid;
			
			// direct preview
			if (!is_numeric($recordUid)) {
				if(!is_object($parentObject)){
					$recordUid = $parentObject->substNEWwithIDs[$recordUid];
				}
			}
			
			/*			
			foreach($GLOBALS['_POST']['data']['tx_mooxnews_domain_model_news'] AS $key => $value){
				echo $key."--".$value."<br>";
				$element 	= $value;
				$identifier = $key;
			}
			print_r($recordUid);
			exit($recordUid);
			*/
			
			if($recordUid){
				
				$extConf 	= unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news']);
				
				$doUpdate = false;
				
				$objectManager 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		
				$newsRepository 	= $objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
				$categoryRepository = $objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');
								
				$item 				= $newsRepository->findByUid($recordUid,false);
								
				if(is_object($item)){
					if(is_object($item->getDatetime()) && (int)$item->getDatetime()->format("Y")>0){
					
						$year 		= (int)$item->getDatetime()->format("Y");				
										
						if($year>1970){										
							$item->setYear($year);						
							$doUpdate = true;
						}
					}
					if ($extConf['autoSetFeGroupOnSave']>0 || $extConf['autoSetSinglePidOnSave']>0) {
					
						$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
						
						if($GLOBALS['_POST']['data']['tx_mooxnews_domain_model_news'][$oldRecordUid]['categories']!=""){
							$categories = explode(",",$GLOBALS['_POST']['data']['tx_mooxnews_domain_model_news'][$oldRecordUid]['categories']);
						}
						
						if(is_numeric($categories[0]) && $categories[0]>0){
							$itemFirstCategory = $categoryRepository->findByUid($categories[0]);						
						}
						
						if(is_object($itemFirstCategory)){	
							
							$categoryFeGroups 	= $itemFirstCategory->feGroup;											
							$oldItemGroups 		= $item->getFeGroup();
							
							if($categoryFeGroups){
								if($extConf['autoSetFeGroupOnSave']>0){
									if(!$extConf['autoSetFeGroupOnSave'] || in_array($item->getFeGroup(),array(-1,-2))){
										if($oldItemGroups!=$categoryFeGroups){
											$item->setFeGroup($categoryFeGroups);
											$doUpdate = true;										
										}
									} else {
										$itemFeGroups = array();
										if($item->getFeGroup()!=""){
											$itemFeGroups 		= explode(",",$item->getFeGroup());
										}								
										if($categoryFeGroups!=""){
											$categoryFeGroups = explode(",",$categoryFeGroups);
										} else {
											$categoryFeGroups = array();
										}
										$newItemFeGroups = $categoryFeGroups;
										foreach($itemFeGroups AS $group){
											if(!in_array($group,$newItemFeGroups)){
												$newItemFeGroups[] = $group;
											}
										}
										
										$newItemFeGroups = implode(",",$newItemFeGroups);
										if($oldItemGroups!=$newItemFeGroups){
											$item->setFeGroup($newItemFeGroups);
											$doUpdate = true;										
										}
									}								
								}
							}
								
							if($extConf['autoSetSinglePidOnSave']>0){
								if($item->getSinglePid()!=$itemFirstCategory->getSinglePid()){
									$item->setSinglePid($itemFirstCategory->getSinglePid());
									$doUpdate = true;								
								}
							}
						}					
					}
					
					/*
					if($item->getShareTwitterText()==""){
						$item->setShareTwitterText($item->getTitle());
						$doUpdate = true;					
					}
					*/
					
					// get post save functions from external extensions
					$externalFunctions = array();
					$functions = get_class_methods(get_class($this));
					foreach($functions AS $function){
						if(substr($function,0,9)=="external_"){
							$externalFunctions[] = $function;
						}
					}
					
					foreach($externalFunctions AS $externalFunction){
						if($this->$externalFunction($item)){
							$doUpdate = true;
						}
					}
					
					if($doUpdate){
						$newsRepository->update($item);	
						$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();				
					}
				}
			}						
		
			if (isset($GLOBALS['_POST']['_savedokview_x']) && !$fields['type'] && !is_null($parentObject)) {
				// If "savedokview" has been pressed and current article has "type" 0 (= normal news article)
				$pagesTsConfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($GLOBALS['_POST']['popViewId']);
				if ($pagesTsConfig['tx_mooxnews.']['singlePid']) {
					$record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('tx_mooxnews_domain_model_news', $recordUid);

					$parameters = array(
						'no_cache' => 1,
						'tx_mooxnews_pi1[controller]' => 'News',
						'tx_mooxnews_pi1[action]' => 'detail',
						'tx_mooxnews_pi1[news_preview]' => $record['uid'],
					);
					if ($record['sys_language_uid'] > 0) {
						if ($record['l10n_parent'] > 0) {
							$parameters['tx_mooxnews_pi1[news_preview]'] = $record['l10n_parent'];
						}
						$parameters['L'] = $record['sys_language_uid'];
					}

					$GLOBALS['_POST']['popViewId_addParams'] = \TYPO3\CMS\Core\Utility\GeneralUtility::implodeArrayForUrl('', $parameters, '', FALSE, TRUE);
					$GLOBALS['_POST']['popViewId'] = $pagesTsConfig['tx_mooxnews.']['singlePid'];
				}
			}
		}
	}

}