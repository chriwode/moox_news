<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Georg Ringer <typo3@ringerge.org>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Userfunc to render alternative label for media elements
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_Hooks_ItemsProcFunc {

	/**
	 * Itemsproc function to extend the selection of templateLayouts in the plugin
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_templateLayout(array &$config) {
		/** @var Tx_MooxNews_Utility_TemplateLayout $templateLayoutsUtility */
		$templateLayoutsUtility = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Tx_MooxNews_Utility_TemplateLayout');
		$templateLayouts = $templateLayoutsUtility->getAvailableTemplateLayouts($config['row']['pid']);
		foreach ($templateLayouts as $layout) {
			$additionalLayout = array(
				$GLOBALS['LANG']->sL($layout[0], TRUE),
				$layout[1]
			);
			array_push($config['items'], $additionalLayout);
		}
	}

	/**
	 * Modifies the select box of orderBy-options as a category menu
	 * needs different ones then a news action
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_orderBy(array &$config) {
		$newItems = '';
		
		// check if the record has been saved once
		if (is_array($config['row']) && !empty($config['row']['pi_flexform'])) {
			$flexformConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($config['row']['pi_flexform']);

				// check if there is a flexform configuration
			if (isset($flexformConfig['data']['sDEF']['lDEF']['switchableControllerActions']['vDEF'])) {
				$selectedActionList = $flexformConfig['data']['sDEF']['lDEF']['switchableControllerActions']['vDEF'];
					// check for selected action
				if (\TYPO3\CMS\Core\Utility\GeneralUtility::isFirstPartOfStr($selectedActionList, 'Category')) {
					$newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByCategory'];
				} elseif (\TYPO3\CMS\Core\Utility\GeneralUtility::isFirstPartOfStr($selectedActionList, 'Tag')) {
					$this->removeNonValidOrderFields($config, 'tx_mooxnews_domain_model_tag');
					$newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByTag'];
				} elseif (\TYPO3\CMS\Core\Utility\GeneralUtility::isFirstPartOfStr($selectedActionList, 'Target')) {
					$this->removeNonValidOrderFields($config, 'tx_mooxnews_domain_model_target');
					$newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByTarget'];
				} else {
					$newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByNews'];
					$orderByNews = true;
				}
			}
		}

			// if a override configuration is found
		if (!empty($newItems)) {
				// remove default configuration
			$config['items'] = array();
				// empty default line
			array_push($config['items'], array('', ''));

			$newItemArray = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $newItems, TRUE);
			$languageKey = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:flexforms_general.orderBy.';
			foreach ($newItemArray as $item) {
					// label: if empty, key (=field) is used
				$label = $GLOBALS['LANG']->sL($languageKey . $item, TRUE);
				if (empty($label)) {
					$label = htmlspecialchars($item);
				}
				array_push($config['items'], array($label, $item));
			}
		}
		
		if($orderByNews){
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){
				$ll = 'LLL:EXT:'.$field['extkey'].'/Resources/Private/Language/locallang_db.xml:';
				if($field['addToMooxNewsFrontendSorting']){
					$prefix = $ll.'tx_'.str_replace("_","",$field['extkey']).'_domain_model_news';
					$prefix = $GLOBALS['LANG']->sL($prefix, TRUE).": ";
					$label = $GLOBALS['LANG']->sL($field['label'], TRUE);
					array_push($config['items'], array($prefix.$label, \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname)));
				}
			}
		}
	}
	
	/**
	 * Modifies the select box of types-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_types(array &$config) {
		
		// init items array
		$config['items'] = array();
		
		$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news']);
		
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option){
			if(!($extConf['hideDefaultNewsType'] && $option[1]=="moox_news")){
				$config['items'][] = array($GLOBALS['LANG']->sL($option[0], TRUE),$option[1]);
			}
		}		
	}
	
	/**
	 * Modifies the select box of privacy-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function privacy(array &$config, &$pObj) {
		
		if(!\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_news_frontend')){
			
			// set local language path
			$llpath = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:';
			
			// init items array
			$config['items'] = array();
			
			// add item
			$config['items'][] = array($GLOBALS['LANG']->sL($llpath."flexforms_general.privacy.disabled", TRUE),"");	
		}
	}
	
	/**
	 * Modifies the select box of date fields
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_dateField(array &$config) {
		
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){
			$ll = 'LLL:EXT:'.$field['extkey'].'/Resources/Private/Language/locallang_db.xml:';
			if($field['addToMooxNewsFrontendDateFields']){					
				$prefix = $ll.'tx_'.str_replace("_","",$field['extkey']).'_domain_model_news';
				$prefix = $GLOBALS['LANG']->sL($prefix, TRUE).": ";
				$label = $GLOBALS['LANG']->sL($field['label'], TRUE);
				array_push($config['items'], array($prefix.$label, \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname)));
			}
		}	
	}
	
	/**
	 * Modifies the select box of types-options with folder restrictions
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_types_restricted(array &$config) {
		
		$extConf 			= unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news']);		
		$items 				= $config['items'];
		$config['items'] 	= array();
		
		if($config['row']['pid']>0){
			$objectManager 	= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$pageRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
			$pageInfo 		= $pageRepository->getPage($config['row']['pid']);
			if($pageInfo['news_types']!=""){
				$allowedTypes = explode(",",$pageInfo['news_types']);
				foreach($items AS $item){
					if(in_array($item[1],$allowedTypes) && !($extConf['hideDefaultNewsType'] && $item[1]=="moox_news")){
						$config['items'][] = $item;
					}
				}
			} else {
				foreach($items AS $item){
					if(!($extConf['hideDefaultNewsType'] && $item[1]=="moox_news")){
						$config['items'][] = $item;
					}
				}
			}
		} else {
			foreach($items AS $item){
				if(!($extConf['hideDefaultNewsType'] && $item[1]=="moox_news")){
					$config['items'][] = $item;
				}
			}
		}
	}
	
	/**
	 * Modifies the select box of languages
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_language(array &$config) {
		$config['items'] 	= array();
		$languages 			= array();
		
		$sysLanguages = \TYPO3\CMS\Backend\Configuration\TranslationConfigurationProvider::getSystemLanguages();
		
		$languages[] 		= array("Standard-Verhalten","");
		$languages[] 		= array("Aktuelle Seitenspache","page");
		$languages[] 		= array("Artikel für alle Sprachen anzeigen","-1");		
		
		foreach($sysLanguages AS $language){
			if($language['uid']>-1){
				if($language['uid']==0){
					if($this->settings['languageDefaultLanguageLabel']!=""){
						$language['title'] = $this->settings['languageDefaultLanguageLabel'];
					}					
					$language['hidden'] = 0;
				}
				if($language['hidden']==0){							
					$languages[] 	= array($language['title'],$language['uid']);					
				}
			}
		}
		
		$config['items'] = $languages;		
	}

	/**
	 * Remove not valid fields from ordering
	 *
	 * @param array $config tca items
	 * @param string $tableName table name
	 * @return void
	 */
	protected function removeNonValidOrderFields(array &$config, $tableName) {
		$allowedFields = array_keys($GLOBALS['TCA'][$tableName]['columns']);

		foreach($config['items'] as $key => $item) {
			if ($item[1] != '' && !in_array($item[1], $allowedFields)) {
				unset($config['items'][$key]);
			}
		}
	}

	/**
	 * Modifies the selectbox of available actions
	 *
	 * @param array &$config
	 * @return void
	 */
	public function user_switchableControllerActions(array &$config) {
		if (!empty($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['list'])) {
			$configuration = (int)$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['list'];
				switch ($configuration) {
					case 1:
						$this->removeActionFromList($config, 'News->list');
						break;
					case 2:
						$this->removeActionFromList($config, 'News->list;News->detail');
						break;
					default:
				}
		}

			// Add additional actions
		if (isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['newItems'])
				&& is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['newItems'])) {
			foreach ($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['newItems'] as $key => $label) {
				array_push($config['items'], array($GLOBALS['LANG']->sL($label), $key, ''));
			}
		}
	}

	/**
	 * Remove given action from switchableControllerActions
	 *
	 * @param array $config available items
	 * @param string $action action to be removed
	 * @return void
	 */
	private function removeActionFromList(array &$config, $action) {
		foreach ($config['items'] as $key => $item) {
			if ($item[1] === $action) {
				unset($config['items'][$key]);
				continue;
			}
		}
	}

	/**
	 * Generate a select box of languages to choose an overlay
	 *
	 * @return string select box
	 */
	public function user_categoryOverlay() {
		$html = '';

		$orderBy = $GLOBALS['TCA']['sys_language']['ctrl']['sortby'] ?
						$GLOBALS['TCA']['sys_language']['ctrl']['sortby'] :
						$GLOBALS['TYPO3_DB']->stripOrderBy($GLOBALS['TCA']['sys_language']['ctrl']['default_sortby']);

		$languages = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
			'*',
			'sys_language',
			'1=1 ' . \TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause('sys_language'),
			'',
			$orderBy
		);

			// if any language is available
		if (count($languages) > 0) {
			$html = '<select name="data[newsoverlay]" id="field_newsoverlay">
						<option value="0">' . $GLOBALS['LANG']->sL('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', TRUE) . '</option>';

			foreach ($languages as $language) {
				$selected = ($GLOBALS['BE_USER']->uc['newsoverlay'] == $language['uid']) ? ' selected="selected" ' : '';
				$html .= '<option ' . $selected . 'value="' . $language['uid'] . '">' . htmlspecialchars($language['title']) . '</option>';
			}

			$html .= '</select>';
		} else {
			$html .= $GLOBALS['LANG']->sL(
						'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:usersettings.no-languages-available', TRUE
					);
		}

		return $html;
	}	
}