<?php
/***************************************************************
 *  Copyright notice
 *  (c) 2013 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Target controller
 */
class Tx_MooxNews_Controller_TargetController extends Tx_MooxNews_Controller_NewsController {

	const SIGNAL_TARGET_LIST_ACTION = 'listAction';

	/**
	 * @var Tx_MooxNews_Domain_Repository_TargetRepository
	 */
	protected $targetRepository;

	/**
	 * Inject a target repository to enable DI
	 *
	 * @param Tx_MooxNews_Domain_Repository_TargetRepository $targetRepository
	 * @return void
	 */
	public function injectTargetRepository(Tx_MooxNews_Domain_Repository_TargetRepository $targetRepository) {
		$this->targetRepository = $targetRepository;
	}

	/**
	 * List targets
	 *
	 * @param array $overwriteDemand
	 * @return void
	 */
	public function listAction(array $overwriteDemand = NULL) {
		// Default value is wrong for targets
		if ($this->settings['orderBy'] === 'datetime') {
			unset($this->settings['orderBy']);
		}

		$demand = $this->createDemandObjectFromSettings($this->settings);

		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== NULL) {
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		$assignedValues = array(
			'targets' => $this->targetsRepository->findDemanded($demand),
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
		);

		$this->emitActionSignal('TargetController', self::SIGNAL_TARGET_LIST_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

}
