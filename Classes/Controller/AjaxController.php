<?php
namespace DCNGmbH\MooxNews\Controller;

/***************************************************************
*  Copyright notice
*
*  (c) 2010 Georg Ringer <typo3@ringerge.org>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Controller of news records
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class AjaxController extends \Tx_MooxNews_Controller_NewsController {	
	
	/**
	 * default action
	 *
	 * @param array request
	 * @return void
	 */
	public function defaultAction($request = array()) {
		exit($this->actionMethodName);
	}
		
	/**
	 * get list code
	 *
	 * @param array $overwriteDemand
	 * @param integer $page
	 * @param string $settings
	 * @return void
	 */
	public function fetchListAction(array $overwriteDemand = NULL,$page = 0,$settings = "") {
		
		if($settings!=""){
		
			$settings = json_decode($settings,true);			
			
			if(!is_array($this->settings)){
				$this->settings = array();
			}
			
			$this->settings = array_merge($this->settings,$settings);
			
			if($page>1){
				$overwriteDemand['limit'] = $this->settings['list']['paginate']['itemsPerPage'];
				$overwriteDemand['offset'] = ($overwriteDemand['limit']*($page-1));				
			} elseif($page==1){
				$overwriteDemand['limit'] = $this->settings['list']['paginate']['itemsPerPage'];
				$overwriteDemand['offset'] = 0;
			}
			
			$this->isAjaxRequest = true;
			
			parent::listAction($overwriteDemand);
		
		} else {
			exit("not allowed");
		}
	}
	
	/**
	 * get code for single news element
	 *
	 * @param \Tx_MooxNews_Domain_Model_News $news news item
	 * @param string $settings
	 * @return void
	 */
	public function fetchDetailAction(\Tx_MooxNews_Domain_Model_News $news = NULL,$settings = "") {
				
		if(!is_null($news) && $settings!=""){
		
			$settings = json_decode($settings,true);			
			
			if(!is_array($this->settings)){
				$this->settings = array();
			}
			
			$this->settings = array_merge($this->settings,$settings);
			
			parent::detailAction($news);
		
		} else {
			exit("not allowed");
		}
	}
	
	/**
	 * get filter code
	 *
	 * @param string $filter
	 * @param array $dependDemand	
	 * @param string $settings
	 * @return void
	 */
	public function fetchFilterAction($filter = "",array $dependDemand = NULL,$settings = "") {
		
		$this->isAjaxRequest = true;
		
		if($filter!="" && $settings!=""){
		
			$dependFields = array();
			
			if(count($dependDemand)){
				foreach($dependDemand AS $dependField => $dependFieldValue){
					$dependFields[\TYPO3\CMS\Core\Utility\GeneralUtility::camelCaseToLowerCaseUnderscored($dependField)] = $dependFieldValue;
				}
			}
			
			$settings = json_decode($settings,true);			
			
			if(!is_array($this->settings)){
				$this->settings = array();
			}
			
			$this->settings = array_merge($this->settings,$settings);
			
			$demand 		= \Tx_MooxNews_Controller_NewsController::createDemandObjectFromSettings($this->settings);
			
			$fieldname 		= \TYPO3\CMS\Core\Utility\GeneralUtility::camelCaseToLowerCaseUnderscored($filter);
			
			$field 			= $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
			
			$items 			= array();
			
			if($field['config']['type']=='select'){							
				if($field['config']['foreign_table']!=""){
					$itemsFromDb = $this->newsRepository->findExtendedFilterItems($fieldname,$demand->getStoragePage(),$field['config']['foreign_table'],$field['config']['foreign_table_where'],$dependFields);					
					foreach($itemsFromDb AS $item){
						$items[$item['uid']] = $item['title'];
					}
				}
			} else {
				$valuesFromDb 	= $this->newsRepository->findExtendedFilterUniqueValues($fieldname,$demand->getStoragePage(),$dependFields);
			}
			
			if($field['config']['type']=='select'){
				if(count($items)){
					if(!isset($items['all'])){
						if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
							$selectAllLabel = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
						} else {
							$selectAllLabel = "Alle";
						}
						$itemsTmp 		= $items;
						$items 			= array();
						$items["all"] 	= $selectAllLabel;
						foreach($itemsTmp AS $key => $value){
							$items[$key] = $value;
						}
						unset($itemsTmp);
					}
				}
				$this->view->assign('options', $items);
			} else {
				
				if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
					$values['all'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
				} else {
					$values['all'] = "Alle";
				}
				
				if(count($valuesFromDb)){
				
					$valueCnt 	= 0;
								
					foreach($valuesFromDb AS $value){
						if($value=="empty"){
							if($field['addToMooxNewsFrontendFilter']['selectEmptyLabel']!=""){
								$valueLabel = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectEmptyLabel'], $field['extkey']);
								$values[$value] = $valueLabel;
							} else {
								$valueLabel = "Ohne Wert";
							}
						} else {
							$values[$value] = $value;
							$valueCnt++;
						}				
					}
					$this->view->assign('options', $values);
					
				} 
			}
			
		} else {
			exit("not allowed");
		}
	}
}