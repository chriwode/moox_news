<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Georg Ringer <typo3@ringerge.org>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Controller of news records
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_Controller_NewsController extends Tx_MooxNews_Controller_NewsBaseController {

	const SIGNAL_NEWS_LIST_ACTION = 'listAction';
	const SIGNAL_NEWS_DETAIL_ACTION = 'detailAction';
	const SIGNAL_NEWS_DATEMENU_ACTION = 'dateMenuAction';
	const SIGNAL_NEWS_SEARCHFORM_ACTION = 'searchFormAction';
	const SIGNAL_NEWS_SEARCHRESULT_ACTION = 'searchResultAction';

	/**
	 * @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
	 */
	protected $typoScriptFrontendController;

	/**
	 * @var Tx_MooxNews_Domain_Repository_NewsRepository
	 */
	protected $newsRepository;
	
	/**
	 * @var Tx_MooxNews_Domain_Repository_CategoryRepository
	 */
	protected $categoryRepository;
	
	/**
	 * @var Tx_MooxNews_Domain_Repository_TagRepository
	 */
	protected $tagRepository;
	
	/**
	 * @var Tx_MooxNews_Domain_Repository_TargetRepository
	 */
	protected $targetRepository;

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;
	
	/**
	 * @var boolean
	 */
	public $isAjaxRequest;
	
	/**
	 * @var array
	 */
	public $charList;

	/**
	 * Initializes the current action
	 *
	 * @return void
	 */
	public function initializeAction() {
		
		$this->newsRepository 		= $this->objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');
		$this->categoryRepository 	= $this->objectManager->get('Tx_MooxNews_Domain_Repository_CategoryRepository');
		$this->tagRepository 		= $this->objectManager->get('Tx_MooxNews_Domain_Repository_TagRepository');
		$this->targetRepository 	= $this->objectManager->get('Tx_MooxNews_Domain_Repository_TargetRepository');
		
		if (isset($this->settings['format'])) {
			$this->request->setFormat($this->settings['format']);
		}
		// Only do this in Frontend Context
		if (!empty($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE'])) {
			// We only want to set the tag once in one request, so we have to cache that statically if it has been done
			static $cacheTagsSet = FALSE;

			/** @var $typoScriptFrontendController \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController  */
			$typoScriptFrontendController = $GLOBALS['TSFE'];
			if (!$cacheTagsSet) {
				$typoScriptFrontendController->addCacheTags(array('tx_mooxnews'));
				$cacheTagsSet = TRUE;
			}
			$this->typoScriptFrontendController = $typoScriptFrontendController;
		}
		foreach (range('A', 'Z') as $char) {
			$this->charList[$char] = $char;
		}
		$this->charList['other'] = "Andere";		
	}

	/**
	 * Create the demand object which define which records will get shown
	 *
	 * @param array $settings
	 * @return Tx_MooxNews_Domain_Model_Dto_NewsDemand
	 */
	protected function createDemandObjectFromSettings($settings) {
		/* @var $demand Tx_MooxNews_Domain_Model_Dto_NewsDemand */
					
		$demand = $this->objectManager->get('Tx_MooxNews_Domain_Model_Dto_NewsDemand');
				
		$demand->setCategories(\TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $settings['categories'], TRUE));
		$demand->setCategoryConjunction($settings['categoryConjunction']);
		$demand->setIncludeSubCategories($settings['includeSubCategories']);
		$demand->setTags($settings['tags']);
		$demand->setPrivacy($settings['privacy']);
		$demand->setTopNewsRestriction($settings['topNewsRestriction']);
		$demand->setTimeRestriction($settings['timeRestriction']);
		$demand->setTimeRestrictionHigh($settings['timeRestrictionHigh']);
		$demand->setArchiveRestriction($settings['archiveRestriction']);
		$demand->setExcludeAlreadyDisplayedNews($settings['excludeAlreadyDisplayedNews']);
		$demand->setIncludeOnlySelectedNewsInRss($settings['list']['rss']['showSelectedOnly']);		
		if ($settings['orderBy']) {
			$demand->setOrder($settings['orderBy'] . ' ' . $settings['orderDirection']);
		}
		$demand->setOrderByAllowed($settings['orderByAllowed']);
		$demand->setTopNewsFirst($settings['topNewsFirst']);
		$demand->setLimit($settings['limit']);
		$demand->setOffset($settings['offset']);		
		$demand->setSearchFields($settings['search']['fields']);
		$demand->setDateField($settings['dateField']);
		$demand->setMonth($settings['month']);
		$demand->setYear($settings['year']);
		
		if($settings['isRss'] && $settings['list']['rss']['excludeSelected']){
			$demand->setExcludeFromRss(1);			
		}
		//$demand->setExcludeFromRss(1);		
		if(!is_array($settings['types']) && $settings['types']!=""){
			$settings['types'] = explode(",",$settings['types']);
		}
		
		$demand->setTypes($settings['types']);	
		if(count($settings['types'])==1){
			$demand->setType($settings['types'][0]);
		}
		
		if(is_numeric($settings['language'])){
			$demand->setSysLanguageUid($settings['language']);
		} elseif($settings['language']=="page" && is_numeric($GLOBALS['TSFE']->sys_language_uid)){
			$demand->setSysLanguageUid($GLOBALS['TSFE']->sys_language_uid);
		} 
		
		// set default values for extended filter fields from external extension
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){				
			if($field['addToMooxNewsFrontendDemand']){
				$fieldname = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
				$extkey	= \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($field['extkey']);
				$settingname = lcfirst(str_replace($extkey,"",$fieldname));
				if(isset($settings[$field['extkey']]['defaults'][$settingname]) && $settings[$field['extkey']]['defaults'][$settingname]!=""){
					$setCall = "set".ucfirst($fieldname);
					if(method_exists($demand, $setCall)){
						$demand->$setCall($settings[$field['extkey']]['defaults'][$settingname]);
					}
				}				
			}
		}				
		
		$demand->setStoragePage(Tx_MooxNews_Utility_Page::extendPidListByChildren($settings['startingpoint'],$settings['recursive']));
		
		$demand->setCharList($this->charList);
		
		return $demand;
	}

	/**
	 * Overwrites a given demand object by an propertyName =>  $propertyValue array
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_NewsDemand $demand
	 * @param array $overwriteDemand
	 * @return Tx_MooxNews_Domain_Model_Dto_NewsDemand
	 */
	protected function overwriteDemandObject($demand, $overwriteDemand) {
		
		unset($overwriteDemand['orderByAllowed']);				
		
		foreach ($overwriteDemand as $propertyName => $propertyValue) {
			\TYPO3\CMS\Extbase\Reflection\ObjectAccess::setProperty($demand, $propertyName, $propertyValue);
		}
		return $demand;
	}

	/**
	 * Output a list view of news
	 *
	 * @param array $overwriteDemand
	 * @return void
	 */
	public function listAction(array $overwriteDemand = NULL) {
		
		// Extension manager configuration
		$configuration 	= Tx_MooxNews_Utility_EmConfiguration::getSettings();								
		
		$demand = $this->createDemandObjectFromSettings($this->settings);
		
		// get extended filter
		$extendedFilters = array();
		if(!$this->settings['hideAllFilter']){
			$extendedFilters = $this->getExtendedFilters($demand,$overwriteDemand);
		}
		
		$years = array();		
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideYearFilter']){
			$allyears = $this->newsRepository->findAllYears($demand->getStoragePage());		
			$latestYear = 0;
			$newsWithoutDatetime = 0;
			
			foreach ($allyears as $year) {			
				if($year['year']>1970){
					$years[$year['year']] = $year['year'];
					if($year['year']>$latestYear){
						$latestYear = $year['year'];
					}
				} else {
					$newsWithoutDatetime = 1;
				}
			}
			if($newsWithoutDatetime){
				$years['0'] = "Ohne Jahresangabe";			
			}
			if(count($years)>1 && !$overwriteDemand['year'] && $latestYear>0){
				//$overwriteDemand['year'] = $latestYear;
			}
		}
			
		if (($this->settings['disableOverrideDemand'] != 1 || $this->isAjaxRequest) && $overwriteDemand !== NULL) {
			
			$resetFields = array("type","types","categories","target","targets","years","year","tag","tags","char");			
			foreach($resetFields AS $resetField){
				if(isset($overwriteDemand[$resetField]) && $overwriteDemand[$resetField]=="all"){
					unset($overwriteDemand[$resetField]);
				}
			}
			foreach($extendedFilters AS $resetField){
				if($resetField['type']=="select" && isset($overwriteDemand[$resetField['name']]) && $overwriteDemand[$resetField['name']]=="all"){
					unset($overwriteDemand[$resetField['name']]);
				}
			}			
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}
				
		if(!$demand->getCategories() && !$overwriteDemand['categories'] && $this->settings['categories']){
			$demand->setCategories($this->settings['categories']);
		}
		
		$newsRecords = $this->newsRepository->findDemanded($demand);
		
		$categories = array();
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideCategoryFilter']){
			$allcategories = $this->categoryRepository->findAllCategories();			
		
			$selectedCategories = explode(",",$this->settings['categories']);
			
			foreach ($allcategories as $cat) {
				if($this->settings['categoryConjunction']==""){				
					$categories[$cat->getUid()] = $cat->getTitle();
				} elseif(in_array($this->settings['categoryConjunction'],array("and","or")) && in_array($cat->getUid(),$selectedCategories)){			
					$categories[$cat->getUid()] = $cat->getTitle();
				} elseif(in_array($this->settings['categoryConjunction'],array("notand","notor")) && !in_array($cat->getUid(),$selectedCategories)){			
					$categories[$cat->getUid()] = $cat->getTitle();
				}
			}
			
			$idList = array();
			foreach ($allcategories as $c) {
				$idList[] = $c->getUid();
			}
		}
		
		$tags = array();
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideTagFilter']){
			$alltags = $this->tagRepository->findAll(FALSE);				
		
			foreach ($alltags as $tag) {
				$tags[$tag->getUid()] = $tag->getTitle();
			}
		}
		
		$targets = array();
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideTargetFilter']){
			$alltargets = $this->targetRepository->findAll(FALSE);				
		
			foreach ($alltargets as $target) {
				$targets[$target->getUid()] = $target->getTitle();
			}
		}
		
		$types = array();
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideTypeFilter']){
			$types = $this->getTypes();
		}
		
		if($this->isAjaxRequest){
			$allDemand = $demand;
			$allDemand->setLimit(NULL);
			$allDemand->setOffset(NULL);
			$count = $this->newsRepository->findDemanded($allDemand)->count();
			$pages = ceil($count/$this->settings['list']['paginate']['itemsPerPage']);
		} else {
			$count = $newsRecords->count();
			$pages = ceil($count/$this->settings['list']['paginate']['itemsPerPage']);
		}
		
		$assignedValues = array(
			'news' => $newsRecords,
			'count' => $count,			
			'pages' => $pages,
			'years' => (count($years)>1)?$years:NULL,
			'categories' => (count($categories)>1)?$categories:NULL,
			'categoriesTree' => (count($idList) || $configuration->getCategoryRestriction()!="current_pid")?$this->categoryRepository->findTree($idList):array(),
			'tags' => (count($tags)>1)?$tags:NULL,
			'targets' => (count($targets)>1)?$targets:NULL,
			'showFilter' => (count($categories)>1 || count($types)>1 || count($targets)>1 || count($years)>1)?TRUE:FALSE,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
			'types' => (count($types)>1)?$types:NULL,
			'charList' => $this->charList,
			'extendedFilters' => $extendedFilters,
			'extendedFields' => $this->getExtendedFields("list")
			
		);
		if($this->isAjaxRequest){
			$assignedValues['settings'] = $this->settings;
		}

		$this->emitActionSignal('NewsController', self::SIGNAL_NEWS_LIST_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Single view of a news record
	 *
	 * @param Tx_MooxNews_Domain_Model_News $news news item
	 * @param integer $currentPage current page for optional pagination
	 * @param integer $contentObjectUid current content object
	 * @return void
	 */
	public function detailAction(Tx_MooxNews_Domain_Model_News $news = NULL, $currentPage = 1, $contentObjectUid = 0) {
		if (is_null($news)) {
			$previewNewsId = ((int)$this->settings['singleNews'] > 0) ? $this->settings['singleNews'] : 0;
			if ($this->request->hasArgument('news_preview')) {
				$isPreview = true;
				$previewNewsId = (int)$this->request->getArgument('news_preview');
				
			}

			if ($previewNewsId > 0) {
				$forcePreview = false;
				if($this->request->hasArgument('news_preview_hash')){
					$previewNewsHash = md5($previewNewsId.$GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']);
					$checkPreviewNewsHash = $this->request->getArgument('news_preview_hash');
					if($previewNewsHash==$checkPreviewNewsHash){
						$forcePreview = true;
					}
				} 
				
				if ($forcePreview || $this->isPreviewOfHiddenRecordsEnabled()) {
					$news = $this->newsRepository->findByUid($previewNewsId, FALSE);
				} else {
					$news = $this->newsRepository->findByUid($previewNewsId);
				}
			}
		}
		
		if ($this->request->hasArgument('overwriteDemand')) {
			$overwriteDemand = $this->request->getArgument('overwriteDemand');						
		} 
		
		if (is_a($news, 'Tx_MooxNews_Domain_Model_News')) {
			$news = $this->checkPidOfNewsRecord($news);
		}

		if (is_null($news) && isset($this->settings['detail']['errorHandling'])) {
			$this->handleNoNewsFoundError($this->settings['detail']['errorHandling']);
		}
				
		$assignedValues = array(
			'newsItem' => $news,
			'isPreview' => $isPreview,
			'contentObjectUid' => (int)$contentObjectUid,
			'currentPage' => (int)$currentPage,
			'overwriteDemand' => $overwriteDemand,
			'extendedFields' => $this->getExtendedFields("detail")
		);

		$this->emitActionSignal('NewsController', self::SIGNAL_NEWS_DETAIL_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);

		Tx_MooxNews_Utility_Page::setRegisterProperties($this->settings['detail']['registerProperties'], $news);
		if (!is_null($news) && is_a($news, 'Tx_MooxNews_Domain_Model_News')) {
			Tx_MooxNews_Utility_Cache::addCacheTagsByNewsRecords(array($news), FALSE);
		}
	}
	
	/**
	 * Get array of allowed types
	 *	
	 * @return	array	types	
	 */
	public function getTypes() {
		
		$allowedTypes = array();
		
		if($this->settings['types']!=""){
			$allowedTypes = explode(",",$this->settings['types']);
		}
		
		$types = array();
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option){
			if($this->settings['types']=="" || in_array($option[1],$allowedTypes)){
				$types[$option[1]] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($option[0], "moox_news");
			}
		}
		return $types;
	}
	
	/**
	 * Checks if the news pid could be found in the startingpoint settings of the detail plugin and
	 * if the pid could not be found it return NULL instead of the news object.
	 *
	 * @param Tx_MooxNews_Domain_Model_News $news
	 * @return NULL|Tx_MooxNews_Domain_Model_News
	 */
	protected function checkPidOfNewsRecord(Tx_MooxNews_Domain_Model_News $news) {
		$allowedStoragePages = GeneralUtility::trimExplode(
			',',
			Tx_MooxNews_Utility_Page::extendPidListByChildren(
				$this->settings['startingpoint'],
				$this->settings['recursive']
			),
			TRUE
		);
		if (count($allowedStoragePages) > 0 && !in_array($news->getPid(), $allowedStoragePages)) {
			$this->signalSlotDispatcher->dispatch(
				__CLASS__,
				'checkPidOfNewsRecordFailedInDetailAction',
				array(
					'news' => $news,
					'newsController' => $this
				)
			);
			$news = NULL;
		}
		return $news;
	}

	/**
	 * Checks if preview is enabled either in TS or FlexForm
	 *
	 * @return bool
	 */
	protected function isPreviewOfHiddenRecordsEnabled() {
		if (!empty($this->settings['previewHiddenRecords']) && $this->settings['previewHiddenRecords'] == 2) {
			$previewEnabled = !empty($this->settings['enablePreviewOfHiddenRecords']);
		} else {
			$previewEnabled = !empty($this->settings['previewHiddenRecords']);
		}
		return $previewEnabled;
	}

	/**
	 * Render a menu by dates, e.g. years, months or dates
	 *
	 * @param array $overwriteDemand
	 * @return void
	 */
	public function dateMenuAction(array $overwriteDemand = NULL) {
		$demand = $this->createDemandObjectFromSettings($this->settings);

		// It might be that those are set, @see http://forge.typo3.org/issues/44759
		$demand->setLimit(0);
		$demand->setOffset(0);
			// @todo: find a better way to do this related to #13856
		if (!$dateField = $this->settings['dateField']) {
			$dateField = 'datetime';
		}
		$demand->setOrder($dateField . ' ' . $this->settings['orderDirection']);
		$newsRecords = $this->newsRepository->findDemanded($demand);

		$demand->setOrder($this->settings['orderDirection']);
		$statistics = $this->newsRepository->countByDate($demand);

		$assignedValues = array(
			'listPid' => ($this->settings['listPid'] ? $this->settings['listPid'] : $GLOBALS['TSFE']->id),
			'dateField' => $dateField,
			'data' => $statistics,
			'news' => $newsRecords,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
		);

		$this->emitActionSignal('NewsController', self::SIGNAL_NEWS_DATEMENU_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Display the search form
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_Search $search
	 * @param array $overwriteDemand
	 * @return void
	 */
	public function searchFormAction(Tx_MooxNews_Domain_Model_Dto_Search $search = NULL, array $overwriteDemand = array()) {
		$demand = $this->createDemandObjectFromSettings($this->settings);
		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== NULL) {
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		if (is_null($search)) {
			$search = $this->objectManager->get('Tx_MooxNews_Domain_Model_Dto_Search');
		}
		$demand->setSearch($search);

		$assignedValues = array(
			'search' => $search,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
		);

		$this->emitActionSignal('NewsController', self::SIGNAL_NEWS_SEARCHFORM_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Displays the search result
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_Search $search
	 * @param array $overwriteDemand
	 * @return void
	 */
	public function searchResultAction(Tx_MooxNews_Domain_Model_Dto_Search $search = NULL, array $overwriteDemand = array()) {
		$demand = $this->createDemandObjectFromSettings($this->settings);
		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== NULL) {
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		if (!is_null($search)) {
			$search->setFields($this->settings['search']['fields']);
			$search->setDateField($this->settings['dateField']);
		}
		$demand->setSearch($search);

		$assignedValues = array(
			'news' => $this->newsRepository->findDemanded($demand),
			'overwriteDemand' => $overwriteDemand,
			'search' => $search,
			'demand' => $demand,
		);

		$this->emitActionSignal('NewsController', self::SIGNAL_NEWS_SEARCHRESULT_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/***************************************************************************
	 * helper
	 **********************/

	/**
	 * Injects the Configuration Manager and is initializing the framework settings
	 *
	 * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager Instance of the Configuration Manager
	 * @return void
	 */
	public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;

		$tsSettings = $this->configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
				'news',
				'news_pi1'
			);
		$originalSettings = $this->configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
			);

			// Use stdWrap for given defined settings
		if (isset($originalSettings['useStdWrap']) && !empty($originalSettings['useStdWrap'])) {
			/** @var  \TYPO3\CMS\Extbase\Service\TypoScriptService $typoScriptService */
			$typoScriptService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Service\\TypoScriptService');
			$typoScriptArray = $typoScriptService->convertPlainArrayToTypoScriptArray($originalSettings);
			$stdWrapProperties = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $originalSettings['useStdWrap'], TRUE);
			foreach ($stdWrapProperties as $key) {
				if (is_array($typoScriptArray[$key . '.'])) {
					$originalSettings[$key] = $this->configurationManager->getContentObject()->stdWrap(
							$originalSettings[$key],
							$typoScriptArray[$key . '.']
					);
				}
			}
		}

		// start override
		if (isset($tsSettings['settings']['overrideFlexformSettingsIfEmpty'])) {
			/** @var Tx_MooxNews_Utility_TypoScript $typoScriptUtility */
			$typoScriptUtility = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Tx_MooxNews_Utility_TypoScript');
			$originalSettings = $typoScriptUtility->override($originalSettings, $tsSettings);
		}

		$this->settings = $originalSettings;
	}
	
	/**
	 * Get extended fields from external extensions
	 *
	 * @param string $view
	 * @return array extendedFields
	 */
	public function getExtendedFields($view = 'list') {
		
		$extendedFields = array();
		
		if($view=="detail"){
			$key = "addToMooxNewsFrontendDetailView";
		} else {
			$key = "addToMooxNewsFrontendListView";
		}
				
		if($this->settings['addExtendedFields']){
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){			
				if($field[$key]){				
					$field['title'] = $fieldname;
					$field['name'] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
					$field['nameEmphasized'] = $fieldname;
					if($field[$key]['type']==""){
						$field[$key]['type'] = "text";
					}
					$extendedFields[] = $field;
				}
			}
		}
		
		return $extendedFields;
	}
	
	/**
	 * Get extended filters from external extensions
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_NewsDemand $demand
	 * @param array $overwriteDemand
	 * @return array extendedFields
	 */
	public function getExtendedFilters($demand,$overwriteDemand = NULL) {
		
		$extendedFilters = array();
		
		if($this->settings['addExtendedFilter']){
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){
				
				if($field['addToMooxNewsFrontendFilter'] && ($demand->getType()=="" || $demand->getType()==$field['extkey'])){
					$fieldnameLCC 	= \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
					$fieldnameUCC 	= \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($fieldname);
					$extkey			= \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($field['extkey']);
					$label 			= \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['label'], $field['extkey']);
					
					if($this->isAjaxRequest){
						if($field['config']['type']=="select"){
							$type = "select";
						} elseif($field['config']['type']=="input" && $field['addToMooxNewsFrontendFilter']['type']=="select") {
							$type = "select";
						} else {
							$type = "default";
						}
						$extendedFilters[] = array(	"name" => $fieldnameLCC,
													"type" => $type );
					} else {
						if($field['config']['type']=="select"){
						
							$items = array();
							if(is_array($field['config']['items'])){
								foreach($field['config']['items'] AS $item){
									if($item[1]==""){
										$item[1] = "all";
									}
									if($item[1]=="all"){
										if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
											$item[0] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
										} else {
											$item[0] = "Alle";
										}
										$items[$item[1]] = $item[0];
									} else {
										$label = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($item[0], $field['extkey']);
										if($label==""){
											$label = $item[0];
										}
										$items[$item[1]] = $label;
									}
									
								}					
							}
							
							$depends 		= array();
							$dependFields 	= array();
							$displayDepends	= array();
									
							if(count($field['addToMooxNewsFrontendFilter']['depends'])){
								foreach($field['addToMooxNewsFrontendFilter']['depends'] AS $depend){								
									if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend])){
										$dependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend];
										if($dependField['config']['type']=="select" || ($dependField['config']['type']=="input" && $dependField['addToMooxNewsFrontendFilter']['type']=="select")){
											$depends[] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($depend);
											$dependField = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($depend);
											if(isset($overwriteDemand[$dependField]) && $overwriteDemand[$dependField]!="all"){
												$dependFields[$depend] = $overwriteDemand[$dependField];
											}
										}
									}
								}
							}
							if(count($field['addToMooxNewsFrontendFilter']['displayDepends'])){
								foreach($field['addToMooxNewsFrontendFilter']['displayDepends'] AS $displayDepend){								
									if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend])){
										$displayDependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend];
										if($displayDependField['config']['type']=="select" || ($displayDependField['config']['type']=="input" && $displayDependField['addToMooxNewsFrontendFilter']['type']=="select")){
											$displayDepends[] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($displayDepend);										
										}
									}
								}
							}
							
							if($field['config']['foreign_table']!=""){
								$itemsFromDb = $this->newsRepository->findExtendedFilterItems($fieldname,$demand->getStoragePage(),$field['config']['foreign_table'],$field['config']['foreign_table_where'],$dependFields);
								foreach($itemsFromDb AS $item){
									$items[$item['uid']] = $item['title'];
								}
							}				
							if(count($items)){
								if(!isset($items['all'])){
									if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
										$selectAllLabel = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
									} else {
										$selectAllLabel = "Alle";
									}
									$itemsTmp 		= $items;
									$items 			= array();
									$items["all"] 	= $selectAllLabel;
									foreach($itemsTmp AS $key => $value){
										$items[$key] = $value;
									}
									unset($itemsTmp);
								}
								$extendedFilters[] = array(	"label" => $label,
															"name" => $fieldnameLCC,
															"type" => 'select',
															"field" => $fieldname,
															"fieldUCC" => $fieldnameUCC,
															"items" => $items,
															"depends" => $depends,
															"displayDepends" => $displayDepends );
							}
						} elseif($field['config']['type']=="input" && $field['addToMooxNewsFrontendFilter']['type']=="select") {
							
							if(!isset($overwriteDemand[$fieldnameLCC])){
								//$overwriteDemand[$fieldnameLCC] = "all";
							}
							
							$depends 		= array();
							$dependFields 	= array();
							$displayDepends	= array();
									
							if(count($field['addToMooxNewsFrontendFilter']['depends'])){
								foreach($field['addToMooxNewsFrontendFilter']['depends'] AS $depend){								
									if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend])){
										$dependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend];
										if($dependField['config']['type']=="select" || ($dependField['config']['type']=="input" && $dependField['addToMooxNewsFrontendFilter']['type']=="select")){
											$depends[] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($depend);
											$dependField = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($depend);
											if(isset($overwriteDemand[$dependField]) && $overwriteDemand[$dependField]!="all"){
												$dependFields[$depend] = $overwriteDemand[$dependField];
											}
										}
									}
								}
							}
							if(count($field['addToMooxNewsFrontendFilter']['displayDepends'])){
								foreach($field['addToMooxNewsFrontendFilter']['displayDepends'] AS $displayDepend){								
									if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend])){
										$displayDependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend];
										if($displayDependField['config']['type']=="select" || ($displayDependField['config']['type']=="input" && $displayDependField['addToMooxNewsFrontendFilter']['type']=="select")){
											$displayDepends[] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($displayDepend);										
										}
									}
								}
							}
							
							$values = array();
							
							$valuesFromDb = $this->newsRepository->findExtendedFilterUniqueValues($fieldname,$demand->getStoragePage(),$dependFields);
							
							if(count($valuesFromDb)){
								
								if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
									$values['all'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
								} else {
									$values['all'] = "Alle";
								}
								
								$valueCnt = 0;
								
								foreach($valuesFromDb AS $value){
									if($value=="empty"){
										if($field['addToMooxNewsFrontendFilter']['selectEmptyLabel']!=""){
											$valueLabel = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectEmptyLabel'], $field['extkey']);
											$values[$value] = $valueLabel;
										} else {
											$valueLabel = "Ohne Wert";
										}
									} else {
										$values[$value] = $value;
										$valueCnt++;
									}
									
								}
								
								if($valueCnt){
																	
									$extendedFilters[] = array(	"label" => $label,
																"name" => $fieldnameLCC,
																"type" => 'select',
																"field" => $fieldname,
																"fieldUCC" => $fieldnameUCC,
																"items" => $values,
																"depends" => $depends,
																"displayDepends" => $displayDepends );
								}
							}
						} else {
							$extendedFilters[] = array(	"label" => $label,
														"name" => $fieldnameLCC,
														"type" => 'default',
														"field" => $fieldname,
														"fieldUCC" => $fieldnameUCC);
						}
					}
				}
			}
		}		
		
		return $extendedFilters;
	}
	
	/**
	 * Injects a view.
	 * This function is for testing purposes only.
	 *
	 * @param \TYPO3\CMS\Fluid\View\TemplateView $view the view to inject
	 * @return void
	 */
	public function setView(\TYPO3\CMS\Fluid\View\TemplateView $view) {
		$this->view = $view;
	}
	
	/**
	 * send secure download header
	 *	
	 * @return void
	 */
	public function secureDownloadAction() {
		exit("test");
	}
	
	/**
	 * Set is ajax request
	 *
	 * @param boolean $isAjaxRequest
	 * @return void
	 */
	public function setIsAjaxRequest($isAjaxRequest) {
		$this->isAjaxRequest = $isAjaxRequest;
	}

	/**
	 * Getis ajax request
	 *
	 * @return boolean
	 */
	public function getIsAjaxRequest() {
		return $this->isAjaxRequest;
	}
	
	/**
	 * Set char list
	 *
	 * @param array $charList
	 * @return void
	 */
	public function setCharList($charList) {
		$this->charList = $charList;
	}

	/**
	 * Get char list
	 *
	 * @return array
	 */
	public function getCharList() {
		return $this->charList;
	}
}
