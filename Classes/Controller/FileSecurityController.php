<?php
namespace DCNGmbH\MooxNews\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FileSecurityController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * frontendUserRepository
	 *
	 * @var \DCNGmbH\MooxMailer\Domain\Repository\FrontendUserRepository
	 */
	protected $frontendUserRepository;
	
	/**
	 * newsRepository
	 *
	 * @var Tx_MooxNews_Domain_Repository_NewsRepository
	 */
	protected $newsRepository;
	
	/**
	 * moox news file root
	 *
	 * @var integer
	 */	
	public static $mooxNewsFileRoot = 'fileadmin/user_upload/moox_news/';
	
	/**
	 * get ts settings
	 *
	 * @param integer $pageUid page uid
	 * @return array $TSObj->setup
	 */
	public static function getTsSetup($pageUid) {
		$sysPageObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('t3lib_pageSelect');       
        $rootLine = $sysPageObj->getRootLine($pageUid);
		$TSObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('t3lib_tsparser_ext');             
        $TSObj->tt_track = 0;
        $TSObj->init();
        $TSObj->runThroughTemplates($rootLine);
        $TSObj->generateConfig();		
		
		return $TSObj->setup;
	}
	
	/**
	 * get base url
	 *
	 * @param integer $pageUid page uid
	 * @return string $baseURL
	 */
	public static function getBaseUrl($pageUid) {
		$TS = self::getTsSetup($pageUid);
          
		if($_SERVER['HTTPS']=="on"){
			$prefix = "https";
		} else {
			$prefix = "http";
		}
		
		if($TS['config.']['baseURL']==$prefix."://"){ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}  elseif($TS['config.']['baseURL']=="//"){ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} elseif($TS['config.']['baseURL']=="/"){ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} else {
			$baseURL = $TS['config.']['baseURL'];
			if(substr(strtolower($baseURL),0,2)=="//"){				
				$baseURL = $prefix.":".$baseURL;
			}
		}
		
		if(substr($baseURL,(strlen($baseURL)-1))!="/"){ 
			$baseURL = $baseURL."/";
		} 

		return $baseURL;
	}
	
	/**
	 * get login redirector pif
	 *
	 * @param integer $pageUid page uid
	 * @return string $loginRedirectorPid
	 */
	public static function getLoginRedirectorPid($pageUid) {
		$TS = self::getTsSetup($pageUid);
        
		return $TS['plugin.']['tx_mooxnews.']['settings.']['feLoginPid'];
	}
	
	/**
	 * Get file info	
	 *
	 * @param \int $uid
	 * @return	array	file 	
	 */
	public function getFileInfo($filename){
		$seperatorIndex 	= strrpos ($filename, ".");
		$file['name'] 		= substr ($filename, 0, $seperatorIndex);
		$file['extension'] 	= strtolower(substr ($filename, ($seperatorIndex+1)));
		return $file;
	}
	
	/**
	 * action load file
	 *	
	 * @param \string $file	
	 * @param \int 	  $page
	 * @param \string $hash	
	 * @param \string $pre
	 * @return void
	 */
	public function loadFileAction($file = '',$page = 0,$hash = '', $pre = '') {		
		
		$this->objectManager 				= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->newsRepository 				= $this->objectManager->get('Tx_MooxNews_Domain_Repository_NewsRepository');		
		$this->frontendUserRepository 		= $this->objectManager->get('DCNGmbH\\MooxMailer\\Domain\\Repository\\FrontendUserRepository');		
		
		$loginRedirectorPid = self::getLoginRedirectorPid($page);
		$baseUrl 			= self::getBaseUrl($page);
		
		if($loginRedirectorPid>0){
			$cObj 		= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('tslib_cObj');
			$loginurl 	= $cObj->typolink('', array('returnLast' => 'url', 'parameter' => $loginRedirectorPid, 'additionalParams' => '&redirect_url=redirecturi', 'useCacheHash' => 0));			
		}
		
		$fileAccess = false;
		
		if($hash!=""){
			$newsUid = base_convert($hash,21,10)-999999;
			$news = $this->newsRepository->findByUid($newsUid,FALSE);
			if(is_a($news, 'Tx_MooxNews_Domain_Model_NewsDefault')){
				if(!($news->getHidden() || $news->getStarttime()>time() || ($news->getEndtime()>0 && $news->getEndtime()>time()))){
					if($news->getFeGroup()!=0){
						
						if(is_array($GLOBALS["TSFE"]->fe_user->user)) {
							if($GLOBALS['TSFE']->fe_user->user['usergroup']!=0){
								$hasNewsAccess 	= false;
								$usergroups 	= explode(",",$GLOBALS['TSFE']->fe_user->user['usergroup']);
								$newsusergroups = explode(",",$news->getFeGroup());
								foreach($newsusergroups AS $newsusergroup){
									if(in_array($newsusergroup,$usergroups)){
										$hasNewsAccess 	= true;
										break;
									}
								}
								if($hasNewsAccess){
									$fileAccess = true;
								}
							}
						} else {
							$loginurl = str_replace("redirecturi",urlencode(substr(urldecode($_SERVER['REQUEST_URI']),1)),$loginurl);													
							if($loginurl!=""){
								$this->redirectToURI($baseUrl.$loginurl);
							}
						}
					} else {
						$fileAccess = true;
					}
				}
			} 
		} 
		
		$filepath 	= $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/".(($pre!="")?$pre."/":FileSecurityController::$mooxNewsFileRoot).$file;
		$filearray 	= explode("/",$file);
		$filearray	= array_reverse($filearray);
		$filename	= $filearray[0];
		
		if(!file_exists($filepath)){
			$fileAccess = false;
		}
		
		if($fileAccess){
			$fileInfo = $this->getFileInfo($filename);
			header("Content-type: application/octet-stream");
			header("Content-Length: " .(string)(filesize($filepath)) );					
			header('Content-Disposition: attachment; filename="'.(($filename)?$filename:"unknown").'"');		
			header('Content-type: application/'.$fileInfo['extension']);			
			readfile($filepath);	
		} else {			
			$this->redirectToURI($baseUrl);
		}
	}
}
?>