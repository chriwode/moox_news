<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

// sort helper function
function sortByFolderAndTitle($a, $b) {
	return strcmp($a["folder"].$a["title"], $b["folder"].$b["title"]);
}
 
/**
 * Administration controller
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_Controller_AdministrationController extends Tx_MooxNews_Controller_NewsController {

	const SIGNAL_ADMINISTRATION_INDEX_ACTION = 'indexAction';
	const SIGNAL_ADMINISTRATION_NEWSPIDLISTING_ACTION = 'newsPidListingAction';

	/**
	 * Page uid
	 *
	 * @var integer
	 */
	protected $page = 0;

	/**
	 * TsConfig configuration
	 *
	 * @var array
	 */
	protected $tsConfiguration = array();

	/**
	 * @var Tx_MooxNews_Domain_Repository_CategoryRepository
	 */
	protected $categoryRepository;
	
	/**
	 * @var \TYPO3\CMS\Frontend\Page\PageRepository
	 */
	protected $pageRepository;
		
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;
	
	/**
	 * backend session
	 *
	 * @var \DCNGmbH\MooxNews\Service\BackendSessionService
	 */
	protected $backendSession;
	
	/**
	 * extConf
	 *
	 * @var array
	 */
	protected $extConf;
		
	/**
	 * forceYearFilter
	 *
	 * @var boolean
	 */
	protected $forceYearFilter;
	
	/**
	 * page type of redirector
	 *
	 * @var integer
	 */	
	public static $listViewFieldSeparator = "&nbsp;|&nbsp;";
	
	/**
	 * get ts settings
	 *
	 * @param integer $pageUid page uid
	 * @return array $TSObj->setup
	 */
	public static function getTsSetup($pageUid) {
		$sysPageObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('t3lib_pageSelect');       
        $rootLine = $sysPageObj->getRootLine($pageUid);
		$TSObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('t3lib_tsparser_ext');             
        $TSObj->tt_track = 0;
        $TSObj->init();
        $TSObj->runThroughTemplates($rootLine);
        $TSObj->generateConfig();		
		
		return $TSObj->setup;
	}
	
	/**
	 * get base url
	 *
	 * @param integer $pageUid page uid
	 * @return string $baseURL
	 */
	public static function getBaseUrl($pageUid) {
		$TS = self::getTsSetup($pageUid);
          
		if($_SERVER['HTTPS']=="on"){
			$prefix = "https";
		} else {
			$prefix = "http";
		}
		
		if($TS['config.']['baseURL']==$prefix."://"){ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}  elseif($TS['config.']['baseURL']=="//"){ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} elseif($TS['config.']['baseURL']=="/"){ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} else {
			$baseURL = $TS['config.']['baseURL'];
			if(substr(strtolower($baseURL),0,2)=="//"){				
				$baseURL = $prefix.":".$baseURL;
			}
		} 
		
		if(substr($baseURL,(strlen($baseURL)-1))!="/"){ 
			$baseURL = $baseURL."/";
		}
		
		return $baseURL;
	}
	
	/**
	 * Function will be called before every other action
	 *
	 * @return void
	 */
	public function initializeAction() {
		$this->setPage((int)\TYPO3\CMS\Core\Utility\GeneralUtility::_GET('id'));
		$this->setTsConfig();
		$this->extConf 	= unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news']);		
		if (!empty($this->extConf['forceYearFilter'])){
			$this->setForceYearFilter(1);
		}
		$this->setMailerActive(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_mailer'));
		$this->pageRepository = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		$this->backendSession = $this->objectManager->get('DCNGmbH\\MooxNews\\Service\\BackendSessionService');
		parent::initializeAction();
		
	}

	/**
	 * Inject a news repository to enable DI
	 *
	 * @param Tx_MooxNews_Domain_Repository_CategoryRepository $categoryRepository
	 * @return void
	 */
	public function injectCategoryRepository(Tx_MooxNews_Domain_Repository_CategoryRepository $categoryRepository) {
		$this->categoryRepository = $categoryRepository;
	}
		
	/**
	 * Main action for administration
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand
	 * @param \array $sorting
	 * @param \int $parent
	 * @dontvalidate  $demand
	 * @return void
	 */
	public function indexAction(Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand = NULL, $sorting = array(), $parent = 0) {						
		
		// Extension manager configuration
		$configuration 	= Tx_MooxNews_Utility_EmConfiguration::getSettings();
		
		if(count($sorting) && ($sorting['field']!="" || isset($sorting['mode']))){
			if($this->backendSession->get("administrationDemand")){
				$demand = unserialize($this->backendSession->get("administrationDemand"));
				if(isset($sorting['mode'])){
					$demand->setSortingMode($sorting['mode']);
				} else {
					$demand->setSortingField($sorting['field']);
					$demand->setSortingDirection($sorting['direction']);
				}				
			}
		}		
		if (is_null($demand)) {			
			if($this->backendSession->get("administrationDemand")){
				$demand = unserialize($this->backendSession->get("administrationDemand"));
			} else {
				$demand = $this->objectManager->get('Tx_MooxNews_Domain_Model_Dto_AdministrationDemand');

				// Preselect by TsConfig (e.g. tx_mooxnews.module.preselect.topNewsRestriction = 1)
				if (isset($this->tsConfiguration['preselect.'])	&& is_array($this->tsConfiguration['preselect.'])) {
					unset($this->tsConfiguration['preselect.']['orderByAllowed']);
					foreach ($this->tsConfiguration['preselect.'] as $propertyName => $propertyValue) {
						\TYPO3\CMS\Extbase\Reflection\ObjectAccess::setProperty($demand, $propertyName, $propertyValue);
					}
				}	
				$this->backendSession->store("administrationDemand",serialize($demand));
			}						
		} else {
			$this->backendSession->store("administrationDemand",serialize($demand));
		}				
		$demand = $this->createDemandObjectFromSettings($demand);
		
		$folders = $this->getFolders($demand);
		
		$this->redirectToFolder($folders,$parent);
		
		$allowedTypes = array();
		if($this->getPage()>0){
			$pageInfo = $this->pageRepository->getPage($this->getPage());
			if($pageInfo['news_types']!=""){
				$allowedTypes = explode(",",$pageInfo['news_types']);
			}
		}		
		if(count($allowedTypes)>0 && !in_array($demand->getType(),$allowedTypes)){
			$demand->setType($allowedTypes[0]);
		}
		
		if($this->settings['list']['paginate']['itemsPerPage']>0){
			$demand->setPerPage($this->settings['list']['paginate']['itemsPerPage']);
		} else {
			$demand->setPerPage(20);
		}
		
		if(!$demand->getSortingField()){
			$demand->setSortingField('title');
		}
		
		if(!$demand->getSortingDirection()){
			$demand->setSortingDirection('ASC');
		}
		
		if($this->getForceYearFilter() && $demand->getYear()<1970){
			$demand->setYear(date("Y"));
		}
		
		if($demand->getSortingMode()){
			$demand->setType(NULL);
			$demand->setQuery(NULL);
			$demand->setTopNewsRestriction(NULL);
			$demand->setYear(NULL);
			$demand->setSysLanguageUid(NULL);
			$demand->setOrder("sorting ASC");					
		}
			
		if($configuration->getCategoryRestriction()=="current_pid"){			
			if($demand->getType()!=""){
				$categories = $this->categoryRepository->findCategoriesByTypeAndPid($this->page,array($demand->getType()),$demand->getSysLanguageUid());				
			} else {
				$categoryRoot = $this->getListViewCategoryRoot($demand->getType());
				if($categoryRoot>0){
					$categories = $this->categoryRepository->findCategoriesByParentAndPid($this->page,$categoryRoot,$demand->getSysLanguageUid());
				} else {
					$categories = $this->categoryRepository->findParentCategoriesByPid($this->page,$demand->getSysLanguageUid());
				}				
			}			
		} else {
			if($demand->getType()!=""){
				$categories = $this->categoryRepository->findAllCategoriesByType(array($demand->getType()),$demand->getSysLanguageUid());
			} else {
				$categoryRoot = $this->getListViewCategoryRoot($demand->getType());
				if($categoryRoot>0){
					$categories = $this->categoryRepository->findAllCategoriesByParent($categoryRoot,$demand->getSysLanguageUid());
				} else {
					$categories = $this->categoryRepository->findAllCategories($demand->getSysLanguageUid());
				}
			}
			
		}
		
		$selectedCategories = "";
		foreach($demand->getCategories() AS $category){
			if($selectedCategories){
				$selectedCategories .= ", ";
			}
			$selectedCategories .= $this->categoryRepository->findByUid($category)->getTitle();
		}
		
		$idList = array();
		foreach ($categories as $c) {
			$idList[] = $c->getUid();
		}
		
		$rootline = $this->pageRepository->getRootLine($this->page);
		
		foreach($rootline AS $rootlinepage){
			if($rootlinepage['is_siteroot']){
				$rootpage = $rootlinepage;
				break;
			}
		}
		
		if(!$rootpage){
			$rootpage = $rootline[0];
		}
		
		$rootfound = false;
		for($i=0;$i<count($rootline);$i++){
			if($rootfound){
				unset($rootline[$i]);
			} else {
				if($rootline[$i]['is_siteroot']){
					$rootfound = true;
				}
			}
		}
		
		$rootline = array_reverse($rootline);
		
		if(isset($rootline[count($rootline)-2])){			
			$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
			if($pageInfo['module']=='mxnews'){
				$folder = $pageInfo['uid'];				
			}			
		} 
			
		$languages 			= $this->getLanguages();		
		$years 				= $this->newsRepository->findAllBackendYears(false);
		
		$optionYears 		= array();
		
		if(!$this->getForceYearFilter()){
			$optionYears[0] = "Alle Jahre anzeigen";
		}
		foreach($years AS $year){
			if($year['year']>1970){
				$optionYears[$year['year']] = $year['year']." anzeigen";
			} else {
				$optionYears[1970] = "Artikel ohne Jahr anzeigen";
			}
		}		
		if($this->settings['defaultPreviewPid']>0){
			$previewBaseUrl = self::getBaseUrl($this->settings['defaultPreviewPid']);
		}
		
		// Get extended fields from external extensions for backend preview
		$extendedBackendPreviewFields = array();
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){			
			if($field['addToMooxNewsBackendPreview']){				
				$field['title'] = $fieldname;
				$field['name'] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
				$field['nameEmphasized'] = $fieldname;
				if($field['addToMooxNewsBackendPreview']['type']==""){
					$field['addToMooxNewsBackendPreview']['type'] = "text";
				}
				$extendedBackendPreviewFields[] = $field;
			}
		}		

		// Get special sorting fields
		$specialSortingFields = array();
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){			
			$nameParts = explode("_",$fieldname);
			if($nameParts[count($nameParts)-1]=="sortfield"){
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][str_replace("_sortfield","",$fieldname)])){
					$key = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase(str_replace("_sortfield","",$fieldname));
					$specialSortingFields[$key] = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($fieldname);
				}
			}			
		}		
		
		$types 	= $this->getTypes($demand,$allowedTypes);		
		$news 	= $this->newsRepository->findDemanded($demand, FALSE);
		$count 	= $news->count();
		if($demand->getSortingMode()){
			$sortHelper 	= array();			
			$sortedUids 	= array();
			$sorting		= array();
			for($pos=0;$pos<$count;$pos++){
				$sortedUids[$pos] = $news->offsetGet($pos)->getUid();
			}
			$sortingCnt = 0;
			foreach($sortedUids AS $pos => $uid){
				if($sortingCnt==0){
					$sortUp = 0;
				} elseif($sortingCnt==1){
					$sortUp = $this->page;
				} else {
					$sortUp = -($sortedUids[$pos-2]);
				}				
				if(isset($sortedUids[$pos+1])){
					$sortDown = -($sortedUids[$pos+1]);
				} else {
					$sortDown = 0;
				}
				$sortHelper["uid".$uid] = array("up" => $sortUp, "down" => $sortDown);
				$sortingCnt++;
			}			
		}
		
		$multipleFunctions 						= array();
		
		$multipleFunctions['delete'] 			= "Einträge löschen";
		$multipleFunctions['show'] 				= "Einträge aktivieren";
		$multipleFunctions['hide'] 				= "Einträge deaktivieren";
		
		if($this->mailerActive){
			$multipleFunctions['mailoff'] 		= "Markierung für Mailversand entfernen";		
			$multipleFunctions['mailsingle'] 	= "Für Direktversand markieren";
			$multipleFunctions['mailcumulated']	= "Für kumulierten Mailversand markieren";
		}
		
		$assignedValues = array(
			'page' => $this->page,
			'folder' => $folder,
			'years' => $optionYears,
			'languages' => $languages,			
			'rootpage' => $rootpage,
			'rootline' => $rootline,
			'demand' => $demand,
			'news' => $news,
			'count' => $count,
			'categories' => (count($idList) || $configuration->getCategoryRestriction()!="current_pid")?$this->categoryRepository->findTree($idList):array(),
			'selectedCategories' => $selectedCategories,
			'dateformat' => $GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'],
			'folders' => (count($folders)>1)?$folders:false,
			'previewBaseUrl' => $previewBaseUrl,
			'extendedBackendPreviewFields' => $extendedBackendPreviewFields,
			'types' => $types,
			'localizeUrl' => $this->getLocalizeUrl(),
			'sortingUrl' => $this->getSortingUrl(),
			'parent' => $parent,
			'conf' => $this->extConf,
			'fields' => $this->getListViewFields($demand->getType()),
			'fieldsSeparator' => self::$listViewFieldSeparator,
			'replacements' => $this->getQueryReplacements($demand->getQuery()),
			'allowedTypes' => $allowedTypes,
			'sortHelper' => $sortHelper,
			'specialSortingFields' => $specialSortingFields,
			'multipleFunctions' => $multipleFunctions,
			'mailerActive' => $this->mailerActive
		);

		$this->emitActionSignal('AdministrationController', self::SIGNAL_ADMINISTRATION_INDEX_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Shows a page tree including count of news + category records
	 *
	 * @param integer $treeLevel
	 * @return void
	 */
	public function newsPidListingAction($treeLevel = 2) {
			
		$tree = Tx_MooxNews_Utility_Page::pageTree($this->page, $treeLevel);

		$rawTree = array();
		foreach ($tree->tree as $row) {
			$this->countRecordsOnPage($row);
			$rawTree[] = $row;
		}

		$assignedValues = array(
			'tree' => $rawTree,
			'treeLevel' => $treeLevel,
		);

		$this->emitActionSignal('AdministrationController', self::SIGNAL_ADMINISTRATION_NEWSPIDLISTING_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Redirect to form to create a news record
	 *
	 * @param string $type
	 * @return void
	 */
	public function newNewsAction($type = "") {
		$this->redirectToCreateNewRecord('tx_mooxnews_domain_model_news',$type);
	}

	/**
	 * Redirect to form to create a category record
	 *
	 * @return void
	 */
	public function newCategoryAction() {
		$this->redirectToCreateNewRecord('sys_category');
	}

	/**
	 * Redirect to form to create a tag record
	 *
	 * @return void
	 */
	public function newTagAction() {
		$this->redirectToCreateNewRecord('tx_mooxnews_domain_model_tag');
	}
	
	/**
	 * Redirect to form to create a target record
	 *
	 * @return void
	 */
	public function newTargetAction() {
		$this->redirectToCreateNewRecord('tx_mooxnews_domain_model_target');
	}

	/**
	 * Create the demand object which define which records will get shown
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand
	 * @return Tx_MooxNews_Domain_Model_Dto_NewsDemand
	 */
	protected function createDemandObjectFromSettings(Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand) {
		$demand->setCategories($demand->getSelectedCategories());		
		$demand->setYear(($demand->getYear()>0)?$demand->getYear():"");
		$demand->setOrder($demand->getSortingField() . ' ' . $demand->getSortingDirection());
		$demand->setStoragePage(Tx_MooxNews_Utility_Page::extendPidListByChildren($this->page, (int)$demand->getRecursive()));
		$demand->setOrderByAllowed($this->settings['orderByAllowed']);

		// Ensure that always a storage page is set
		if ((int)$demand->getStoragePage() === 0) {
			$demand->setStoragePage('-3');
		}

		return $demand;
	}

	/**
	 * Update page record array with count of news & category records
	 *
	 * @param array $row page record
	 * @return void
	 */
	private function countRecordsOnPage(array &$row) {
		$pageUid = (int)$row['row']['uid'];

		/* @var $db \TYPO3\CMS\Core\Database\DatabaseConnection */
		$db = $GLOBALS['TYPO3_DB'];

		$row['countNews'] = $db->exec_SELECTcountRows(
			'*',
			'tx_mooxnews_domain_model_news',
			'pid=' . $pageUid . \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields('tx_mooxnews_domain_model_news'));
		$row['countCategories'] = $db->exec_SELECTcountRows(
			'*',
			'sys_category',
			'pid=' . $pageUid . \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields('sys_category'));

		$row['countNewsAndCategories'] = ($row['countNews'] + $row['countCategories']);
	}

	/**
	 * Redirect to tceform creating a new record
	 *
	 * @param string $table table name
	 * @param string $type type
	 * @return void
	 */
	private function redirectToCreateNewRecord($table,$type = '') {
		$pid = $this->page;
		if ($pid === 0) {
			if (isset($this->tsConfiguration['defaultPid.'])
				&& is_array($this->tsConfiguration['defaultPid.'])
				&& isset($this->tsConfiguration['defaultPid.'][$table])
			) {
				$pid = (int)$this->tsConfiguration['defaultPid.'][$table];
			}
		}

		$returnUrl = 'mod.php?M=moox_MooxNewsTxMooxnewsM2&id=' . $this->page . $this->getToken();
		if($type!=""){
			$url = 'alt_doc.php?edit[' . $table . '][' . $pid . ']=new&defVals[' . $table . '][type]='.$type.'&returnUrl=' . urlencode($returnUrl);
		} else {		
			$url = 'alt_doc.php?edit[' . $table . '][' . $pid . ']=new&returnUrl=' . urlencode($returnUrl);
		}

		\TYPO3\CMS\Core\Utility\HttpUtility::redirect($url);
	}

	/**
	 * Set the TsConfig configuration for the extension
	 *
	 * @return void
	 */
	protected function setTsConfig() {
		$tsConfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($this->page);
		if (isset($tsConfig['tx_mooxnews.']['module.']) && is_array($tsConfig['tx_mooxnews.']['module.'])) {
			$this->tsConfiguration = $tsConfig['tx_mooxnews.']['module.'];
		}
	}	

	/**
	 * Get a CSRF token
	 *
	 * @return string
	 */
	protected function getToken() {
		return '&moduleToken=' . \TYPO3\CMS\Core\FormProtection\FormProtectionFactory::get()->generateToken('moduleCall', 'moox_MooxNewsTxMooxnewsM2');
	}
	
	/**
	 * Get array of folders with news module	
	 *
	 * @param Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand
	 * @return	array	folders with news module	
	 */
	public function getFolders($demand) {
		
		global $BE_USER;
		
		$folders = array();
		
		if($demand->getType()!='' && false){
			$typeStmt = 'AND news_type="'.$demand->getType().'"';
		}
		
		$query = array(
			'SELECT' => '*',
			'FROM' => 'pages',
			'WHERE' => $BE_USER->getPagePermsClause(1).' AND deleted=0 AND doktype=254 AND module="mxnews"'.$typeStmt
		);
		$pages = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);
		
		$folderCnt = 0;
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($pages)) {
			$folders[$folderCnt] = $row;
			$rootline = $this->pageRepository->getRootLine($row['uid']);
			
			foreach($rootline AS $rootlinepage){
				if($rootlinepage['is_siteroot']){
					$rootlinepage['title'] = $rootlinepage['title'];
					$folders[$folderCnt]['rootpage'] = $rootlinepage;
					break;
				}
			}
		
			if(!$folders[$folderCnt]['rootpage']){
				$folders[$folderCnt]['rootpage'] = $rootline[0];
			}
			
			$rootline = array_reverse($rootline);			
			
			if(isset($rootline[count($rootline)-2])){			
				$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
				if($pageInfo['module']=='mxnews'){
					$folders[$folderCnt]['folder'] = $pageInfo['uid'];				
				}
				
			}
			
			$folders[$folderCnt]['rootline'] = $rootline;
			$folderCnt++;
		}
		
		usort($folders, "sortByFolderAndTitle");
		
		$folders = array_reverse($folders);
		
		return $folders;		
	}
	
	/**
	 * Get array of allowed types
	 *	
	 * @param Tx_MooxNews_Domain_Model_Dto_AdministrationDemand $demand
	 * @param array $allowedTypes
	 * @return	array	types	
	 */
	public function getTypes($demand,$allowedTypes = array()) {
		
		$types 	= array();		
				
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option){			
			if((count($allowedTypes)<1 || in_array($option[1],$allowedTypes)) && !($this->extConf['hideDefaultNewsType'] && $option[1]=="moox_news")){
				$types[$option[1]] = $GLOBALS['LANG']->sL($option[0], TRUE);
			}
		}
		return $types;
	}	
	
	/**
	 * Returns allowed fields
	 *
	 * @return array
	 */
	public function getAllowedFields() {
		$allowedFields = array();
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $field => $column){
			$allowedFields[] = $field;
		}
		return $allowedFields;
	}
	
	/**
	 * Get array of allowed languages
	 *	
	 * @return	array	languages	
	 */
	public function getLanguages() {
		
		$return				= array();
		$languages 			= array();
		$languagesSelect	= array();
		
		$translationConfigurationProvider = $this->objectManager->get('TYPO3\\CMS\\Backend\\Configuration\\TranslationConfigurationProvider');
		
		$sysLanguages = $translationConfigurationProvider->getSystemLanguages();
		
		//$sysLanguages = \TYPO3\CMS\Backend\Utility\BackendUtility::getSystemLanguages();
		
		$language['title'] 		= "Artikel für alle Sprachen anzeigen";
		$language['uid'] 		= -1;
		$language['flagIcon'] 	= "flags-europeanunion";
		$language[0]			= $language['title'];
		$language[1]			= $language['uid'];
		$language[2]			= $language['flagIcon'];				
		$languages[-1] 			= $language;
		$languagesSelect[-1] 	= $language[0];
		
		/*
		foreach($sysLanguages AS $language){
			if($language[1]==0){
				if($this->settings['languageDefaultLanguageLabel']!=""){
					$language[0] = $this->settings['languageDefaultLanguageLabel']." [0]";
				}
				if($this->settings['languageDefaultIsoFlag']!=""){
					$language[2] = "flags-".$this->settings['languageDefaultIsoFlag'];
				}			
			} 
			$language['title'] 				= $language[0];
			$language['uid'] 				= $language[1];
			$language['flag'] 				= $language[2];
			$languages[$language[1]] 		= $language;
			$languagesSelect[$language[1]] 	= $language[0];
		}
		*/
		
		foreach($sysLanguages AS $language){
			if($language['uid']>-1){
				if($language['uid']==0){
					if($this->settings['languageDefaultLanguageLabel']!=""){
						$language['title'] = $this->settings['languageDefaultLanguageLabel'];
					}
					if($this->settings['languageDefaultIsoFlag']!=""){
						$language['flagIcon'] = "flags-".$this->settings['languageDefaultIsoFlag'];
					}
					$language['hidden'] = 0;
				}
				if($language['hidden']==0){
					$language[0] 					= $language['title'];
					$language[1] 					= $language['uid'];
					$language[2] 					= $language['flagIcon'];					
					$languages[$language['uid']] 	= $language;
					$languagesSelect[$language['uid']] 	= $language['title'];
				}
			}
		}
		
		$return['items'] 	= $languages;
		$return['select'] 	= $languagesSelect;
		
		return $return;
	}
	
	/**
	 * Get first folder with news module	
	 *	
	 * @return	array	folder 	
	 */
	public function getFirstFolder() {
		
		global $BE_USER;
		
		$query = array(
			'SELECT' => '*',
			'FROM' => 'pages',
			'WHERE' => $BE_USER->getPagePermsClause(1)." AND deleted=0 AND doktype=254 AND module='mxnews'",
			'LIMIT' => 1
		);
		$pages = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);		
		return $pages->fetch_assoc();		
	}
	
	/**
	 * action delete news
	 *	
	 * @param \int $uid	 
	 * @return void
	 */
	public function deleteNewsAction($uid = 0) {			
		
		if($uid>0){
		
			$object = $this->newsRepository->findByUid($uid,FALSE);
			
			$this->newsRepository->remove($object);
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flushCache($uid);
			
			$this->flashMessageContainer->add(
					'', 
					'Eintrag wurde gelöscht.', 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
						
		} 
		
		$this->redirect("index");
	}
	
	/**
	 * action delete translation
	 *	
	 * @param \int $uid
	 * @param \int $parent
	 * @return void
	 */
	public function deleteTranslationAction($uid = 0, $parent = 0) {			
		
		if($uid>0){
					
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'tx_mooxnews_domain_model_news',
				'uid='.$uid,
				array('deleted' => 1)
			);
						
			$this->flushCache($uid);
		
			$this->flashMessageContainer->add(
					'', 
					'Übersetzung wurde gelöscht.',  
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 
		
		$this->redirect("index",NULL,NULL,array("parent" => $parent));
	}
	
	/**
	 * action move to folder
	 *
	 * @param \int $pid	
	 * @param \int $uid	 
	 * @return void
	 */
	public function moveToFolderAction($pid = 0, $uid = 0) {			
		
		if($pid>0 && $uid>0){						
			
			$object = $this->newsRepository->findByUid($uid,FALSE);
			
			$object->setPid($pid);
			
			$maxSort = $this->newsRepository->findMaxSortingByPid($pid);
			
			$newsSort = $maxSort+1;
			
			$object->setSorting($newsSort);
			
			$this->newsRepository->update($object);								
			
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'tx_mooxnews_domain_model_news',
				'l10n_parent='.$uid,
				array('pid' => $pid)
			);
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flushCache($uid);
		
			$this->flashMessageContainer->add(
					'', 
					'Eintrag wurde erfolgreich verschoben', 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 
		
		$this->redirect("index");			
	}
	
	/**
	 * action clone news
	 *
	 * @param \int $uid	 
	 * @return void
	 */
	public function cloneNewsAction($uid = 0) {			
		
		if($uid>0){						
			
			$defaultStorageFields 	= array(	"categories",
												"contentElements",
												"relatedFiles",
												"relatedLinks",
												"media",
												"falMedia",
												"falRelatedFiles"	);
											
			$testedStorageFields 	= array(	"categories"	);
			
			$storageClasses = array("TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage");
			
			$original = $this->newsRepository->findByUid($uid,FALSE);
			
			$clone = $this->objectManager->create('Tx_MooxNews_Domain_Model_News');

			// $product = source object
			$properties = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getSettablePropertyNames($original);
			foreach ($properties as $name) {
				$value = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getProperty($original, $name);
				if(!in_array($name,$defaultStorageFields) && (!is_object($value) || (is_object($value) && !in_array(get_class($value),$storageClasses)))){					
					\TYPO3\CMS\Extbase\Reflection\ObjectAccess::setProperty($clone, $name, $value);
				} 				
			}
			
			foreach ($properties as $name) {
				$value = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getProperty($original, $name);
				if((in_array($name,$defaultStorageFields) && in_array($name,$testedStorageFields)) || (!in_array($name,$defaultStorageFields) && is_object($value) && in_array(get_class($value),$storageClasses))){										
					$getCall = "get".ucfirst($name);
					$setCall = "get".ucfirst($name);
					if(method_exists($original, $getCall)){
						$objects = $original->$getCall();
						if(method_exists($clone->$getCall(),"attach")){
							if(count($objects)){
								foreach ($objects as $object) {
									if(\TYPO3\CMS\Extbase\Reflection\ObjectAccess::isPropertySettable($object, "uid")){
										//$object->setUid(NULL);
									}
									$clone->$getCall()->attach($object);								
								}
							}
						} elseif(method_exists($clone,$setCall)){
							$clone->$setCall($objects);
						}
					}
				} 				
			}
			
			$clone->setTitle($original->getTitle()." [Kopie]");
			$clone->setHidden(1);
			
			$this->newsRepository->add($clone);
												
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flashMessageContainer->add(
					'', 
					'Eintrag wurde dupliziert', 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 
		
		$this->redirect("index");
	}
	
	/**
	 * action toggle news state
	 *
	 * @param \int $uid	 
	 * @return void
	 */
	public function toggleNewsStateAction($uid = 0) {			
		
		if($uid>0){						
			
			$object = $this->newsRepository->findByUid($uid,FALSE);
			
			if($object->getHidden()==1){
				$object->setHidden(0);
				$action = "aktiviert";
			} else {
				$object->setHidden(1);
				$action = "deaktiviert";
			}			
			
			$this->newsRepository->update($object);								
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flushCache($uid);
		
			$this->flashMessageContainer->add(
					'', 
					'Eintrag wurde erfolgreich '.$action, 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 
		
		$this->redirect("index");
		
	}
	
	/**
	 * action toggle translation state
	 *
	 * @param \int $uid
	 * @param \int $parent
	 * @return void
	 */
	public function toggleTranslationStateAction($uid = 0, $parent = 0) {			
		
		if($uid>0){						
			
			$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
				'hidden',
				'tx_mooxnews_domain_model_news',
				'deleted=0 AND uid='.$uid,
				'',
				'',
				1
			);
			
			$newsItem = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
			if($newsItem['hidden']==1){
				$newsItem['hidden'] = 0;
				$action = "aktiviert";
			} else {
				$newsItem['hidden'] = 1;
				$action = "deaktiviert";
			}
			
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'tx_mooxnews_domain_model_news',
				'uid='.$uid,
				array('hidden' => $newsItem['hidden'])
			);
						
			$this->flushCache($uid);
		
			$this->flashMessageContainer->add(
					'', 
					'Übersetzung wurde erfolgreich '.$action, 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 
		
		$this->redirect("index",NULL,NULL,array("parent" => $parent));
		
	}
	
	/**
	 * action multiple
	 *
	 * @param \string $function	
	 * @param \array $items
	 * @return void
	 */
	public function multipleAction($function = "", $items = array()) {			
		
		$functionCnt = 0;
		
		if(is_array($items) && count($items)){
			switch ($function) {
				case "delete":
					$news = array();
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$news[] = $object;
							unset($object);
						}
					}
					$this->view->assign('items', $news);
					$this->view->assign('function', $function);
					$this->view->assign('functionTxt', "löschen");				
					$skipRedirect = true;
					break;
				case "deleteConfirmed":
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$this->newsRepository->remove($object);
							$this->flushCache($object->getUid());
							unset($object);
							$functionCnt++;
						}
					}
					$message = $functionCnt." Einträge gelöscht";
					$skipPagination = true;															
					break;
				case "show":
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$object->setHidden(0);						
							$this->newsRepository->update($object);
							$this->flushCache($object->getUid());
							unset($object);
							$functionCnt++;
						}
					}	
					$message = $functionCnt." Einträge aktiviert";
					break;
				case "hide":
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$object->setHidden(1);						
							$this->newsRepository->update($object);
							$this->flushCache($object->getUid());
							unset($object);
							$functionCnt++;						
						}
					}	
					$message = $functionCnt." Einträge deaktiviert";
					break;	
				case "mailoff":
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$object->setMailerFrequency(0);						
							$this->newsRepository->update($object);
							unset($object);
							$functionCnt++;						
						}
					}	
					$message = "Markierung für Mailversand für ".$functionCnt." Einträge entfernt";
					break;
				case "mailsingle":
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$object->setMailerFrequency(1);						
							$this->newsRepository->update($object);
							unset($object);
							$functionCnt++;						
						}
					}	
					$message = $functionCnt." Einträge für Direktversand markiert";
					break;
				case "mailcumulated":
					foreach($items AS $uid){					
						$object = $this->newsRepository->findByUid($uid,FALSE);						
						if(is_object($object)){
							$object->setMailerFrequency(2);						
							$this->newsRepository->update($object);
							unset($object);
							$functionCnt++;						
						}
					}	
					$message = $functionCnt." Einträge für kumulierten Versand markiert";
					break;
			}
		}
		
		if($functionCnt){
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();					
		}
		
		if($message){
			$this->flashMessageContainer->add(
					'', 
					$message, 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
		}
		
		if(!$skipRedirect){
			$this->redirect("index");	
		}
	}
	
	/**
	 * Get query replacements	
	 *
	 * @param string $query
	 * @return	array	list view fields 	
	 */
	public function getQueryReplacements($query = "") {
		
		$replacements = array();
		
		if($query!=""){
			$replacements[] = array("search" => $query, "replace" => '<span class="query">'.$query.'</span>');
			$replacements[] = array("search" => strtolower($query), "replace" => '<span class="query">'.strtolower($query).'</span>');
			$replacements[] = array("search" => strtoupper($query), "replace" => '<span class="query">'.strtoupper($query).'</span>');
			$replacements[] = array("search" => ucfirst($query), "replace" => '<span class="query">'.ucfirst($query).'</span>');
			$replacements[] = array("search" => '<span class="query"><span class="query">', "replace" => '<span class="query">');
			$replacements[] = array("search" => '</span></span>', "replace" => '</span>');
		}
		
		return $replacements;
	}
	
	/**
	 * Get list view fields	
	 *	
	 * @param \string $type
	 * @return	int	category tree root id 	
	 */
	public function getListViewCategoryRoot($type = 0) {
		
		$listViewCategoryRoot = 0;
		
		if(isset($this->settings[$type]['backendCategoryRoot']) && $this->settings[$type]['backendCategoryRoot']>0){			
			$listViewCategoryRoot = $this->settings[$type]['backendCategoryRoot'];		
		} 
		
		return $listViewCategoryRoot;
		
	}
	
	/**
	 * Get list view fields	
	 *	
	 * @param \string $type
	 * @return	array	list view fields 	
	 */
	public function getListViewFields($type = 0) {
		
		$varTypes 		= array("text","date","datetime","currency");		
		$dateFields 	= array("datetime","archive","tstamp");
		$dateTimeFields = array();		
		$listViewFields = array();
		
		if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewFields'][$type])&& $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewFields'][$type]!=""){			
			$listViewFieldsString = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewFields'][$type];		
		} else {
			$listViewFieldsString = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewFields']['moox_news'];
		}
		
		if($listViewFieldsString!=""){
			$listViewFieldsTmp = explode(",",$listViewFieldsString);
			foreach($listViewFieldsTmp AS $listViewField){
				$listViewField = explode(":",$listViewField);
				if(in_array(trim($listViewField[0]),$this->getAllowedFields())){
					$fieldTCA 	= $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][trim($listViewField[0])];
					$fieldType 	= "text";
					if(isset($fieldTCA['addToMooxNewsBackendPreview']['type']) && in_array($fieldTCA['addToMooxNewsBackendPreview']['type'],$varTypes)){
						$fieldType = $fieldTCA['addToMooxNewsBackendPreview']['type'];
					} elseif(in_array(trim($listViewField[0]),$dateFields)){
						$fieldType = "date";
					} elseif(in_array(trim($listViewField[0]),$dateTimeFields)){
						$fieldType = "datetime";
					}
					$extkey = (($fieldTCA['extkey']!="")?$fieldTCA['extkey']:"moox_news");
					if($extkey=="moox_news"){
						$label = "LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_news.".trim($listViewField[0]);
					} else {
						$label = (($fieldTCA['label']!="")?$fieldTCA['label']:"");
					}
					
					$listViewFields[] = array(	
												"name" 			=> \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase(trim($listViewField[0])),
												"lenght" 		=> (trim($listViewField[1])>0)?trim($listViewField[1]):10000,
												"additional" 	=> array(),
												"type"			=> $fieldType,
												"extkey"		=> $extkey,
												"label"			=> $label
										);
				} else {
					$additionalFieldsAccepted = true;
					$listViewAdditionalFields = array();
					$listViewAdditionalFieldsTmp = explode("|",$listViewField[0]);					
					if(count($listViewAdditionalFieldsTmp)>1){
						foreach($listViewAdditionalFieldsTmp AS $listViewAdditionalField){
							if(in_array(trim($listViewAdditionalField),$this->getAllowedFields())){
								$fieldTCA 	= $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][trim($listViewAdditionalField)];
								$fieldType 	= "text";
								if(isset($fieldTCA['addToMooxNewsBackendPreview']['type']) && in_array($fieldTCA['addToMooxNewsBackendPreview']['type'],$varTypes)){
									$fieldType = $fieldTCA['addToMooxNewsBackendPreview']['type'];
								} elseif(in_array(trim($listViewAdditionalField),$dateFields)){
									$fieldType = "date";
								} elseif(in_array(trim($listViewAdditionalField),$dateTimeFields)){
									$fieldType = "datetime";
								}
								$extkey = (($fieldTCA['extkey']!="")?$fieldTCA['extkey']:"moox_news");
								if($extkey=="moox_news"){
									$label = "LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_news.".trim($listViewAdditionalField);
								} else {
									$label = (($fieldTCA['label']!="")?$fieldTCA['label']:"");
								}
								$listViewAdditionalFields[] = array(
																		"name" 		=> \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase(trim($listViewAdditionalField)), 
																		"type" 		=> $fieldType,
																		"extkey"	=> $extkey,
																		"label"		=> $label
																	);									
							} else {
								$additionalFieldsAccepted = false;
								break;
							}							
						}
					} else {
						$additionalFieldsAccepted = false;
					}
					if($additionalFieldsAccepted){
						$mainViewField = $listViewAdditionalFields[0];
						$fieldTCA 	= $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$mainViewField];
						$fieldType 	= "text";
						if(isset($fieldTCA['addToMooxNewsBackendPreview']['type']) && in_array($fieldTCA['addToMooxNewsBackendPreview']['type'],$varTypes)){
							$fieldType = $fieldTCA['addToMooxNewsBackendPreview']['type'];
						} elseif(in_array(trim($listViewAdditionalField),$dateFields)){
							$fieldType = "date";
						} elseif(in_array(trim($listViewAdditionalField),$dateTimeFields)){
							$fieldType = "datetime";
						}
						$extkey = (($fieldTCA['extkey']!="")?$fieldTCA['extkey']:"moox_news");
						if($extkey=="moox_news"){
							$label = "LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_news.".$mainViewField;
						} else {
							$label = (($fieldTCA['label']!="")?$fieldTCA['label']:"");
						}
						unset($listViewAdditionalFields[0]);
						$listViewFields[] = array(	
													"name" 			=> \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToLowerCamelCase($mainViewField),
													"lenght" 		=> (trim($listViewField[1])>0)?trim($listViewField[1]):10000,
													"additional" 	=> $listViewAdditionalFields,
													"type"			=> $fieldType,
													"extkey"		=> $extkey,
													"label"			=> $label
											);
					}
				}
			}
		}		
		if(!count($listViewFields)){
			$listViewFields[0] = array("name" => "datetime", "length" => 10000, "additional" => array(), "type" => 'date', "extkey" => 'moox_news', "label" => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_news.datetime');
			$listViewFields[1] = array("name" => "archive", "length" => 10000, "additional" => array(), "type" => 'date', "extkey" => 'moox_news', "label" => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_news.archive');
			$listViewFields[2] = array("name" => "tstamp", "length" => 10000, "additional" => array(), "type" => 'date', "extkey" => 'moox_news', "label" => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_news.tstamp');
		}
		
		return $listViewFields;
	}
	
	/**
	 * set page or redirect to default/last folder
	 *
	 * @param \array $folders
	 * @param \int $parent
	 * @return void
	 */
	public function redirectToFolder($folders = array(),$parent = 0) {
		
		global $BE_USER;
		
		$id 		= \TYPO3\CMS\Core\Utility\GeneralUtility::_GET('id');
		$id			= ($id!="")?(int)$id:NULL;
		
		if(is_null($id)){
			if(!is_null($this->backendSession->get("id"))){
				$id = $this->backendSession->get("id");
				$url = 'mod.php?M=moox_MooxNewsTxMooxnewsM2&id=' . (int)$id . $this->getToken();
				if($parent>0){
					$url .= "&tx_mooxnews_moox_mooxnewstxmooxnewsm2[parent]=".$parent;
				}
				\TYPO3\CMS\Core\Utility\HttpUtility::redirect($url);
			} elseif((int)$BE_USER->getTSConfigVal('tx_mooxnews.module.redirectToPageOnStart')>0){
				$id = (int)$BE_USER->getTSConfigVal('tx_mooxnews.module.redirectToPageOnStart');
				$url = 'mod.php?M=moox_MooxNewsTxMooxnewsM2&id=' . (int)$id . $this->getToken();
				if($parent>0){
					$url .= "&tx_mooxnews_moox_mooxnewstxmooxnewsm2[parent]=".$parent;
				}
				\TYPO3\CMS\Core\Utility\HttpUtility::redirect($url);
			} elseif((int)$this->settings['mooxNewsStartPid']>0){
				$id = (int)$this->settings['mooxNewsStartPid'];
				$url = 'mod.php?M=moox_MooxNewsTxMooxnewsM2&id=' . (int)$id . $this->getToken();
				if($parent>0){
					$url .= "&tx_mooxnews_moox_mooxnewstxmooxnewsm2[parent]=".$parent;
				}
				\TYPO3\CMS\Core\Utility\HttpUtility::redirect($url);
			}
			
		}
		$isMooxNewsFolder = false;
		foreach($folders AS $mooxNewsFolder){
			if($mooxNewsFolder['uid']==$id){
				$isMooxNewsFolder = true;
				break;
			}
		}
		if(!$isMooxNewsFolder){			
			$this->backendSession->delete("administrationDemand");
			$firstFolder = $this->getFirstFolder();
			if($firstFolder['uid']>0){
				$id = $firstFolder['uid'];
				$url = 'mod.php?M=moox_MooxNewsTxMooxnewsM2&id=' . (int)$id . $this->getToken();
				if($parent>0){
					$url .= "&tx_mooxnews_moox_mooxnewstxmooxnewsm2[parent]=".$parent;
				}
				\TYPO3\CMS\Core\Utility\HttpUtility::redirect($url);
			}
		}
		
		if(!is_null($id) && $id>0){
			$this->setPage($id);
			$this->backendSession->store("id",$this->page);
		} else {
			$this->backendSession->delete("id");
			$this->backendSession->delete("administrationDemand");
		}		
	}
	
	/**
	 * Flush cache	
	 *	
	 * @param \int $uid	
	 * @return void
	 */
	public function flushCache($uid = 0) {
		
		$cacheTag = "tx_mooxnews_domain_model_news_".$uid;

		/** @var $cacheManager \TYPO3\CMS\Core\Cache\CacheManager */
		$cacheManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager');
		$cacheManager->getCache('cache_pages')->flushByTag($cacheTag);
		$cacheManager->getCache('cache_pagesection')->flushByTag($cacheTag);
		$cacheManager->getCache('cache_pages')->flushByTag('tx_mooxnews');
		$cacheManager->getCache('cache_pagesection')->flushByTag('tx_mooxnews');	
	}
	
	/**
	 * get localize url	
	 *	
	 * @return string localize url
	 */
	public function getLocalizeUrl() {
					
		$localizeUrlTemplate  = 'tce_db.php?';
		$localizeUrlTemplate .= '&cmd[tx_mooxnews_domain_model_news][---uid---][localize]=---lang---';
		$localizeUrlRedirect  = 'alt_doc.php?';
		$localizeUrlRedirect .= 'returnUrl=';
		$localizeUrlRedirect .= urlencode(\TYPO3\CMS\Backend\Utility\BackendUtility::getModuleUrl('moox_MooxNewsTxMooxnewsM2', array()));
		$localizeUrlRedirect .= '&edit[tx_mooxnews_domain_model_news][---uid---]=edit';
		$localizeUrlRedirect .= '&justLocalized=tx_mooxnews_domain_model_news'.urlencode(":---uid---:---lang---");
		$localizeUrlTemplate .= '&redirect='.urlencode($localizeUrlRedirect);		
		$localizeUrlTemplate .= \TYPO3\CMS\Backend\Utility\BackendUtility::getUrlToken('tceAction');
		$localizeUrlTemplate .= '&prErr=1&uPT=1';
			
		return $localizeUrlTemplate;
	}
	
	/**
	 * get sorting url	
	 *	
	 * @return string sorting url
	 */
	public function getSortingUrl() {
		
		$sortingUrlTemplate  = 'tce_db.php?';
		$sortingUrlTemplate .= '&cmd[tx_mooxnews_domain_model_news][---uid---][move]=---pos---';
		$sortingUrlTemplate .= '&redirect=\'+T3_THIS_LOCATION+\'';		
		$sortingUrlTemplate .= \TYPO3\CMS\Backend\Utility\BackendUtility::getUrlToken('tceAction');
		$sortingUrlTemplate .= '&prErr=1&uPT=1';
			
		return $sortingUrlTemplate;
	}
	
	/**
	 * Returns page
	 *
	 * @return integer
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * Set page
	 *
	 * @param integer $page page
	 * @return void
	 */
	public function setPage($page) {
		$this->page = $page;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf() {
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf) {
		$this->extConf = $extConf;
	}

	/**
	 * Returns mailer active
	 *
	 * @return boolean
	 */
	public function getMailerActive() {
		return $this->mailerActive;
	}

	/**
	 * Set mailer active
	 *
	 * @param boolean $mailerActive mailer active
	 * @return void
	 */
	public function setMailerActive($mailerActive) {
		$this->mailerActive = $mailerActive;
	}
	
	/**
	 * Returns force year filter
	 *
	 * @return boolean
	 */
	public function getForceYearFilter() {
		return $this->forceYearFilter;
	}

	/**
	 * Set force year filter
	 *
	 * @param boolean $forceYearFilter force year filter
	 * @return void
	 */
	public function setForceYearFilter($forceYearFilter) {
		$this->forceYearFilter = $forceYearFilter;
	}
}
