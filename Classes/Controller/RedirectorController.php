<?php
namespace DCNGmbH\MooxNews\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class RedirectorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
					
	/**
	 * action redirect
	 *	
	 * @param \int $pid
	 * @param \int $uid	
	 * @return void
	 */
	public function redirectAction($pid = 0, $uid = 0) {		
		
		if($pid>0 && $uid>0){			
						
			$uri = $this->uriBuilder->setTargetPageUid($pid)->setCreateAbsoluteUri(true)->uriFor('detail', array("news" => $uid), 'News', 'MooxNews', 'Pi1');
				
			$this->redirectToURI($uri);
						
		} else {
			echo "can't redirect to news item";
			exit();
		}
	}
}
?>