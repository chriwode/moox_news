<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Paginate controller to create the pagination.
 * Extended version from fluid core
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */
class Tx_MooxNews_ViewHelpers_Widget_Controller_PaginatorController extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\Controller\PaginateController {

	/**
	 * @var array
	 */
	protected $configuration = array('itemsPerPage' => 10, 'insertAbove' => FALSE, 'insertBelow' => TRUE, 'maximumNumberOfLinks' => 99, 'addQueryStringMethod' => '', 'mode' => '');			

	/**
	 * @return void
	 */
	public function initializeAction() {
	
		$this->objects = $this->widgetConfiguration['objects'];
		\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($this->configuration, $this->widgetConfiguration['configuration'], TRUE);
		$itemsPerPage = (integer)$this->configuration['itemsPerPage'];
		if ($itemsPerPage === 0) {
			throw new RuntimeException('The itemsPerPage is 0 which is not allowed. Please also add "list.paginate.itemsPerPage" to the TS setting settings.overrideFlexformSettingsIfEmpty!', 1400741142);
		}
		$this->numberOfPages = ceil(count($this->objects) / (int)$this->configuration['itemsPerPage']);
		$this->maximumNumberOfLinks = (int)$this->configuration['maximumNumberOfLinks'];		
	}
}
