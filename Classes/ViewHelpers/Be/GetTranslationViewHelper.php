<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2011 Georg Ringer <typo3@ringerge.org>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * ViewHelper to get translation of given news record and language
 *
 * # Example: Basic example
 * <code>
 * <n:be.getTranslation as="translation" newsItem="newsItem" language="1"/>
 * </code>
 * <output>
 * Array of translation
 * </output>
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */

class Tx_MooxNews_ViewHelpers_Be_GetTranslationViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Render link to news item or internal/external pages
	 *
	 * @param Tx_MooxNews_Domain_Model_News $newsItem current news object
	 * @param int $language
	 * @param string $as	 
	 * @param boolean $reverse If enabled, the iterator will start with the last element and proceed reversely
	 * @param string $iteration The name of the variable to store iteration information (index, cycle, isFirst, isLast, isEven, isOdd)
	 * @return int translations count
	 */
	public function render(Tx_MooxNews_Domain_Model_News $newsItem, $language, $as = "translation", $reverse = FALSE, $iteration = NULL) {
		
		return self::renderStatic($this->arguments, $this->buildRenderChildrenClosure(), $this->renderingContext);
	}
	
	/**
	 * @param array $arguments
	 * @param \Closure $renderChildrenClosure
	 * @param \TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext
	 * @return string
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 */
	static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, \TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
		$templateVariableContainer = $renderingContext->getTemplateVariableContainer();
		
		$object = self::getTranslation($arguments['newsItem']->getUid(),$arguments['language']);				
		
		$templateVariableContainer->add($arguments['as'], $object);
		$output = $renderChildrenClosure();
		$templateVariableContainer->remove($arguments['as']);
		
		return $output;
	}		
	
	/**
	 * Get translation
	 *
	 * @param int $uid
	 * @param int $sysLanguageUid
	 * @return Tx_MooxNews_Domain_Model_News
	 */
	public function getTranslation($uid,$sysLanguageUid) {
		
		$translation = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordLocalization('tx_mooxnews_domain_model_news',$uid,$sysLanguageUid);
		if(is_array($translation[0])){
			$return = $translation[0];				
		}
		
		return $return;		
	}
}
